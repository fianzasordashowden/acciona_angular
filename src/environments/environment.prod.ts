export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCYtKCW-P9gnT0xb_7Pgs3dkAN8Lxy894s",
    authDomain: "pushnotificationapp-a7f24.firebaseapp.com",
    databaseURL: "https://pushnotificationapp-a7f24.firebaseio.com",
    projectId: "pushnotificationapp-a7f24",
    storageBucket: "pushnotificationapp-a7f24.appspot.com",
    messagingSenderId: "390424072203",
    appId: "1:390424072203:web:742bd2eeac460818",
    measurementId: "G-F0N5611NTB"
  }
};
