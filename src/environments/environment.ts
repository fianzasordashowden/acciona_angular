// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCYtKCW-P9gnT0xb_7Pgs3dkAN8Lxy894s",
    authDomain: "pushnotificationapp-a7f24.firebaseapp.com",
    databaseURL: "https://pushnotificationapp-a7f24.firebaseio.com",
    projectId: "pushnotificationapp-a7f24",
    storageBucket: "pushnotificationapp-a7f24.appspot.com",
    messagingSenderId: "390424072203",
    appId: "1:390424072203:web:742bd2eeac460818"
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
