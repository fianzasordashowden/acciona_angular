import {JsonProperty, JsonObject} from "json2typescript";

@JsonObject("UserLog")
export class UserLog {
    @JsonProperty('username', String)
    username: string = undefined;
    @JsonProperty('fecha', String)
    fecha: String = undefined;
    @JsonProperty('aplicacion', String)
    aplicacion:  String = undefined;
    @JsonProperty('browser', String)
    browser:  String = undefined;
    @JsonProperty('device', String)
    device:  String = undefined;
    @JsonProperty('isDesktop', Boolean)
    isDesktop:  Boolean = undefined;
    @JsonProperty('isMobile', Boolean)
    isMobile:  Boolean = undefined;
    @JsonProperty('isTablet', Boolean)
    isTablet:  Boolean = undefined;
    @JsonProperty('os', Boolean)
    os:  String = undefined;
  }


  @JsonObject("PostInfoUser")
  export class PostInfoUser {
    @JsonProperty('userName', String)
    userName: string = undefined;
    @JsonProperty('dateAlta', Date)
    dateAlta: Date = undefined;
    @JsonProperty('idArea', Number)
    idArea:  Number = undefined;
    @JsonProperty('cvePerfil', Number)
    cvePerfil:  Number = undefined;
    @JsonProperty('nameRol', String)
    nameRol:  String = undefined;
    @JsonProperty('idGrupo', Number)
    idGrupo:  Number = undefined;
    @JsonProperty('idFiado', Number)
    idFiado:  Number = undefined;
    @JsonProperty('ejecutivo', String)
    ejecutivo:  String = undefined;
    @JsonProperty('eMail', String)
    eMail:  String = undefined;
    @JsonProperty('idUsuario', Number)
    idUsuario:  Number = undefined;
    @JsonProperty('idParam', Number)
    idParam:  Number = undefined;
      
  }
