import {JsonProperty, JsonObject} from "json2typescript";

import { FilesSolNuevaDTO } from "./filesSolNuevaDTO";


@JsonObject("solicitudComentarioSolDTO")
export class solicitudComentarioSolDTO {
    @JsonProperty('userId', String)
    userId:         number = undefined
    @JsonProperty('numSol', String)
    numSol:      number = undefined
    @JsonProperty('desComent', String)
    desComent:     number = undefined     
    @JsonProperty('FileComentDTO', [FilesSolNuevaDTO])
    FileComentDTO:  FilesSolNuevaDTO [] = new Array<FilesSolNuevaDTO>()
   
}