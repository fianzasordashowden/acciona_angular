
import {JsonProperty, JsonObject, JsonConverter, JsonCustomConvert} from "json2typescript";
import { FilesSolNuevaDTO } from "./filesSolNuevaDTO";

@JsonObject("PostRegisterSolNueva")
export class PostRegisterSolNueva {
    @JsonProperty('userId', Number)
    userId:  number = undefined;
    @JsonProperty('userName', String)
    userName:  String = undefined;
    @JsonProperty('userDate', String)
    userDate:  String = undefined;
    @JsonProperty('ramoId', Number)
    ramoId:  number = undefined;
    @JsonProperty('subRamoId', Number)
    subRamoId:  number = undefined;
    @JsonProperty('paqId', Number)
    paqId:  number = undefined;
    @JsonProperty('tipMoviId', Number)
    tipMoviId:  number = undefined;
    @JsonProperty('clieId', Number)
    clieId:  number = undefined;
    @JsonProperty('afianId', Number)
    afianId:  number = undefined;
    @JsonProperty('gereId', Number)
    gereId:  number = undefined;
    @JsonProperty('nameBenef', Number)
    benfId:  number = undefined;
    @JsonProperty('benfId', String)
    nameBenef:  String = undefined;
    @JsonProperty('detSol', String)

    fiadoId:  number = undefined;
    @JsonProperty('fiadoId', String)
    fiadoNombre:  String = undefined;
    @JsonProperty('fiadoNombre', String)
    fiadoRFC:  String = undefined;
    @JsonProperty('fiadoRFC', String)
    fiadoRFCConf:  String = undefined;
    @JsonProperty('fiadoRFCConf', String)
    fiadoGrupo:  number = undefined;
    @JsonProperty('fiadoGrupo', Number)

    detSol:  String = undefined;
    @JsonProperty('rfcFia', String)
    rfcFia:  String = undefined;
    @JsonProperty('tipProy', Number)
    tipProy: number = undefined;
    @JsonProperty('filesSolNuevaDTO',  [FilesSolNuevaDTO])
    filesSolNuevaDTO: FilesSolNuevaDTO [] ;
    
    @JsonProperty('observaciones', String)
    observaciones: String = undefined;
  }

  @JsonObject("PostRegisterSolAct")
  export class PostRegisterSolAct {
      @JsonProperty('numSol', Number)
      numSol:  number = undefined;
      @JsonProperty('numFia', String)
      numFia:  String = undefined;
      @JsonProperty('idAfi', Number)
      idAfi:  number = undefined;
      @JsonProperty('dateVig', String)
      dateVig:  String = undefined;
      @JsonProperty('folioRec', String)
      folioRec:  String = undefined;
      @JsonProperty('dateRec', String)
      dateRec:  String = undefined;
      @JsonProperty('montoRec', Number)
      montoRec:  number = undefined;
      @JsonProperty('comRec', Number)
      comRec:  number = undefined;
      @JsonProperty('primaRec', Number)
      primaRec:  number = undefined;
      @JsonProperty('uuidRec', String)
      uuidRec:  String = undefined;
      @JsonProperty('relatFia', String)
      relatFia:  String = undefined;
      @JsonProperty('filesSolNuevaDTO',  [FilesSolNuevaDTO])
      filesSolNuevaDTO: FilesSolNuevaDTO [] ;
    }


    @JsonObject("PostRegisterContrato")
    export class PostRegisterContrato {
        @JsonProperty('ID_CONTR', Number)
        ID_CONTR:  number = undefined;
        @JsonProperty('NUM_CONTR', String)
        NUM_CONTR:  String = undefined;
        @JsonProperty('ID_FIA', Number)
        ID_FIA:  number = undefined;
        @JsonProperty('NUM_SOL', Number)
        NUM_SOL:  number = undefined;
        @JsonProperty('CVE_PAQUETE', Number)
        CVE_PAQUETE:  number = undefined;
        @JsonProperty('CVE_AFIAN', Number)
        CVE_AFIAN:  number = undefined;
        @JsonProperty('CVE_COMPANY', Number)
        CVE_COMPANY:  number = undefined;
        @JsonProperty('RFC_FIADO', String)
        RFC_FIADO:  String = undefined;
        @JsonProperty('RELA_A', String)
        RELA_A:  String = undefined;
        @JsonProperty('FEC_CONTR', String)
        FEC_CONTR:  String = null;
        @JsonProperty('FEC_CUMP_CONTR', String)
        FEC_CUMP_CONTR:  String = null;
        @JsonProperty('ID_USUARIO', Number)
        ID_USUARIO:  number = undefined;
        @JsonProperty('FilesContratoDTO',  [FilesSolNuevaDTO])
        FilesContratoDTO: FilesSolNuevaDTO [] ;
      }


    @JsonObject("PostRegisterFianza")
    export class PostRegisterFianza {
        @JsonProperty('ID_FIA', Number)
        ID_FIA:  number = undefined;
        @JsonProperty('NUM_SOL', Number)
        NUM_SOL:  number = undefined;
        @JsonProperty('NUM_FIA', String)
        NUM_FIA:  String = undefined;
        @JsonProperty('NUM_INC', String)
        NUM_INC:  String = undefined;
        @JsonProperty('CVE_AFIAN', Number)
        CVE_AFIAN:  number = undefined;
        @JsonProperty('CVE_PAQUETE', Number)
        CVE_PAQUETE:  number = undefined;
        @JsonProperty('FEC_VIG_INI_FIA', String)
        FEC_VIG_INI_FIA:  String = undefined;
        @JsonProperty('FEC_VIG_FIN_FIA', String)
        FEC_VIG_FIN_FIA:  String = undefined;
        @JsonProperty('FEC_CUM_FIA', String)
        FEC_CUM_FIA:  String = undefined;
        @JsonProperty('FEC_EMI_FIA', String)
        FEC_EMI_FIA:  String = undefined;
        @JsonProperty('ID_USUARIO', Number)
        ID_USUARIO:  number = undefined;
        @JsonProperty('ID_REC', Number)
        ID_REC:  number = undefined;
        @JsonProperty('CVE_PROV', Number)
        CVE_PROV:  number = undefined;
        @JsonProperty('CVE_GRUPO', Number)
        CVE_GRUPO:  number = undefined;
        @JsonProperty('CVE_BENEF', Number)
        CVE_BENEF:  number = undefined;
        @JsonProperty('MONTO_FIA', Number)
        MONTO_FIA:  number = undefined;
        @JsonProperty('PRIMA_TOTAL_FIA', Number)
        PRIMA_TOTAL_FIA:  number = undefined;
        @JsonProperty('STATUS', Number)
        STATUS:  number = undefined;
        @JsonProperty('FilesFianzaDTO',  [FilesSolNuevaDTO])
        FilesFianzaDTO: FilesSolNuevaDTO [] ;
      }


      @JsonObject("PostRegisterRecibo")
      export class PostRegisterRecibo {
        @JsonProperty('ID_REC', Number)
        ID_REC:  number = undefined;
        @JsonProperty('NUM_SOL', Number)
        NUM_SOL:  number = undefined;
        @JsonProperty('NUM_REC', String)
        NUM_REC:  String = undefined;
        @JsonProperty('CVE_AFIAN', Number)
        CVE_AFIAN:  number = undefined;
        @JsonProperty('UUID_REC', String)
        UUID_REC:  String = undefined;
        @JsonProperty('FEC_REC', String)
        FEC_REC:  String = undefined;
        @JsonProperty('PRIMA_NETA', Number)
        PRIMA_NETA:  number = undefined;
        @JsonProperty('COM_REC', Number)
        COM_REC:  number = undefined;
        @JsonProperty('STATUS_RECIBO', Number)
        STATUS_RECIBO:  number = undefined;
        @JsonProperty('TIPO_MONEDA', String)
        TIPO_MONEDA:  string = undefined;
        @JsonProperty('NUM_FIA', String)
        NUM_FIA:  String = undefined;
        @JsonProperty('ID_USUARIO', Number)
        ID_USUARIO:  number = undefined;
        @JsonProperty('FilesReciboDTO',  [FilesSolNuevaDTO])
        FilesReciboDTO: FilesSolNuevaDTO [] ;
      }
      
    
      @JsonObject("PostAnularSolicitud")
      export class PostAnularSolicitud {
        @JsonProperty('NUM_SOL', Number)
        NUM_SOL:  number = undefined;
        @JsonProperty('ID_USUARIO', Number)
        ID_USUARIO:  number = undefined;
      }
    
        
    