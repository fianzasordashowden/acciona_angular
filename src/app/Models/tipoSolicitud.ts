import {JsonProperty, JsonObject} from "json2typescript";
import {TipoDocumento} from './tipoDocu'

@JsonObject("TipoSolicitud")
export class TipoSolicitud {
    @JsonProperty('CVE_TIPO_SOL', Number)
    CVE_TIPO_SOL: number = undefined;
    @JsonProperty('DES_SOLICITUD', String)
    DES_SOLICITUD: String = undefined;
    @JsonProperty('lstTipDocumentos', [TipoDocumento])
    lstTipDocumentos:  TipoDocumento[] = new Array<TipoDocumento>()

  }

  @JsonObject("TipoSolicitudNew")
  export class TipoSolicitudNew {
      @JsonProperty('CVE_PAQUETE', Number)
      CVE_PAQUETE: number = undefined;
      @JsonProperty('CVE_RAMO', Number)
      CVE_RAMO: number = undefined;
      @JsonProperty('CVE_SUBRAMO', Number)
      CVE_SUBRAMO: number = undefined;
      @JsonProperty('CVE_AFIANZADORA', Number)
      CVE_AFIANZADORA: number = undefined;
      @JsonProperty('DES_PAQUETE', String)
      DES_PAQUETE: String = undefined;
      @JsonProperty('lstTipDocumentos', [TipoDocumento])
      lstTipDocumentos:  TipoDocumento[] = new Array<TipoDocumento>()
    }
  

