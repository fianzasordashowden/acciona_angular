import {JsonProperty, JsonObject} from "json2typescript";

import {TipoSolicitud, TipoSolicitudNew} from './tipoSolicitud'
import {TipoMovimiento} from './tipoMov'
import { Afianzadora, contratoSolDTO, fianzaSolDTO, reciboSolDTO, statusRecDTO, tipoMonedaRecDTO, contratoNotifDTO } from "./afianzadora";
import { Beneficiarios } from "./beneficiarios";
import { Fiados } from "./fiados";
import { fileFianza, porcentajeFiaDTO } from "./fileNewSol";
import { TipoRamo, TipoSubRamo, Gerencias, Proyectos } from "./fiaCore";

@JsonObject("solicitudFianzasDTO")
export class solicitudFianzasDTO {

    @JsonProperty('NUM_SOL', String)
    numSol:         number = undefined
    @JsonProperty('CVE_CLIENTE', String)
    cveCli:     number = undefined 
    @JsonProperty('CVE_TIPO_SOL', String)
    cveTipSol:    number = undefined
    @JsonProperty('DES_TIPO_SOL', String)
    desTipSol: string = undefined;
    @JsonProperty('CVE_STATUS_SOL', String)
    cveStatusSol:  number = undefined
    @JsonProperty('DES_STATUS_SOL', String)
    desStSol: string = undefined;
    @JsonProperty('FEC_SOLICITUD', String)
    fecSol: string = undefined;
    @JsonProperty('FEC_MOVIMIENTO', String)
    fecMov: string = undefined;
    @JsonProperty('OBSERVACIONES_SOL', String)
    obsvSol:   string = undefined
    @JsonProperty('VALOR_REFERENCIA1', String)
    numFianza:   string = undefined
    @JsonProperty('VALOR_REFERENCIA2', String)
    numControl:   string = undefined
    @JsonProperty('CVE_USUARIO', String)
    cveUsuario:     number = undefined 
    @JsonProperty('DES_USUARIO', String)
    desUsuSol: string = undefined;
    @JsonProperty('CVE_SOLICITANTE', String)
    cveSol: number = undefined
    @JsonProperty('CVE_CORPORATIVO', String)
    cveCorporativo: number = undefined 
    @JsonProperty('CVE_GRUPO', String)
    cveGrupo:       number = undefined 
    @JsonProperty('CVE_BENEF', String)
    cveBenef:       number = undefined 
    @JsonProperty('CVE_OPERACION', String)
    cveOperacion:   string = undefined
    @JsonProperty('VALOR_REFERENCIA3', String)
    ordCompra:   string = undefined
    @JsonProperty('VALOR_REFERENCIA4', String)
    numReciboOrd:   string = undefined
    @JsonProperty('VALOR_REFERENCIA5', String)
    numInclusion:   string = undefined
    @JsonProperty('CVE_SUBTIPO_SOL', String)
    cveSubTipoSol:    number = undefined 
    @JsonProperty('ACTIVO', String)
    activo:             number = undefined 
    @JsonProperty('VALOR_REFERENCIA6', String)
    numReciboBim:  string = undefined
}

JsonObject("solicitudNuevaDTO")
export class solicitudNuevaDTO {
    @JsonProperty('lstTipSolicitudes', [TipoSolicitud])
    lstTipSol:  TipoSolicitud [] = new Array<TipoSolicitud>()
    @JsonProperty('lstTipMovimiento', [TipoMovimiento])
    lstTipMovimiento:  TipoMovimiento [] = new Array<TipoMovimiento>()
    @JsonProperty('lstAfianzadoras', [Afianzadora])
    lstAfianzadoras:  Afianzadora [] = new Array<Afianzadora>()
    @JsonProperty('lstBeneficiarios', [Beneficiarios])
    lstBeneficiarios:  Beneficiarios [] = new Array<Beneficiarios>()
    
}

JsonObject("solicitudNuevaDTONew")
export class solicitudNuevaDTONew {
    
    @JsonProperty('lstTipRamos', [TipoSolicitud])
    lstTipRamos:  TipoRamo [] = new Array<TipoRamo>()
    @JsonProperty('lstTipSubRamos', [TipoSolicitud])
    lstTipSubRamos:  TipoSubRamo [] = new Array<TipoSubRamo>()
    @JsonProperty('lstTipSolicitudes', [TipoSolicitud])
    lstTipSol:  TipoSolicitudNew [] = new Array<TipoSolicitudNew>()
    @JsonProperty('lstTipMovimiento', [TipoMovimiento])
    lstTipMovimiento:  TipoMovimiento [] = new Array<TipoMovimiento>()
    @JsonProperty('lstAfianzadoras', [Afianzadora])
    lstAfianzadoras:  Afianzadora [] = new Array<Afianzadora>()
    @JsonProperty('lstBeneficiarios', [Beneficiarios])
    lstBeneficiarios:  Beneficiarios [] = new Array<Beneficiarios>()
    @JsonProperty('lstProyectos', [Proyectos])
    lstProyectos:  Proyectos [] = new Array<Proyectos>()
    @JsonProperty('lstFiados', [Fiados])
    lstFiados:  Fiados [] = new Array<Fiados>()
    
}

@JsonObject("solicitudAccionaFianzasDTO")
export class solicitudAccionaFianzasDTO {
    @JsonProperty('NUM_SOL', Number)
    NUM_SOL: number = undefined
    @JsonProperty('CVE_AFI', String)
    CVE_AFI:  number = undefined
    @JsonProperty('DES_AFI', String)
    DES_AFI:     number = undefined 
    @JsonProperty('CVE_CLIENTE', String)
    CVE_CLIENTE:     number = undefined 
    @JsonProperty('CVE_TIPO_SOL', String)
    CVE_TIPO_SOL:    number = undefined
    @JsonProperty('DESC_PAQUETE', String)
    DESC_PAQUETE: string = undefined;
    @JsonProperty('CVE_STATUS_SOL', String)
    CVE_STATUS_SOL:  number = undefined
    @JsonProperty('DES_STATUS_SOL', String)
    DES_STATUS_SOL: string = undefined;
    @JsonProperty('FEC_SOLICITUD', String)
    FEC_SOLICITUD: string = undefined;
    @JsonProperty('FEC_MOVIMIENTO', String)
    FEC_MOVIMIENTO: string = undefined;
    @JsonProperty('OBSERVACIONES_SOL', String)
    OBSERVACIONES_SOL:   string = undefined
    @JsonProperty('CVE_USUARIO', String)
    CVE_USUARIO: number = undefined 
    @JsonProperty('DESC_USUARIO', String)
    DESC_USUARIO: string = undefined;
    @JsonProperty('CVE_SOLICITANTE', Number)
    CVE_SOLICITANTE: number = undefined
    @JsonProperty('DES_SOLICITANTE', String)
    DES_SOLICITANTE: string = undefined;
    @JsonProperty('CVE_CORPORATIVO', Number)
    CVE_CORPORATIVO: number = undefined 
    @JsonProperty('CVE_GRUPO', Number)
    CVE_GRUPO:       number = undefined 
    @JsonProperty('CVE_BENEF', Number)
    CVE_BENEF: number = undefined
    @JsonProperty('DES_BENEF', String)
    DES_BENEF: String = undefined
    @JsonProperty('CVE_SUBTIPO_SOL', Number)
    CVE_SUBTIPO_SOL:    number = undefined 
    @JsonProperty('DES_SUBTIPO_SOL', String)
    DES_SUBTIPO_SOL: String = undefined
    @JsonProperty('ACTIVO', Number)
    ACTIVO: number = undefined 
    @JsonProperty('NUM_FIA', String)
    NUM_FIA:   string = undefined
    @JsonProperty('PRIMA_FIA', Number)
    PRIMA_FIA: number = undefined 
    @JsonProperty('FEC_CON_FIA', String)
    FEC_CON_FIA:   string = undefined
    @JsonProperty('FEC_VIG_FIA', String)
    FEC_VIG_FIA:   string = undefined
    @JsonProperty('FEC_CUM_FIA', String)
    FEC_CUM_FIA:   string = undefined
    @JsonProperty('NUM_REC_FIA', String)
    NUM_REC_FIA:   string = undefined
    @JsonProperty('UUID_REC', String)
    UUID_REC:   string = undefined
    @JsonProperty('FEC_REC', String)
    FEC_REC:   string = undefined
    @JsonProperty('MON_REC', Number)
    MON_REC: number = undefined 
    @JsonProperty('COM_REC', Number)
    COM_REC: number = undefined
    @JsonProperty('DES_GERE', String)
    DES_GERE:   string = undefined 
    @JsonProperty('FIADO_RFC', String)
    FIADO_RFC: String = undefined
    @JsonProperty('DESC_GERE', String)
    DESC_GERE: String = undefined
    @JsonProperty('VALOR_REFERENCIA_1', String)
    VALOR_REFERENCIA1:   string = undefined
    @JsonProperty('lstFilesFia', [fileFianza])
    lstFilesFia:  fileFianza [] = new Array<fileFianza>()
    @JsonProperty('lstDocsFia', [fileFianza])
    lstDocsFia:  fileFianza [] = new Array<fileFianza>()
   
}

@JsonObject("solicitudAccionaFianzasLst")
export class solicitudAccionaFianzasLst {
    @JsonProperty('NUM_SOL', Number)
    NUM_SOL: number = undefined
    @JsonProperty('CVE_AFIAN', Number)
    CVE_AFIAN: number = undefined
    @JsonProperty('DES_AFI', String)
    DES_AFI: String = undefined 
    @JsonProperty('CVE_PAQUETE', Number)
    CVE_PAQUETE: number = undefined
    @JsonProperty('DESC_PAQUETE', String)
    DESC_PAQUETE: string = undefined;
    @JsonProperty('DESC_STSOL', String)
    DESC_STSOL: string = undefined;
    @JsonProperty('CVE_GERE', Number)
    CVE_GERE: number = undefined
    @JsonProperty('DESC_GERE', String)
    DESC_GERE: string = undefined;
    @JsonProperty('CVE_BENEF', Number)
    CVE_BENEF: number = undefined
    @JsonProperty('DES_BENEF', String)
    DES_BENEF: String = undefined
    @JsonProperty('DESC_USUARIO', String)
    DESC_USUARIO: string = undefined;
    @JsonProperty('FIADO_RFC', String)
    FIADO_RFC: String = undefined
    @JsonProperty('FIADO_NOMBRE', String)
    FIADO_NOMBRE: String = undefined
    @JsonProperty('FEC_SOLICITUD', String)
    FEC_SOLICITUD: string = undefined;
    @JsonProperty('FEC_EMI_FIA', String)
    FEC_EMI_FIA: string = undefined;
    @JsonProperty('FEC_VIG_FIA', String)
    FEC_VIG_FIA:   string = undefined
    @JsonProperty('FEC_VIG_INI_FIA', String)
    FEC_VIG_INI_FIA:   string = undefined
    @JsonProperty('FEC_VIG_FIN_FIA', String)
    FEC_VIG_FIN_FIA:   string = undefined
    @JsonProperty('FEC_CUM_FIA', String)
    FEC_CUM_FIA:   string = undefined
    @JsonProperty('FEC_CONTR', String)
    FEC_CONTR:   string = undefined
    @JsonProperty('NUM_CONTR', String)
    NUM_CONTR:   string = undefined
    @JsonProperty('NUM_FIA', String)
    NUM_FIA:     number = undefined 
    @JsonProperty('RELA_CONTR', String)
    RELA_CONTR: String = undefined
    @JsonProperty('COM_FIA', Number)
    COM_FIA: number = undefined 
    @JsonProperty('MONTO_FIA', Number)
    MONTO_FIA: number = undefined 
    @JsonProperty('PRIMA_NETA_FIA', Number)
    PRIMA_NETA_FIA: number = undefined 
    @JsonProperty('DES_GERE', Number)
    PRIMA_TOTAL_FIA: number = undefined 
    @JsonProperty('STATUS_REC', String)
    STATUS_REC: String = undefined 
    @JsonProperty('DES_PROY_SOL', String)
    DES_PROY_SOL: String = undefined 
    @JsonProperty('DESCRIPCION_REC', String)
    DESCRIPCION_REC: String = undefined 
    @JsonProperty('FEC_CUMP_CONTR', String)
    FEC_CUMP_CONTR: String = undefined 
    @JsonProperty('OBSV_SOL', Number)
    OBSV_SOL: String = undefined  
    @JsonProperty('lstPorcFiaDto', [porcentajeFiaDTO])
    lstPorcFiaDto:  porcentajeFiaDTO [] = new Array<porcentajeFiaDTO>()
    
}

JsonObject("solExcelGO")
export class solExcelGO {
    @JsonProperty('Solicitud', Number)
    Solicitud: number = undefined
    @JsonProperty('Afianzadora', String)
    Afianzadora: String = undefined
    @JsonProperty('Fiador_RFC', String)
    Fiador_RFC: String = undefined
    @JsonProperty('Fiador_Proveedor', String)
    Fiador_Proveedor: String = undefined
    @JsonProperty('Beneficiario', String)
    Beneficiario: String = undefined
    @JsonProperty('Usuario', String)
    Usuario: String = undefined
    @JsonProperty('Estatus', String)
    Estatus: String = undefined
    @JsonProperty('Contrato', String)
    Contrato: String = undefined
    @JsonProperty('Fec_Contrato', String)
    Fec_Contrato: String = undefined
    @JsonProperty('Tipo_Fianza', String)
    Tipo_Fianza: String = undefined
    @JsonProperty('Fec_Solicitud', String)
    Fec_Solicitud: String = undefined
    @JsonProperty('Fec_Emision', String)
    Fec_Emision: String = undefined
    @JsonProperty('Fec_Vigencia_Ini', String)
    Fec_Vigencia_Ini: String = undefined
    @JsonProperty('Fec_Vigencia_Fin', String)
    Fec_Vigencia_Fin: String = undefined
    @JsonProperty('Relacion_A', String)
    Relacion_A: String = undefined
    @JsonProperty('Fec_Cumplimiento', String)
    Fec_Cumplimiento: String = undefined
    @JsonProperty('Monto_Fia', Number)
    Monto_Fia: Number = undefined
    @JsonProperty('Comision_Fia', Number)
    Comision_Fia: Number = undefined
    @JsonProperty('Prima_Neta_Fia', Number)
    Prima_Neta_Fia: Number = undefined
    @JsonProperty('Prima_Total_Fia', Number)
    Prima_Total_Fia: Number = undefined  
    // @JsonProperty('OBSVSERVACIONES_SOL', Number)
    // OBSVSERVACIONES_SOL: String = undefined  

}

JsonObject("solExcelAcc")
export class solExcelAcc {
    @JsonProperty('Solicitud', Number)
    Solicitud: number = undefined
    @JsonProperty('Afianzadora', String)
    Afianzadora: String = undefined
    @JsonProperty('Fiador_Proveedor', String)
    Fiador_Proveedor: String = undefined
    @JsonProperty('Beneficiario', String)
    Beneficiario: String = undefined
    @JsonProperty('Usuario', String)
    Usuario: String = undefined
    @JsonProperty('Estatus', String)
    Estatus: String = undefined
    @JsonProperty('Contrato', String)
    Contrato: String = undefined
    @JsonProperty('Fec_Contrato', String)
    Fec_Contrato: String = undefined
    @JsonProperty('Tipo_Fianza', String)
    Tipo_Fianza: String = undefined
    @JsonProperty('Fec_Solicitud', String)
    Fec_Solicitud: String = undefined
    @JsonProperty('Fec_Emision', String)
    Fec_Emision: String = undefined
    @JsonProperty('Fec_Vigencia_Ini', String)
    Fec_Vigencia_Ini: String = undefined
    @JsonProperty('Fec_Vigencia_Fin', String)
    Fec_Vigencia_Fin: String = undefined
    @JsonProperty('Relacion_A', String)
    Relacion_A: String = undefined
    @JsonProperty('Fec_Cumplimiento', String)
    Fec_Cumplimiento: String = undefined
    @JsonProperty('Monto_Fia', Number)
    Monto_Fia: Number = undefined
    // @JsonProperty('Comision_Fia', Number)
    // Comision_Fia: Number = undefined
    // @JsonProperty('Prima_Neta_Fia', Number)
    // Prima_Neta_Fia: Number = undefined
    @JsonProperty('Prima_Total_Fia', Number)
    Prima_Total_Fia: Number = undefined  
}

JsonObject("fiaExcelAcc")
export class fiaExcelAcc {
    @JsonProperty('Contrato', String)
    Contrato: String = undefined
    @JsonProperty('Fec_Contrato', String)
    Fec_Contrato: String = undefined
    @JsonProperty('Fianza', String)
    Fianza: number = undefined
    @JsonProperty('Tipo_Fianza', String)
    Tipo_Fianza: String = undefined
    @JsonProperty('Afianzadora', String)
    Afianzadora: String = undefined
    @JsonProperty('Fiado_RFC', String)
    Fiado_RFC: String = undefined
    @JsonProperty('Fiado_Proveedor', String)
    Fiado_Proveedor: String = undefined
    @JsonProperty('Fec_Solicitud', String)
    Fec_Solicitud: String = undefined
    @JsonProperty('Fec_Emision', String)
    Fec_Emision: String = undefined
    @JsonProperty('Fec_Vigencia_Ini', String)
    Fec_Vigencia_Ini: String = undefined
    @JsonProperty('Fec_Vigencia_Fin', String)
    Fec_Vigencia_Fin: String = undefined
    @JsonProperty('Fec_Cumplimiento', String)
    Fec_Cumplimiento: String = undefined
    @JsonProperty('Beneficiario', String)
    Beneficiario: String = undefined
    @JsonProperty('Monto_Fia', Number)
    Monto_Fia: Number = undefined
    @JsonProperty('Prima_Total_Fia', Number)
    Prima_Total_Fia: Number = undefined  
    @JsonProperty('Usuario', String)
    Usuario: String = undefined
    @JsonProperty('Estatus', String)
    Estatus: String = undefined
    @JsonProperty('Relacion_A', String)
    Relacion_A: String = undefined
    
}

JsonObject("fiaVenExcelAcc")
export class fiaVenExcelAcc {
    @JsonProperty('Fianza', String)
    Fianza: number = undefined
    @JsonProperty('Monto_Fia', Number)
    Monto_Fia: Number = undefined
    @JsonProperty('Fiador_Proveedor', String)
    Fiador_Proveedor: String = undefined
    @JsonProperty('Beneficiario', String)
    Beneficiario: String = undefined
    @JsonProperty('Fec_Cump_Contrato', String)
    Fec_Cump_Contrato: String = undefined
    @JsonProperty('Fec_Vigencia_Fianza', String)
    Fec_Vigencia_Fianza: String = undefined

}



@JsonObject("solicitudDetalleFiaDTO")
export class solicitudDetalleFiaDTO {
    @JsonProperty('NUM_SOL', Number)
    NUM_SOL: number = undefined
    @JsonProperty('DES_STATUS_SOL', String)
    DES_STATUS_SOL:  String = undefined
    @JsonProperty('ID_FIA', Number)
    ID_FIA:     number = undefined 
    @JsonProperty('CONTRAC_SOL', [contratoSolDTO])
    CONTRAC_SOL:  contratoSolDTO [] = new Array<contratoSolDTO>()
    @JsonProperty('FIANZA_SOL', [fianzaSolDTO])
    FIANZA_SOL:  fianzaSolDTO [] = new Array<fianzaSolDTO>()
    @JsonProperty('RECIBO_SOL', [reciboSolDTO])
    RECIBO_SOL:  reciboSolDTO [] = new Array<reciboSolDTO>()
    @JsonProperty('lstFilesFia', [fileFianza])
    lstFilesFia:  fileFianza [] = new Array<fileFianza>()
    @JsonProperty('lstDocsFia', [fileFianza])
    lstDocsFia:  fileFianza [] = new Array<fileFianza>()
    @JsonProperty('lstStatusRec', [statusRecDTO])
    lstStatusRec:  statusRecDTO [] = new Array<statusRecDTO>()
    @JsonProperty('lstTipMonRec', [tipoMonedaRecDTO])
    lstTipMonRec:  tipoMonedaRecDTO [] = new Array<tipoMonedaRecDTO>()
    @JsonProperty('lstCancelacionFia', [fileFianza])                    // #Cancelaciones -> Ahernandezh 02/02/2021
    lstCancelacionFia:  fileFianza [] = new Array<fileFianza>()         // #Cancelaciones -> Ahernandezh 02/02/2021
}

@JsonObject("contracNotifiDTO")
export class contracNotifiDTO {
    @JsonProperty('lstContratosWarning', [contratoNotifDTO])
    lstContratosWarning:  contratoNotifDTO [] = new Array<contratoNotifDTO>()
    @JsonProperty('lstContratosError', [statusRecDTO])
    lstContratosError:  contratoNotifDTO [] = new Array<contratoNotifDTO>()
}
