import {JsonProperty, JsonObject} from "json2typescript";

@JsonObject("Afianzadora")
export class Afianzadora {
    @JsonProperty('CVE_AFIAN', Number)
    cveAfianz: number = undefined;
    @JsonProperty('NOM_AFIAN', String)
    nomAfianz: String = undefined;
    @JsonProperty('RFC_AFIAN', String)
    rfcAfianz: String = undefined;
    @JsonProperty('NOMBRE_LOGO', String)
    logoAfianz: String = undefined;
    @JsonProperty('ID_GRUPO', Number)
    idGrupo: number = undefined;
  }


  @JsonObject("contratoSolDTO")
  export class contratoSolDTO {
      @JsonProperty('NUM_CONTR', String)
      NUM_CONTR: String = undefined;
      @JsonProperty('RFC_FIADO', String)
      RFC_FIADO: String = undefined;
      @JsonProperty('RELA_A', String)
      RELA_A: String = undefined;
      @JsonProperty('FEC_CONTR', String)
      FEC_CONTR: String = undefined;
      @JsonProperty('FEC_CUMP_CONTR', String)
      FEC_CUMP_CONTR: String = undefined;
      @JsonProperty('DES_GERENCIA', String)
      DES_GERENCIA: String = undefined;
    }

  @JsonObject("fianzaSolDTO")
  export class fianzaSolDTO {
    @JsonProperty('NUM_FIA', String)
    NUM_FIA: String = undefined;
    @JsonProperty('NUM_INC', String)
    NUM_INC: String = undefined;
    @JsonProperty('DESC_AFIAN', String)
    DESC_AFIAN: String = undefined;
    @JsonProperty('TIPO_FIANZA', String)
    TIPO_FIANZA: String = undefined;
    @JsonProperty('DESC_BENEF', String)
    DESC_BENEF: String = undefined;
    @JsonProperty('FEC_EMI_FIA', String)
    FEC_EMI_FIA: String = undefined;
    @JsonProperty('FEC_VIG_INI_FIA', String)
    FEC_VIG_INI_FIA: String = undefined;
    @JsonProperty('FEC_VIG_FIN_FIA', String)
    FEC_VIG_FIN_FIA: String = undefined;
    @JsonProperty('FEC_CUM_FIA', String)
    FEC_CUM_FIA: String = undefined;
    @JsonProperty('MONTO_FIA', Number)
    MONTO_FIA: number = undefined;
    @JsonProperty('PRIMA_TOTAL_FIA', Number)
    PRIMA_TOTAL_FIA: number = undefined;
    @JsonProperty('STATUS', String)
    STATUS: String = undefined;
  }

  @JsonObject("reciboSolDTO")
  export class reciboSolDTO {
      @JsonProperty('NUM_REC', String)
      NUM_REC: String = undefined;
      @JsonProperty('UUID_REC', String)
      UUID_REC: String = undefined;
      @JsonProperty('FEC_REC', String)
      FEC_REC: String = undefined;
      @JsonProperty('PRIMA_NETA', Number)
      PRIMA_NETA: number = undefined;
      @JsonProperty('COM_REC', Number)
      FEC_CUMP_CONTR: number = undefined;
      @JsonProperty('STATUS_RECIBO', String)
      STATUS_RECIBO: String = undefined;
    }

  @JsonObject("statusRecDTO")
  export class statusRecDTO {
    @JsonProperty('CVE_STATUS_REC', Number)
    CVE_STATUS_REC: number = undefined;
    @JsonProperty('DESC_STATUS', String)
    DESC_STATUS: String = undefined;
  }


  @JsonObject("tipoMonedaRecDTO")
  export class tipoMonedaRecDTO {
    @JsonProperty('CVE_MONEDA', Number)
    CVE_MONEDA: number = undefined;
    @JsonProperty('DESC_MONEDA', String)
    DESC_MONEDA: String = undefined;
  }

  @JsonObject("contratoNotifDTO")
  export class contratoNotifDTO {
    @JsonProperty('ID_CONTR', Number)
    ID_CONTR: number = undefined;
    @JsonProperty('NUM_CONTR', String)
    NUM_CONTR: String = undefined;
    @JsonProperty('RFC_FIADO', String)
    RFC_FIADO: String = undefined;
    @JsonProperty('RELA_A', String)
    RELA_A: String = undefined;
    @JsonProperty('FEC_CONTR', String)
    FEC_CONTR: String = undefined;
    @JsonProperty('FEC_CUMP_CONTR', String)
    FEC_CUMP_CONTR: String = undefined;
    @JsonProperty('DES_GERENCIA', String)
    DES_GERENCIA: String = undefined;
    @JsonProperty('NUM_SOL', Number)
    NUM_SOL: number = undefined;
    @JsonProperty('ID_GRUPO', Number)
    ID_GRUPO: number = undefined;
}