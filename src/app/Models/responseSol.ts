

import {JsonProperty, JsonObject} from "json2typescript";
import { solicitudFianzasDTO, solicitudNuevaDTO } from "./solicitudes";
import { SolicitudNuevaComponent } from "../SolicitudesFianzas/solicitud-nueva/solicitud-nueva.component";


@JsonObject("Response")
export class ResponseSol {
    @JsonProperty('codeResult', Number)
    codResult: number = undefined
    @JsonProperty('result ', String)
    resultSt: string = undefined
    @JsonProperty('solicitudesFianzasDTO', [solicitudFianzasDTO])
    solFianLst: solicitudFianzasDTO[] = undefined
}

@JsonObject("Response")
export class ResponseNewSol {
    @JsonProperty('codeResult', Number)
    codResult: number = undefined
    @JsonProperty('result ', String)
    resultSt: string = undefined
    @JsonProperty('solicitudNuevaDTO', [solicitudNuevaDTO])
    solNew: solicitudNuevaDTO = new solicitudNuevaDTO()
}
