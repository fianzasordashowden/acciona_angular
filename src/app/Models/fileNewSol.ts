import {JsonProperty, JsonObject} from "json2typescript";
import { Client } from "./clienteToken";

@JsonObject("fileData")
export class fileData {
    @JsonProperty('nameFile', String)
    nameFile: String = "";
    @JsonProperty('extFile', String)
    extFile: String = "";
    @JsonProperty('sizeFile', String)
    sizeFile: String = "";
    @JsonProperty('typeFile', String)
    typeFile: String = "";
    @JsonProperty('dataFile', String)
    dataFile: String = "";
  }


@JsonObject("fileFianza")
export class fileFianza {
    @JsonProperty('NUM_SOL', Number)
    NUM_SOL: number = undefined;
    @JsonProperty('ID_TIPO', Number)
    ID_TIPO: number = undefined;
    @JsonProperty('ID_DOCUMENTO', Number)
    ID_DOCUMENTO: number = undefined;
    @JsonProperty('NOM_DOCUMENTO', String)
    NOM_DOCUMENTO: String = undefined;
    @JsonProperty('FS_DOCUMENTO', String)
    FS_DOCUMENTO: String = undefined;
    @JsonProperty('DES_TIP', String)
    DES_TIP: String = undefined;
    @JsonProperty('CONTEXTO', String)
    CONTEXTO: String = undefined;
    @JsonProperty('b_DOCUMENTO', String)
    b_DOCUMENTO: String = undefined;
    @JsonProperty('s_DOCUMENTO', String)
    s_DOCUMENTO: String = undefined;
  }


  @JsonObject("porcentajeFiaDTO")
  export class porcentajeFiaDTO {
      @JsonProperty('DESC_STATUS', String)
      DESC_STATUS: String = undefined;
      @JsonProperty('TOT_PORCENTAJE', Number)
      TOT_PORCENTAJE: number = undefined;
   }