import {JsonProperty, JsonObject} from "json2typescript";

@JsonObject("TipoRamo")
export class TipoRamo {
    @JsonProperty('CVE_RAMO', Number)
    CVE_RAMO: number = undefined;
    @JsonProperty('DES_RAMO', String)
    DES_RAMO: String = undefined;
  }


@JsonObject("TipoSubRamo")
export class TipoSubRamo {
    @JsonProperty('CVE_SUBRAMO', Number)
    CVE_SUBRAMO: number = undefined;
    @JsonProperty('CVE_RAMO', Number)
    CVE_RAMO: number = undefined;
    @JsonProperty('DES_SUBRAMO', String)
    DES_SUBRAMO: String = undefined;
}

@JsonObject("Gerencias")
export class Gerencias {
    @JsonProperty('CVE_GERE', Number)
    CVE_GERE: number = undefined;
    @JsonProperty('DESC_GERE', String)
    DESC_GERE: String = undefined;
}


@JsonObject("Proyectos")
export class Proyectos {
    @JsonProperty('ID_PROY', Number)
    ID_PROY: number = undefined;
    @JsonProperty('DESC_PROY', String)
    DESC_PROY: String = undefined;
    @JsonProperty('ACTIVO', String)
    ACTIVO: String = undefined;
}
