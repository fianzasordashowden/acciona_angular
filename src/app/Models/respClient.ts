import { ClientData } from "./data";
import { authCli } from "./authUsu";
import {JsonProperty, JsonObject} from "json2typescript";

@JsonObject("ResponseUsu")
export class ResponseUsu {
    @JsonProperty('Data', [ClientData])
    Data: ClientData  = new ClientData();
    @JsonProperty('Auth', [authCli])
    Auth: authCli  = new authCli();
  }
