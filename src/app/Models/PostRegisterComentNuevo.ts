
import {JsonProperty, JsonObject, JsonConverter, JsonCustomConvert} from "json2typescript";
import { FileComentDTO } from "./FileComentDTO";
import { FilesSolNuevaDTO } from "./filesSolNuevaDTO";

@JsonObject("PostRegisterComentNuevo")
export class PostRegisterComentNuevo 
{
    @JsonProperty('userId', Number)
    userId:  number = undefined;
    @JsonProperty('numSol', Number)
    numSol:  Number = undefined;
    @JsonProperty('idDoc', Number)
    idDoc:  Number = undefined;
    @JsonProperty('desComent', String)
    desComent:  String = undefined;
    @JsonProperty('fileComentDTO',  [FileComentDTO])
    fileComentDTO: FileComentDTO [] ;
    @JsonProperty('FilesFianzaDTO',  [FilesSolNuevaDTO])  // #Cancelaciones -> Ahernandezh 02/02/2021 
    FilesFianzaDTO: FilesSolNuevaDTO [] ;                 // #Cancelaciones -> Ahernandezh 02/02/2021
  }