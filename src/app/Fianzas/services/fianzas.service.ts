import { Injectable } from '@angular/core';

//Import variables globales
import { rootGlobals } from 'src/app/rootGlobals';

//IPR Peticiones Rest
import { HttpClient, HttpResponse, HttpHeaders, HttpParams, HttpErrorResponse,} from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';

import { map, catchError, tap } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import {JsonConvert, OperationMode, ValueCheckingMode} from "json2typescript"

//Models
import { solicitudAccionaFianzasDTO } from 'src/app/Models/solicitudes';
import { SecurityService } from 'src/app/Servicios/security.service';

//Prueba IPR
import { NgxSpinnerService } from 'ngx-spinner';
import { PostInfoUser } from 'src/app/Models/userLog';

import Swal from 'sweetalert2';
//import moment = require('moment');

@Injectable({
  providedIn: 'root'
})
export class FianzasService {

  //FGonzalez: declaramos la variable que usaremos para alamacenar el token
  tokenUsuFia: any;  

  // Asiganamos nuestras URL's de nuestros servicios
  private urlGetFianzas:        string = "Solicitudes/GetFianzasAcc";
  private urlGetRptFianzas:     string = "Reportes/GetRptFianzasAcc";
  private urlGetRptFianzasVen:  string = "Solicitudes/GetFianzasVenAcc";
  private urlReportExcFia:      string = "Reportes/RptFiAcc";
  

  constructor(
    private http: HttpClient,
    private varGlob: rootGlobals,
    private securityServices: SecurityService,
    //Pruebas IPR
    private spinner: NgxSpinnerService,
    // Traemos nustras variables globales
    public globals: rootGlobals,
    ) {       
      // FGonzalez: Lo primero que se hace es validar si la sessión esta activa.
      // En caso de que no la misma función validateSession() nos redireccionara al login
      this.securityServices.validateSession();              
    }  


  getAllFianzas(dataForm: PostInfoUser): Observable<any> {

    try{
      // FGonzalez: Armamos el token con la variable de sesion
      this.tokenUsuFia = JSON.parse(sessionStorage.getItem('TokenUsuFia'))._value;

      //Url del servicio
      let urlGetFianzas: string = this.globals.GetUrlServicio() + this.urlGetFianzas;

      let jsonCrt = new JsonConvert();
      jsonCrt.operationMode = OperationMode.LOGGING; // print some debug data
      jsonCrt.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
      jsonCrt.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null

      // Armamos los headers
      // var headers = new HttpHeaders()
      // .set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
      // .set('Authorization', this.tokenUsuFia)
      // .set('Accept', 'application/json');

      var headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', this.tokenUsuFia)
      .set('Accept', 'application/json');


      // Armamos el body para este servicio ira vacio
      //const body = new HttpParams().toString();

      // return this.http
      // .post<solicitudAccionaFianzasDTO>(
      //   urlGetSolicitudes, 
      //   body,    
      //   {headers: headers}    
      // )
      // .pipe(map(data => data));  

      // Armamos el body con los datos del formulario
      const body =  dataForm;

      return this.http
      // .post<solicitudComentarioSolDTO>(
      .post<any>(
        urlGetFianzas, 
         body,    
        {headers: headers}    
      )
      .pipe(map(data => data));

      
    }catch(e) { 
      this.securityServices.clearSession();
    }

  
  }


  ReportFianzas(dataForm: PostInfoUser): Observable<any> {
     
    try {
      // FGonzalez: Armamos el token con la variable de sesion
      this.tokenUsuFia = JSON.parse(sessionStorage.getItem('TokenUsuFia'))._value;
      
      //cve perfil
      let cve_perfil = JSON.parse(JSON.parse(sessionStorage.getItem('infoUser'))._value).CVE_PERFIL;

      //Url del servicio
      //let urlReportPDFSol: string = this.globals.GetUrlServicio() + this.urlGetRptFianzas + "?_num_rpt=" + num_rpt;
      let urlReportPDFSol: string = this.globals.GetUrlServicio() + this.urlGetRptFianzas;

      let jsonCrt = new JsonConvert();
      jsonCrt.operationMode = OperationMode.LOGGING; // print some debug data
      jsonCrt.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
      jsonCrt.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null

      // Armamos los headers
      // var headers = new HttpHeaders()
      // .set('Content-Type', 'application/json')
      // .set('Authorization', this.tokenUsuFia)
      // .set('Accept', 'application/json');
      var headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', this.tokenUsuFia)
      .set('Accept', 'application/json');


      // Armamos el body para este servicio ira vacio
      //const body = new HttpParams().toString();
      const body =  dataForm;

      return this.http
      .post<any>(
        urlReportPDFSol, 
        body,    
        {headers: headers}    
      )
      .pipe(map(data => data));

    }catch(e) { 
      this.securityServices.clearSession();
    } 
  }  


  getAllFianzasVencidas(dataForm: PostInfoUser): Observable<any> {

    try{
      // FGonzalez: Armamos el token con la variable de sesion
      this.tokenUsuFia = JSON.parse(sessionStorage.getItem('TokenUsuFia'))._value;

      //Url del servicio
      let urlGetFianzas: string = this.globals.GetUrlServicio() + this.urlGetRptFianzasVen;

      var headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', this.tokenUsuFia)
      .set('Accept', 'application/json');

      // Armamos el body con los datos del formulario
      const body =  dataForm;

      return this.http
      // .post<solicitudComentarioSolDTO>(
      .post<any>(
        urlGetFianzas, 
         body,    
        {headers: headers}    
      )
      .pipe(map(data => data));

      
    }catch(e) { 
      this.securityServices.clearSession();
    }

  
  }


  ReportPDFFia(): Observable<any> {
     
    try {
      // FGonzalez: Armamos el token con la variable de sesion
      this.tokenUsuFia = JSON.parse(sessionStorage.getItem('TokenUsuFia'))._value;
      
      //cve perfil
      let cve_perfil = JSON.parse(JSON.parse(sessionStorage.getItem('infoUser'))._value).CvePerfil;

      //Url del servicio
      let urlRptExSol: string = this.globals.GetUrlServicio() + this.urlReportExcFia;

      let jsonCrt = new JsonConvert();
      jsonCrt.operationMode = OperationMode.LOGGING; // print some debug data
      jsonCrt.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
      jsonCrt.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null

      // Armamos los headers
      var headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', this.tokenUsuFia)
      .set('Accept', 'application/json');

      // Armamos el body para este servicio ira vacio
      const body = new HttpParams().toString();

      return this.http
      .post<any>(
        urlRptExSol, 
        body,    
        {headers: headers}    
      )
      .pipe(map(data => data));

    }catch(e) { 
      this.securityServices.clearSession();
    } 
  }  


  downloadExcel(pdf, nomArchivo) {
    const linkSource = `data:application/vnd.ms-excel,${pdf}`;
    const downloadLink = document.createElement("a");
    const fileName = nomArchivo;
    //const fileName = nomArchivo + moment().format("DD/MM/YYYY") +".pdf";
    downloadLink.href = linkSource;
    downloadLink.download = fileName;
    downloadLink.click();
  }


  getReporte(): Observable<Blob> 
  {
    try {
      
      var headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', this.tokenUsuFia)
      .set('Accept', 'application/json');

      let urlReportExcSol: string = this.globals.GetUrlServicio() + this.urlReportExcFia;

      return this.http
      .post(
        urlReportExcSol, 
        '',    
        {
          headers: headers,
          responseType: 'blob'
        }    
      )
      .map((x) => {
        return x;
      });
      
    } catch (e) {

    }   

    

  }


}
