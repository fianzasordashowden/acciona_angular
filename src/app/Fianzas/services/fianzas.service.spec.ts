import { TestBed, inject } from '@angular/core/testing';

import { FianzasService } from './fianzas.service';

describe('FianzasService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FianzasService]
    });
  });

  it('should be created', inject([FianzasService], (service: FianzasService) => {
    expect(service).toBeTruthy();
  }));
});
