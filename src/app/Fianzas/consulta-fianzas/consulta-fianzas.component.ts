import { Component, OnInit, ViewChild, Input, SimpleChanges, ChangeDetectionStrategy } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource, MatDialog} from '@angular/material';
import { ActivatedRoute } from '@angular/router';

//Tools
import { ConfirmationService } from 'primeng/primeng';
import { NgxSpinnerService } from 'ngx-spinner';
import * as XLSX from 'xlsx';
import Scene from "scenejs";
import * as moment from 'moment';
import * as FileSaver from 'file-saver';

//Servicios
import { FianzasService } from 'src/app/Fianzas/services/fianzas.service';
import { SecurityService } from 'src/app/Servicios/security.service';

//Models
import { solicitudFianzasDTO, solicitudAccionaFianzasDTO, solExcelGO, solicitudAccionaFianzasLst, solicitudDetalleFiaDTO, solExcelAcc, fiaExcelAcc } from 'src/app/Models/solicitudes';

//Components
import { fileFianza, porcentajeFiaDTO } from 'src/app/Models/fileNewSol';
import { DomSanitizer } from '@angular/platform-browser';
import { PostInfoUser } from 'src/app/Models/userLog';

import Swal from 'sweetalert2'

@Component({
  selector: 'app-consulta-fianzas',
  templateUrl: './consulta-fianzas.component.html',
  styleUrls: ['./consulta-fianzas.component.scss']
})
export class ConsultaFianzasComponent implements OnInit {

  nameTit: string = "Fianzas";
  solicitudesLst: solicitudAccionaFianzasLst[];
  porcFiaLst : porcentajeFiaDTO[];
  solDetFianza: solicitudDetalleFiaDTO;
  fileDocsLst: Array<fileFianza> = [];
  fileFianzaLst: Array<fileFianza> = [];
  show:boolean = false;
  RptExcel: Array<fiaExcelAcc> = [];
  id_rpt : number = 0;
  infUser: PostInfoUser = new PostInfoUser();
  excelSrc: any = '';

  displayedColumns: string[] = ['NUM_FIA', 'NUM_REC', 'DESC_PAQUETE', 'FEC_VIG_INI_FIA', 'DES_BENEF', 'DESCRIPCION_REC'];
  dataSourseFia = new MatTableDataSource();

  @Input() childStatusSol: string;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  constructor(
    private fianzasService: FianzasService,
    private securityServices: SecurityService,
    private confirmationService: ConfirmationService,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private sanitizer: DomSanitizer,
  ) { }

  ngOnInit() {

    this.dataSourseFia.paginator = this.paginator;
    this.dataSourseFia.sort = this.sort;

    this.getAllFianzas(this.childStatusSol);
    document.getElementById("push-menu").click();

  }


  applyFilter(filterValue: string) {
    this.dataSourseFia.filter = filterValue.trim().toLowerCase();
    if (this.dataSourseFia.paginator) {
      this.dataSourseFia.paginator.firstPage();
    }
  }

  private refreshTable() {
    this.paginator._changePageSize(this.paginator.pageSize);
  }


//********************************************************************************/

  //Metodos Solicitudes
  getAllFianzas(statusSol) {

    this.spinner.show();

    let JsonUsu =  JSON.parse(JSON.parse(sessionStorage.getItem('infoUser'))._value);

    this.infUser.cvePerfil = JsonUsu.CvePerfil;
    this.infUser.nameRol = JsonUsu.Rol;
    this.infUser.idArea = JsonUsu.area;
    this.infUser.ejecutivo = JsonUsu.ejecutivo;
    this.infUser.dateAlta =  moment(JsonUsu.fechaAlta, 'DD-MM-YYYY').toDate();
    this.infUser.idUsuario = JsonUsu.idUsuario;
    this.infUser.idGrupo = JsonUsu.id_grupo;
    this.infUser.idFiado = JsonUsu.idFiado;
    this.infUser.eMail = JsonUsu.mail;
    this.infUser.userName = JsonUsu.nombreCompleto;
    this.infUser.idParam = 0;



    this.fianzasService.getAllFianzas(this.infUser).subscribe(
      data => {
        var prueba = data;
        console.log(prueba);
        this.solicitudesLst = data.fianzasAccDTO.lstFianzasDTO;
        this.porcFiaLst = data.fianzasAccDTO.lstPorcFiaDto;

        this.porcFiaLst.sort(function (a, b) {
          var textA = a.DESC_STATUS.toUpperCase();
          var textB = b.DESC_STATUS.toUpperCase();
          return textA.localeCompare(textB);
        });

        this.dataSourseFia.data =  data.fianzasAccDTO.lstFianzasDTO;

        this.spinner.hide();

      },
      // Si ducede algun error se limpia la sessión y redirige al login
      error => {
        this.spinner.hide();
        this.securityServices.clearSession();
        console.log('oops: ', error)
      }
    );


  }

  export() {

    this.spinner.show();

      //Cargar los documentos de la solicitud
      this.fianzasService.getReporte().subscribe(
        data => {
         
          let nombre = "FianzasAcc.xls";
          let file: string = "";

          const blob = new Blob([data], {type: nombre} );
          this.spinner.hide();
          file = window.URL.createObjectURL(blob);

          return FileSaver.saveAs(blob, nombre);
         
        },
        // Si ducede algun error se limpia la sessión y redirige al login
        error => {
          this.spinner.hide();
          this.securityServices.clearSession();
          console.log('oops: ', error)
        }
      );


  }


  rptFianzas(rpt) {

    this.RptExcel = [];
    let nombre_archivo = "";
    this.spinner.show();
    this.id_rpt = rpt;

    let JsonUsu =  JSON.parse(JSON.parse(sessionStorage.getItem('infoUser'))._value);

    this.infUser.cvePerfil = JsonUsu.CvePerfil;
    this.infUser.nameRol = JsonUsu.Rol;
    this.infUser.idArea = JsonUsu.area;
    this.infUser.ejecutivo = JsonUsu.ejecutivo;
    this.infUser.dateAlta =  moment(JsonUsu.fechaAlta, 'DD-MM-YYYY').toDate();
    this.infUser.idUsuario = JsonUsu.idUsuario;
    this.infUser.idGrupo = JsonUsu.id_grupo;
    this.infUser.idFiado = JsonUsu.idFiado;
    this.infUser.eMail = JsonUsu.mail;
    this.infUser.userName = JsonUsu.nombreCompleto;
    this.infUser.idParam = this.id_rpt;



    //Cargar los documentos de la solicitud
    this.fianzasService.ReportFianzas(this.infUser).subscribe(
      data => {

        var prueba = data;
        console.log(prueba);
        this.solicitudesLst = data.fianzasAccDTO.lstFianzasDTO;
        this.dataSourseFia.data =  data.fianzasAccDTO.lstFianzasDTO;

        this.solicitudesLst.forEach( (sol) => {
        let element;
          element = new fiaExcelAcc();
          element.Contrato = ( (sol.NUM_CONTR == null) ? 'n/a' : sol.NUM_CONTR ) ;
          element.Fec_Contrato = (sol.FEC_CONTR == null) ? 'n/a' : ((moment(sol.FEC_CONTR.toString()).locale('es').format('L').toString() == 'Invalid date' ? 'n/a' : moment(sol.FEC_CONTR.toString()).locale('es').format('L').toString() ));
          element.Fianza = sol.NUM_FIA;
          element.Tipo_Fianza = sol.DESC_PAQUETE;
          element.Afianzadora = sol.DES_AFI;
          element.Fiador_Proveedor =  sol.FIADO_RFC;
          element.Fec_Solicitud = (sol.FEC_SOLICITUD == null)  ? 'n/a' : ((moment(sol.FEC_SOLICITUD.toString()).locale('es').format('L').toString() == 'Invalid date' ? 'n/a' : moment(sol.FEC_SOLICITUD.toString()).locale('es').format('L').toString() ));
          element.Fec_Emision = (sol.FEC_EMI_FIA == null)  ? 'n/a' : ((moment(sol.FEC_EMI_FIA.toString()).locale('es').format('L').toString() == 'Invalid date' ? 'n/a' : moment(sol.FEC_EMI_FIA.toString()).locale('es').format('L').toString() ));
          element.Fec_Vigencia_Ini = (sol.FEC_VIG_INI_FIA == null)  ? 'n/a' : ( (moment(sol.FEC_VIG_INI_FIA.toString()).locale('es').format('L').toString() == 'Invalid date' ? 'n/a' : moment(sol.FEC_VIG_INI_FIA.toString()).locale('es').format('L').toString() ));
          element.Fec_Vigencia_Fin = (sol.FEC_VIG_FIN_FIA == null)  ? 'n/a' : ( (moment(sol.FEC_VIG_FIN_FIA.toString()).locale('es').format('L').toString() == 'Invalid date' ? 'n/a' : moment(sol.FEC_VIG_FIN_FIA.toString()).locale('es').format('L').toString() ));
          element.Fec_Cumplimiento = (sol.FEC_CUM_FIA == null)  ? 'n/a' : ( (moment(sol.FEC_CUM_FIA).locale('es').format('L').toString() == 'Invalid date' ? 'n/a' : moment(sol.FEC_CUM_FIA).locale('es').format('L').toString() ));
          element.Beneficiario = sol.DES_BENEF;
          element.Monto_Fia = sol.MONTO_FIA;
          element.Prima_Total_Fia = sol.PRIMA_TOTAL_FIA;
          element.Usuario = sol.DESC_USUARIO;
          element.Estatus = sol.DESC_STSOL;
          element.Relacion_A = sol.RELA_CONTR;

          //element.Comision_Fia = sol.COM_FIA;
          //element.Prima_Neta_Fia = sol.PRIMA_NETA_FIA;
          this.RptExcel.push(element);

        });



      //const workBook = XLSX.utils.book_new(); // create a new blank book
      //const workSheet = XLSX.utils.json_to_sheet(this.RptExcel);

      //XLSX.utils.book_append_sheet(workBook, workSheet, 'Solicitudes-Fianzas'); // add the worksheet to the book
      //XLSX.writeFile(workBook, nombre_archivo); // initiate a file download in browser


        this.spinner.hide();
      },



      // Si ducede algun error se limpia la sessión y redirige al login
      error => {
        this.spinner.hide();
        this.securityServices.clearSession();
        console.log('oops: ', error)
      }
    );
  }

  


}
