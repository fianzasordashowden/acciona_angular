import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultaFianzasComponent } from './consulta-fianzas.component';

describe('ConsultaFianzasComponent', () => {
  let component: ConsultaFianzasComponent;
  let fixture: ComponentFixture<ConsultaFianzasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultaFianzasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultaFianzasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
