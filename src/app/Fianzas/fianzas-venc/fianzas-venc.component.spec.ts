import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FianzasVencComponent } from './fianzas-venc.component';

describe('FianzasVencComponent', () => {
  let component: FianzasVencComponent;
  let fixture: ComponentFixture<FianzasVencComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FianzasVencComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FianzasVencComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
