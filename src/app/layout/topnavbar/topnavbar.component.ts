import { Component, OnInit, Input } from '@angular/core';
import { SessionStorageService} from 'angular-web-storage';
import { SecurityService } from 'src/app/Servicios/security.service';

import { MessagingService } from 'src/app/shared/messaging.service';
import { trigger, state, style, animate, transition } from '@angular/animations';

import Scene from "scenejs";
import { shake, flip, fadeIn, wipeIn, keyframer, flipX } from "@scenejs/effects";

import { SceneItem } from '../../../../node_modules/scenejs';
import { SrvsolicitudesService } from 'src/app/SolicitudesFianzas/services/srvsolicitudes.service';

@Component({
  selector: 'app-topnavbar',
  templateUrl: './topnavbar.component.html',
  styleUrls: ['./topnavbar.component.scss'],
  animations: [
    trigger('changeDivSize', [
      state('initial', style({
        backgroundColor: 'green',
        width: '100px',
        height: '100px'
      })),
      state('final', style({
        backgroundColor: 'red',
        width: '200px',
        height: '200px'
      })),
      transition('initial=>final', animate('1500ms')),
      transition('final=>initial', animate('1000ms'))
    ]),

    trigger('balloonEffect', [
      state('initial', style({
        backgroundColor: 'green',
        transform: 'scale(1)'
      })),
      state('final', style({
        backgroundColor: 'red',
        transform: 'scale(1.5)'
      })),
      transition('final=>initial', animate('1000ms')),
      transition('initial=>final', animate('1500ms'))
    ]),

    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition('void <=> *', animate(1000)),
    ]),

    trigger('EnterLeave', [
      state('flyIn', style({ transform: 'translateX(0)' })),
      transition(':enter', [
        style({ transform: 'translateX(-100%)' }),
        animate('0.5s 300ms ease-in')
      ]),
      transition(':leave', [
        animate('0.3s ease-out', style({ transform: 'translateX(100%)' }))
      ])
    ]),
    trigger('fadeInOut', [
      state('in', style({ opacity: 100 })),
      transition('* => void', [
            animate(300, style({ opacity: 0 }))
      ])
    ])
  ]
})
export class TopnavbarComponent implements OnInit {

  @Input() nameTopBar: string;
  message: any = null; 

  constructor( 
    // public router: Router,
    public session: SessionStorageService,
    private securityServices: SecurityService,
    private messagingService: MessagingService,
    private solicitudService: SrvsolicitudesService
  ) { }

  ngOnInit() {
    // FGonzalez: Cachamos la información del usuario almacenada en la variable de sesión
    var infoUser = JSON.parse(JSON.parse(sessionStorage.getItem('infoUser'))._value);
    var nombreCompleto = infoUser.NOMBRE_COMPLETO;
    document.getElementById("nombre_usuario").textContent = nombreCompleto;

    this.acceptNotifications(infoUser.ID_USUARIO);

    new SceneItem({
      attribute: {
        "data-text": [
          "",
          "G",
          "Ge",
          "Ges",
          "Gest",
          "Gesti",
          "Gestió",
          "Gestión",
          "Gestión y",
          "Gestión y A",
          "Gestión y Ad",
          "Gestión y Adm",
          "Gestión y Admi",
          "Gestión y Admin",
          "Gestión y Admini",
          "Gestión y Adminis",
          "Gestión y Administ",
          "Gestión y Administr",
          "Gestión y Administra",
          "Gestión y Administrac",
          "Gestión y Administraci",
          "Gestión y Administració",
          "Gestión y Administración",
          "Gestión y Administración d",
          "Gestión y Administración de",
          "Gestión y Administración de F",
          "Gestión y Administración de Fi",
          "Gestión y Administración de Fia",
          "Gestión y Administración de Fian",
          "Gestión y Administración de Fianz",
          "Gestión y Administración de Fianza",
          "Gestión y Administración de Fianzas",
        ],
      }
    }, {
      duration: 1,
      selector: ".text",
      iterationCount: 1,
    }).play();

  }
  
  clearSession(){
    this.securityServices.clearSession();
  }

  acceptNotifications(idUser) {
    this.messagingService.requestPermission(idUser);
    this.messagingService.receiveMessage();
    this.message = this.messagingService.currentMessage;
  }

  manual() 
  {
    //this.spinner.show();
    //Cargar los documentos de la solicitud
    this.solicitudService.GetManual().subscribe(
      data => 
      {
        var archivo = data.FileStream._buffer;
        this.solicitudService.downloadPDF(archivo, "ManualAcciona_");
        //this.spinner.hide();
      },
      // Si ducede algun error se limpia la sessión y redirige al login
      error => 
      {
        //this.spinner.hide();
        this.securityServices.clearSession();
      }
    );
  }
}
