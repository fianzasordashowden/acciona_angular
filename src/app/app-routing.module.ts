import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login/login.component';
import { SolicitudesComponent } from './SolicitudesFianzas/solicitudes/solicitudes.component';
import { SolicitudesListComponent } from './SolicitudesFianzas/solicitudesComp/solicitudes-list/solicitudes-list.component';
import { SolicitudNuevaComponent } from './SolicitudesFianzas/solicitud-nueva/solicitud-nueva.component';
import { ChangepassComponent } from './login/changepass/changepass.component';
import { ResetpassComponent } from './login/resetpass/resetpass.component';
import { ConsultaFianzasComponent } from './Fianzas/consulta-fianzas/consulta-fianzas.component';
import { FianzasVencComponent } from './Fianzas/fianzas-venc/fianzas-venc.component';

const routes: Routes = [
    {path: 'login', component: LoginComponent},
    {path:'solicitudes', component: SolicitudesComponent},
    {path:"solicitudes/:id", component:SolicitudesComponent},
    {path:"nueva", component:SolicitudNuevaComponent},
    {path:'', component: LoginComponent},
    {path:'changepass', component: ChangepassComponent},
    {path:'resetpass', component: ResetpassComponent},
    {path:'fianzas', component: ConsultaFianzasComponent},
    {path:'fianzasVenc', component: FianzasVencComponent},
    
];

//export const routing = RouterModule.forRoot(routes);

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
