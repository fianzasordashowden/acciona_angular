import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import {Router} from '@angular/router';

import { SessionStorageService, SessionStorage, StorageService } from 'angular-web-storage';
import {JsonConvert, JsonConverter, OperationMode, ValueCheckingMode} from "json2typescript"
import {NgxSpinnerService} from 'ngx-spinner'

import { Usuario } from 'src/app/Models/usuFront';
import { InfoUsuario } from 'src/app/Models/infoUser';
import { SchemaUsuario } from 'src/app/Models/shemaUsu';
import { ResponseUsu } from 'src/app/Models/respClient';

import { SecurityService } from 'src/app/Servicios/security.service';
import { DeviceDetectorService } from 'ngx-device-detector';

import * as moment from 'moment';


declare var $;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
      
    //Device info
    deviceInfo: any = null;

    loginForm: FormGroup;
    resetPassForm: FormGroup;
    public isError = false;
    noFoundUsu:boolean = false;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private securityServices: SecurityService,
        public session: SessionStorageService,
        private spinner: NgxSpinnerService,
        private deviceService: DeviceDetectorService,
    ) { }

  usuarioFront: Usuario = new Usuario() ;

  ngOnInit() {

    this.loginForm = this.formBuilder.group({
      inEmail: ['', Validators.compose([Validators.required]) ],
      inPass: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
      done: false
    }); 

    this.resetPassForm = this.formBuilder.group({
      correoRecuperar: ['', Validators.compose([Validators.required, Validators.email]) ],
      }); 

      document.body.className = 'hold-transition login-page';
          $(() => {
              $('input').iCheck({
                  checkboxClass: 'icheckbox_square-blue',
                  radioClass: 'iradio_square-blue',
                  increaseArea: '20%' /* optional */
              });
          });
  }

 
  onResetPass(){
      
    this.spinner.show();

    //FGonzalez: Validamos la session llamando la función "loginuser" que se encuentra en nuestro securityServices
    return this.securityServices
      .sendMailToResPassword(this.resetPassForm.get('correoRecuperar').value)
      //Nos suscribimos al servicio esperando un data con los parametros que enviamos a la función
      .subscribe(
        data => {
          
          let mensaje = "";   

          if (data) {  
            mensaje = "Se ha enviado un mail a su correo con el cual podra cambiar su contraseña" ;              
          }
          else{
            mensaje = "Nooooo" ;
          } 

          document.getElementById("msgResetPass").innerHTML = mensaje

          this.spinner.hide();

        },
        // Si ducede algun error se limpia la sessión y redirige al login
        error => {
          this.spinner.hide();
          this.securityServices.clearSession();
        }
      );
  }


  onLogin() {
    
    let usuFront = new Usuario ();
    let jsonCrt = new JsonConvert();
    
    if (this.loginForm.valid) {
      
      this.spinner.show();

      //FGonzalez: Validamos la session llamando la función "loginuser" que se encuentra en nuestro securityServices
      return this.securityServices
        .loginuser(this.loginForm.get('inEmail').value, this.loginForm.get('inPass').value)
        //Nos suscribimos al servicio esperando un data con los parametros que enviamos a la función
        .subscribe(
          data => {
            if (data) {               
                //deserealizo obj Usu
                var objAllSt = JSON.stringify(data);
                //serealizo obj usu
                var objAll = JSON.parse(objAllSt);
                var arrUsuInfo = JSON.parse(objAll.infoUser);
                var arrShema = JSON.parse(objAll.schemaUser);

                objAll.infoUser = [];
                objAll.schemaUser = [];

                usuFront = jsonCrt.deserializeObject(objAll, Usuario);
                usuFront.info = arrUsuInfo;
                usuFront.shema = arrShema;

                //Get divice Info                
                this.deviceInfo = this.deviceService.getDeviceInfo();

                var usuRsp = new ResponseUsu();
                usuRsp.Data.Client.ClientToken = data["access_token"].toString();
                usuRsp.Data.Client.ClientTokenPlatform = this.deviceInfo.os.toString();
                usuRsp.Data.Client.ClientId = arrUsuInfo.idUsuario.toString();                
              
                usuRsp.Auth.CredentialPlatform = this.deviceInfo.browser.toString();
                usuRsp.Auth.CredentialLogin = usuFront.name.toString();
                usuRsp.Auth.Timestamp = moment(new Date()).format().toString();
                usuRsp.Auth.CredentialPwd = "";
                usuRsp.Auth.idLoginCli = 26; //[aqui va el numero del id del proveedor/agenteGO -IPR]

                // FGonzalez: almacenamos la información del usuario en una variable de sesion
                this.session.set("nameUsuFia", usuRsp.Auth.CredentialLogin);
                this.session.set("usuIdFia", arrUsuInfo.idUsuario.toString());
                this.session.set("infoUser", data["infoUser"]);
                this.session.set("schemaUser", data["schemaUser"]);

                // FGonzalez: Traemos el tipo de token y el token para alamacenarlos en la variable de sesión
                var tokenUsu = data["token_type"] + " " + data["access_token"]; 
                this.session.set("TokenUsuFia", tokenUsu);

                setTimeout(() => {
                  /** spinner ends after 5 seconds */
                  this.spinner.hide();
                }, 2000);

                //Si todo salio bien nos redirijira a solicitudes
                this.router.navigate(['solicitudes']);                    
            }
            // Si no trea la data correctamente o trae otra cosa se limpia la sesión y redirige al login
            else{
              this.securityServices.clearSession();
              this.noFoundUsu = true;
            } 
          },
          // Si ducede algun error se limpia la sessión y redirige al login
          error => {
            this.spinner.hide();
            this.securityServices.clearSession();
            this.noFoundUsu = true;
          }
        );
    } else {
      this.onIsError();
      //this.router.navigate(['login']);
      this.noFoundUsu = true;
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
      }, 500);
    }
  }


  onIsError(): void {
    this.isError = true;
    setTimeout(() => {
      this.isError = false;
      this.spinner.hide();
    }, 500);
  }



}
