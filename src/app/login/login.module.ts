import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule} from 'ngx-spinner'



/*Custom Files*/
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from '../app-routing.module';
import { ResetpassComponent } from './resetpass/resetpass.component';


  //Carga de Layout 
  import { LayoutModule } from 'src/app/layout/layout.module';
import { ChangepassComponent } from './changepass/changepass.component';

@NgModule({
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    LayoutModule,
  ],
  declarations: [LoginComponent, ResetpassComponent, ChangepassComponent]
})
export class LoginModule { }
