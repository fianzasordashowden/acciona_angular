import { Component, OnInit, ViewChild, Input, SimpleChanges, ChangeDetectionStrategy, ViewContainerRef } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource, MatDialog} from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { MatRippleModule } from '@angular/material/core';

//Tools
import { NgxSpinnerService } from 'ngx-spinner';
import * as XLSX from 'xlsx';
import Scene from "scenejs";
import * as moment from 'moment';

//Servicios
import { SrvsolicitudesService } from '../../services/srvsolicitudes.service';
import { SecurityService } from 'src/app/Servicios/security.service';

//Models
import { solExcelGO, solicitudAccionaFianzasLst, solicitudDetalleFiaDTO, solExcelAcc } from 'src/app/Models/solicitudes';

//Components
import { SolicitudesDetalleComponent } from '../solicitudes-detalle/solicitudes-detalle.component';
import { fileFianza } from 'src/app/Models/fileNewSol';
import { ToastsManager } from 'ng6-toastr/src/toast-manager';
import { contratoNotifDTO } from 'src/app/Models/afianzadora';
import { Toast } from 'ng6-toastr/src/toast';
import { PostInfoUser } from 'src/app/Models/userLog';



@Component({
  selector: 'app-solicitudes-list',
  templateUrl: './solicitudes-list.component.html',
  styleUrls: ['./solicitudes-list.component.scss'],
  // your selector and template
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class SolicitudesListComponent implements OnInit {
  
  solicitudesLst: solicitudAccionaFianzasLst[];
  solDetFianza: solicitudDetalleFiaDTO;
  fileDocsLst: Array<fileFianza> = []; 
  fileFianzaLst: Array<fileFianza> = []; 
  fileCancelacionLst: Array<fileFianza> = []; // #Cancelaciones -> Ahernandezh 02/02/2021
  show:boolean = false;
  RptExcel: Array<solExcelGO> = [];
  infUser: PostInfoUser = new PostInfoUser();
  
  // displayedColumns: string[] = ['NUM_SOL', 'DES_TIPO_SOL', 'DESC_USUARIO', 'FIADO_RFC', 'DES_PROY_SOL', 'FEC_SOLICITUD', 'DESC_STSOL', 'actionsColumn'];

  displayedColumns: string[] = ['NUM_SOL', 'DES_TIPO_SOL', 'OBSV_SOL', 'FIADO_RFC', 'DES_PROY_SOL', 'FEC_SOLICITUD', 'DESC_STSOL', 'actionsColumn'];

  
  dataSourseSol = new MatTableDataSource();

  @Input() childStatusSol: string;
  @Input() menuStatus: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  activeRoute: any;


  constructor(
    private solicitudService: SrvsolicitudesService,
    private securityServices: SecurityService,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    public toastr: ToastsManager, vcr: ViewContainerRef
  ) { 
    this.toastr.setRootViewContainerRef(vcr);
  }


  //Carga Inicial
  ngOnInit() {

    this.dataSourseSol.paginator = this.paginator;
    this.dataSourseSol.sort = this.sort;

    this.route.params.subscribe(routeParams => {
      this.getAllSolicitudes(routeParams.id);
    });

    this.getAllNotification();

  }// fin ngOnInit
  
 

  //********************************************************************************/

 //Metodos Solicitudes
  getAllSolicitudes(statusSol) {  


    let JsonUsu =  JSON.parse(JSON.parse(sessionStorage.getItem('infoUser'))._value);
    
    this.infUser.cvePerfil = JsonUsu.CvePerfil;
    this.infUser.nameRol = JsonUsu.Rol;
    this.infUser.idArea = JsonUsu.area;
    this.infUser.ejecutivo = JsonUsu.ejecutivo;
    this.infUser.dateAlta =  moment(JsonUsu.fechaAlta, 'DD-MM-YYYY').toDate();
    this.infUser.idUsuario = JsonUsu.idUsuario;
    this.infUser.idGrupo = JsonUsu.id_grupo;
    this.infUser.idFiado = JsonUsu.idFiado;
    this.infUser.eMail = JsonUsu.mail;
    this.infUser.userName = JsonUsu.nombreCompleto;

    this.spinner.show();    

    this.solicitudService.getAllSolicitudes(this.infUser).subscribe(
      data => {
        let lstFiltrada : any;

        if (statusSol == '4'){
          lstFiltrada = data.solicitudesAccDTO.filter(function (f) { return f.CVE_STATUS_SOL == '2'; });
        }else if (statusSol == '5'){
          lstFiltrada = data.solicitudesAccDTO.filter(function (f) { return f.CVE_STATUS_SOL == '3'; });
        }
        else{
          lstFiltrada = data.solicitudesAccDTO;
        }

        this.solicitudesLst = lstFiltrada;
        this.dataSourseSol.data =  lstFiltrada;

        this.spinner.hide();
        
      },
      // Si ducede algun error se limpia la sessión y redirige al login
      error => {
        this.spinner.hide();
        this.securityServices.clearSession();
        console.log('oops: ', error)
      }
    );

   
  }

  //*******************  Functions DataMaterial********************* */
  
  applyFilter(filterValue: string) {
    this.dataSourseSol.filter = filterValue.trim().toLowerCase();
    if (this.dataSourseSol.paginator) {
      this.dataSourseSol.paginator.firstPage();
    }
  }

  private refreshTable() {
    // Refreshing table using paginator
    this.paginator._changePageSize(this.paginator.pageSize);
  }


  //Styles for diferent status SOLICITUD    
  getStyleClass(colRow) {
    
    switch (colRow.CVE_STATUS_SOL) {
      case 1:
        return "bt-color-1";  
      case 2:
        return "bt-color-2";
      case 3:
        return "bt-color-3";
      case 4:
        return "bt-color-4";
      case 5:
        return "bt-color-5";
      case 6:
        return "bt-color-6";
      case 7:
          return "bt-color-7";
      default:
        return "bt-color-1";
    }
    
  }


  startEdit(solDetFianza: solicitudDetalleFiaDTO) {
    
    this.spinner.show();

    //Cargar los documentos de la solicitud
    this.solicitudService.getFilesSolicitud(solDetFianza.NUM_SOL).subscribe(
      data => {

        this.fileDocsLst = [];
        this.fileFianzaLst = [];
        this.fileCancelacionLst = []; // #Cancelaciones -> Ahernandezh 02/02/2021
        
        data.solDetAccDTO.SOLICITUD[0].FILES_SOL.forEach( (file) => 
        {
          let fileNew = new fileFianza();
          fileNew.NUM_SOL = file.NUM_SOL;
          fileNew.NOM_DOCUMENTO = file.NOM_DOCUMENTO;
          fileNew.ID_TIPO = file.ID_TIPO;
          fileNew.ID_DOCUMENTO = file.ID_DOCUMENTO;
          fileNew.FS_DOCUMENTO = file.FS_DOCUMENTO;
          fileNew.DES_TIP = file.DES_TIP;
          fileNew.CONTEXTO = file.CONTEXTO;
          fileNew.b_DOCUMENTO = file.b_DOCUMENTO;
          fileNew.s_DOCUMENTO = file.s_DOCUMENTO;
          switch(file.ID_TIPO){
            case 3:
            case 7:
            case 8:
            case 33:  
                this.fileFianzaLst.push(fileNew);
            break;
            case 34: // #Cancelaciones -> Ahernandezh 02/02/2021
                this.fileCancelacionLst.push(fileNew);
            default: // #Cancelaciones -> Ahernandezh 02/02/2021
                if(file.ID_TIPO != 32)
                {
                  if(file.ID_TIPO != 34)
                  {
                    this.fileDocsLst.push(fileNew);
                  }
                }
                
            break;
          }
 
        });


        solDetFianza.NUM_SOL = data.solDetAccDTO.SOLICITUD[0].NUM_SOL;
        solDetFianza.DES_STATUS_SOL = data.solDetAccDTO.SOLICITUD[0].DESC_STATUS_SOL;
        solDetFianza.ID_FIA  = data.solDetAccDTO.SOLICITUD[0].ID_FIA;
        
        solDetFianza.CONTRAC_SOL = data.solDetAccDTO.SOLICITUD[0].CONTRATO[0];
        solDetFianza.FIANZA_SOL = data.solDetAccDTO.SOLICITUD[0].FIANZA[0];
        solDetFianza.RECIBO_SOL = data.solDetAccDTO.SOLICITUD[0].RECIBO[0];
        
        solDetFianza.lstStatusRec = data.solDetAccDTO.SOLICITUD[0].STATUS_RECIBO;
        solDetFianza.lstDocsFia = this.fileDocsLst;
        solDetFianza.lstFilesFia = this.fileFianzaLst;
        solDetFianza.lstCancelacionFia = this.fileCancelacionLst; // #Cancelaciones -> Ahernandezh 02/02/2021
        solDetFianza.lstTipMonRec = data.solDetAccDTO.SOLICITUD[0].TIPO_MONEDA;

        this.spinner.hide();

        /*Open Dialog*/
        const dialogRef = this.dialog.open(SolicitudesDetalleComponent, {
          data: {solDetFianza, show:this.show}
        });

        dialogRef.afterClosed().subscribe(result => {
          
          this.ngOnInit();

          if (result === 1) {
            this.refreshTable();
          }
        });
      },
      // Si ducede algun error se limpia la sessión y redirige al login
      error => {
        this.spinner.hide();
        this.securityServices.clearSession();
        console.log('oops: ', error)
      }

    );

  }

  //****************** Detalles Solicitud ************************************/

  selectSolFianza(solDetFianza: solicitudDetalleFiaDTO) {  
    this.solDetFianza = Object.assign({}, solDetFianza);
  }

  //************* Cerrar Dialog *************************************************/
  closeDialogForm() {
    this.solDetFianza = null;
  }

  //***************************************************************************** */
  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {  
      let change = changes[propName];
    }
  }

  getTypeFile(extFile) {
    
    var fileType = 'application/octet-stream';
    var fileExtension = extFile.split('.').pop();

    if (fileExtension == 'jpg') {
        fileType = 'image/jpg';
    }
    else if (fileExtension == 'png') {
        fileType = 'image/png';
    }else if (fileExtension == 'gif') {
        fileType = 'image/gif';
    }else if (fileExtension == 'tiff') {
        fileType = 'image/tiff';
    }else if (fileExtension == 'jpeg' || fileExtension == 'jpg') {
        fileType = 'image/jpeg';
    }else if (fileExtension == 'pdf') {
      fileType = 'application/pdf';
    }else if (fileExtension == 'txt') {
      fileType = 'text/plain';
    }else if (fileExtension == 'doc' || fileExtension == 'docx' ) {
      fileType = 'application/vnd.ms-word';
    }else if (fileExtension == 'xls ' || fileExtension == 'xlsx') {
      fileType = 'application/vnd.ms-excel';
    }else if (fileExtension == 'csv') {
      fileType = 'text/csv';
    }
    
    return fileType;

  }

  export() {

    this.spinner.show();    
    let cve_perfil = JSON.parse(JSON.parse(sessionStorage.getItem('infoUser'))._value).CvePerfil;
    let nombre_archivo = "";

    this.RptExcel = [];

     this.solicitudesLst.forEach( (sol) => {
      let element;


      if(cve_perfil == 6){
        element = new solExcelGO();
        element.Nombre_del_Proyecto = sol.DES_PROY_SOL;
        element.Solicitud = sol.NUM_SOL ;
        element.Afianzadora = sol.DES_AFI;
        element.Fiador_RFC =  sol.FIADO_RFC;
        element.Fiador_Proveedor =  sol.FIADO_NOMBRE;
        element.Beneficiario = sol.DES_BENEF;
        //element.Usuario = sol.DESC_USUARIO;
        element.Usuario = sol.OBSV_SOL;
        element.Estatus = sol.DESC_STSOL;
        element.Contrato = ( (sol.NUM_CONTR == null) ? 'n/a' : sol.NUM_CONTR );

        element.Fec_Contrato = (sol.FEC_CONTR == null) ? 'n/a' : ((moment(sol.FEC_CONTR.toString()).locale('es').format('L').toString() == 'Invalid date' ? 'n/a' : moment(sol.FEC_CONTR.toString()).locale('es').format('L').toString() ));
        element.Tipo_Fianza = sol.DESC_PAQUETE;  
        element.Fec_Solicitud = (sol.FEC_SOLICITUD == null)  ? 'n/a' : ((moment(sol.FEC_SOLICITUD.toString()).locale('es').format('L').toString() == 'Invalid date' ? 'n/a' : moment(sol.FEC_SOLICITUD.toString()).locale('es').format('L').toString() ));  
        element.Fec_Emision = (sol.FEC_EMI_FIA == null)  ? 'n/a' : ((moment(sol.FEC_EMI_FIA.toString()).locale('es').format('L').toString() == 'Invalid date' ? 'n/a' : moment(sol.FEC_EMI_FIA.toString()).locale('es').format('L').toString() ));  
        
        element.Fec_Vigencia_Ini = (sol.FEC_VIG_INI_FIA == null)  ? 'n/a' : ( (moment(sol.FEC_VIG_INI_FIA.toString()).locale('es').format('L').toString() == 'Invalid date' ? 'n/a' : moment(sol.FEC_VIG_INI_FIA.toString()).locale('es').format('L').toString() ));  
        element.Fec_Vigencia_Fin = (sol.FEC_VIG_FIN_FIA == null)  ? 'n/a' : ( (moment(sol.FEC_VIG_FIN_FIA.toString()).locale('es').format('L').toString() == 'Invalid date' ? 'n/a' : moment(sol.FEC_VIG_FIN_FIA.toString()).locale('es').format('L').toString() ));  
        
        element.Relacion_A = sol.RELA_CONTR;
        element.Fec_Cumplimiento = (sol.FEC_CUM_FIA == null)  ? 'n/a' : ( (moment(sol.FEC_CUM_FIA).locale('es').format('L').toString() == 'Invalid date' ? 'n/a' : moment(sol.FEC_CUM_FIA).locale('es').format('L').toString() )); 
        element.Monto_Fia = '$' + sol.MONTO_FIA.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        element.Comision_Fia = '$' + sol.COM_FIA.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        element.Prima_Neta_Fia = '$' + sol.PRIMA_NETA_FIA.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        element.Prima_Total_Fia = '$' + sol.PRIMA_TOTAL_FIA.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        //element.OBSVSERVACIONES_SOL = sol.OBSV_SOL;

        nombre_archivo = 'Reporte_Solicitudes_GO.xlsx'

      }
      else if (cve_perfil == 7 || cve_perfil == 8){
        element = new solExcelAcc();
        element.Nombre_del_Proyecto = sol.DES_PROY_SOL;
        element.Solicitud = sol.NUM_SOL;
        element.Afianzadora = sol.DES_AFI;
        element.Fiador_RFC =  sol.FIADO_RFC;
        element.Fiador_Proveedor =  sol.FIADO_NOMBRE;
        element.Beneficiario = sol.DES_BENEF;
        element.Usuario = sol.OBSV_SOL;
        element.Estatus = sol.DESC_STSOL;
        element.Contrato = ( (sol.NUM_CONTR == null) ? 'n/a' : sol.NUM_CONTR );

        element.Fec_Contrato = (sol.FEC_CONTR == null) ? 'n/a' : ((moment(sol.FEC_CONTR.toString()).locale('es').format('L').toString() == 'Invalid date' ? 'n/a' : moment(sol.FEC_CONTR.toString()).locale('es').format('L').toString() ));
        element.Tipo_Fianza = sol.DESC_PAQUETE;  
        element.Fec_Solicitud = (sol.FEC_SOLICITUD == null)  ? 'n/a' : ((moment(sol.FEC_SOLICITUD.toString()).locale('es').format('L').toString() == 'Invalid date' ? 'n/a' : moment(sol.FEC_SOLICITUD.toString()).locale('es').format('L').toString() ));  
        element.Fec_Emision = (sol.FEC_EMI_FIA == null)  ? 'n/a' : ((moment(sol.FEC_EMI_FIA.toString()).locale('es').format('L').toString() == 'Invalid date' ? 'n/a' : moment(sol.FEC_EMI_FIA.toString()).locale('es').format('L').toString() ));  

        element.Fec_Vigencia_Ini = (sol.FEC_VIG_INI_FIA == null)  ? 'n/a' : ( (moment(sol.FEC_VIG_INI_FIA.toString()).locale('es').format('L').toString() == 'Invalid date' ? 'n/a' : moment(sol.FEC_VIG_INI_FIA.toString()).locale('es').format('L').toString() ));  
        element.Fec_Vigencia_Fin = (sol.FEC_VIG_FIN_FIA == null)  ? 'n/a' : ( (moment(sol.FEC_VIG_FIN_FIA.toString()).locale('es').format('L').toString() == 'Invalid date' ? 'n/a' : moment(sol.FEC_VIG_FIN_FIA.toString()).locale('es').format('L').toString() ));  

        element.Relacion_A = sol.RELA_CONTR;
        element.Fec_Cumplimiento = (sol.FEC_CUM_FIA == null)  ? 'n/a' : ( (moment(sol.FEC_CUM_FIA).locale('es').format('L').toString() == 'Invalid date' ? 'n/a' : moment(sol.FEC_CUM_FIA).locale('es').format('L').toString() )); 
        element.Monto_Fia = '$' + sol.MONTO_FIA.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");        
        element.Prima_Total_Fia = '$' + sol.PRIMA_TOTAL_FIA.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        //element.OBSVSERVACIONES_SOL = sol.OBSV_SOL;

        nombre_archivo = 'Reporte_Solicitudes_Acc.xlsx'
      }
      
      this.RptExcel.push(element);
     
    }); 
    const workBook = XLSX.utils.book_new(); // create a new blank book
    const workSheet = XLSX.utils.json_to_sheet(this.RptExcel);

    XLSX.utils.book_append_sheet(workBook, workSheet, 'Solicitudes-Fianzas'); // add the worksheet to the book
    XLSX.writeFile(workBook, nombre_archivo); // initiate a file download in browser

    this.spinner.hide();
  }

  exportPDF() { 
    
    this.spinner.show();

    //Cargar los documentos de la solicitud
    this.solicitudService.ReportPDFSol().subscribe(
      data => {
        var archivo = data.FileStream._buffer;
        this.solicitudService.downloadPDF(archivo, "Solicitudes_Acciona_");
        this.spinner.hide();
      },
      // Si ducede algun error se limpia la sessión y redirige al login
      error => {
        this.spinner.hide();
        this.securityServices.clearSession();
        console.log('oops: ', error)
      }
    );
  }

//*********************************** TOAST ********************************************************** */
  
  getAllNotification() {

    this.spinner.show();    
    
    let lstContracNotifWar : Array<contratoNotifDTO> = [];
    let lstContracNotifErr : Array<contratoNotifDTO> = [];

    this.solicitudService.getAllNotifications().subscribe(
      data => {
    
        lstContracNotifWar = data.contractsNotifDTO.lstContratosWarning;
        lstContracNotifErr = data.contractsNotifDTO.lstContratosError;


        lstContracNotifWar.forEach( (file) => {
          
          this.toastr.warning('El contrato de la solicitud:  ' + file.NUM_SOL + '  esta a un mes de vencer, favor de revisar', ':: Contratos a punto de Vencer ::', {dismiss: 'controlled'})
          .then((toast: Toast) => {
              setTimeout(() => {
                  this.toastr.dismissToast(toast);
              }, 1500);
          });

        });
        
        setTimeout(() => {
          lstContracNotifErr.forEach( (file) => {
            this.toastr.warning('El contrato de la solicitud: ' + file.NUM_SOL + ' ya esta vencido, favor de revisar', ':: Contratos Vencidos ::', {dismiss: 'controlled'})
            .then((toast: Toast) => {
                setTimeout(() => {
                    this.toastr.dismissToast(toast);
                }, 1500);
            });
          });
        }, 3000);

      
        

        /*
        if(lstContracNotifWar.length > 0){
          lstContracNotifWar.forEach( (file) => {
            this.toastr.warning('El contrato de la solicitud:  ' + file.NUM_SOL + '  esta a un mes de vencer, favor de revisar', 'Alert!');
          });
        }
    
        if(lstContracNotifErr.length > 0){
          lstContracNotifErr.forEach( (file) => {
            this.toastr.warning('El contrato de la solicitud: ' + file.NUM_SOL + ' ya esta vencido, favor de revisar', 'Alert!');
          });
        }
        */

        this.spinner.hide();

      },
      // Si ducede algun error se limpia la sessión y redirige al login
      error => {
        this.spinner.hide();
        this.securityServices.clearSession();
        console.log('oops: ', error)
      }
    );

      

  }

  showSuccess() {
    //this.toastr.success('You are awesome!', 'Success!');
    this.toastr.warning('You are being warned.', 'Alert!');
    
    /*
    this.toastr.custom('<div class="toast-bottom-left" id="toast-container" style="position: fixed;"><div class="toast" style="opacity: 1;"><i class="material-icons">help_outline</i><div class="toast-title ng-star-inserted" style="">Alert!</div><div class="ng-tns-c14-11"><span class="toast-message ng-star-inserted" style="">You are being warned.</span></div></div></div>'
    , null, {enableHTML: true});
    */
  }

}