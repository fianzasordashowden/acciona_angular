import { Component, OnInit, Input, Output, EventEmitter, Inject, Directive, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

//Material
import {MAT_DIALOG_DATA, MatDialogRef, MatTabChangeEvent} from '@angular/material';
import {MomentDateAdapter} from '@angular/material-moment-adapter';

import { SatCalendar, SatDatepicker, DateAdapter,  MAT_DATE_LOCALE, MAT_DATE_FORMATS, NativeDateAdapter} from 'saturn-datepicker';

import { NgxSpinnerService } from 'ngx-spinner';

import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';

//Forms
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

//animations
import { trigger, state, style, animate, transition} from '@angular/animations';

import * as moment from 'moment';

import Swal from 'sweetalert2'

//Models
import { solicitudFianzasDTO, solicitudDetalleFiaDTO, solicitudNuevaDTONew } from 'src/app/Models/solicitudes';
import { PostRegisterSolAct, PostRegisterContrato, PostRegisterFianza, PostRegisterRecibo, PostAnularSolicitud } from 'src/app/Models/PostRegisterSolNueva';
import { TipoDocumento } from 'src/app/Models/tipoDocu';
import { FilesSolNuevaDTO, fileFianza } from 'src/app/Models/filesSolNuevaDTO';

import * as FileSaver from 'file-saver';

import { SrvsolicitudesService } from '../../services/srvsolicitudes.service';
import { from } from 'rxjs';

import Scene from "scenejs";

import { SecurityService } from 'src/app/Servicios/security.service';
import { PostRegisterComentNuevo } from 'src/app/Models/PostRegisterComentNuevo';

import { SessionStorageService } from 'angular-web-storage';
import { FileComentDTO } from 'src/app/Models/FileComentDTO';
import { statusRecDTO } from 'src/app/Models/afianzadora';


export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'L',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

const config: ScrollToConfigOptions = {
  target: 'dvcoments'
};

@Component({
  selector: 'app-solicitudes-detalle',
  templateUrl: './solicitudes-detalle.component.html',
  styleUrls: ['./solicitudes-detalle.component.scss'],
  animations: [
    trigger('changeDivSize', [
      state('initial', style({
        backgroundColor: 'green',
        width: '100px',
        height: '100px'
      })),
      state('final', style({
        backgroundColor: 'red',
        width: '200px',
        height: '200px'
      })),
      transition('initial=>final', animate('1500ms')),
      transition('final=>initial', animate('1000ms'))
    ]),

    trigger('balloonEffect', [
      state('initial', style({
        backgroundColor: 'green',
        transform: 'scale(1)'
      })),
      state('final', style({
        backgroundColor: 'red',
        transform: 'scale(1.5)'
      })),
      transition('final=>initial', animate('1000ms')),
      transition('initial=>final', animate('1500ms'))
    ]),

    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition('void <=> *', animate(1000)),
    ]),

    trigger('EnterLeave', [
      state('flyIn', style({ transform: 'translateX(0)' })),
      transition(':enter', [
        style({ transform: 'translateX(-100%)' }),
        animate('0.5s 300ms ease-in')
      ]),
      transition(':leave', [
        animate('0.3s ease-out', style({ transform: 'translateX(100%)' }))
      ])
    ]),
    trigger('fadeInOut', [
      state('in', style({ opacity: 100 })),
      transition('* => void', [
            animate(300, style({ opacity: 0 }))
      ])
    ])
  ],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
    {provide: MAT_DATE_LOCALE, useValue: 'es-ES' },
  ],
  //encapsulation: ViewEncapsulation.None
})

export class SolicitudesDetalleComponent implements OnInit  {

  date1 = { begin: new Date(), end: new Date() };
  

  //FileUpload
  /*
  //******************  Upload File   ********************************************* /**        
  @Input() text = 'Upload';
  /** Name used in form which will be sent in HTTP request. */
  @Input() param = 'file';
  /** Target URL for file uploading. */
  @Input() target = 'https://file.io';
  /** File extension that accepted, same as 'accept' of <input type="file" />. 
      By the default, it's set to 'image/*'. */
  @Input() accept = 'image/*, .pdf, .doc, .xls, .xlsx, .docx, ppt, pptx, .xml';
  /** Allow you to add handler after its completion. Bubble up response text from remote.**/
  @Output() complete = new EventEmitter<string>();

  stMatPro: string = "void";
  arrTipDocs: Array<TipoDocumento> = [];
  fileUpload: HTMLInputElement;
  arrFileDocs: Array<FilesSolNuevaDTO> = [];
  postRegNewComent: PostRegisterComentNuevo = new PostRegisterComentNuevo();
  
  solNewAfia: solicitudNuevaDTONew = new solicitudNuevaDTONew();

  private filesLoad: any[] = [];
  private fileReader = new FileReader();

  //******************************************************************************* /**    
  @Input() solDetFianza: solicitudDetalleFiaDTO;
  @Output() saveSolFianzaEvent = new EventEmitter<solicitudFianzasDTO>();
  @Output() closeSolFianzaEvent = new EventEmitter();

  show: boolean;
  btnDis: boolean;
  idFia: number;
  idContr: number;
  numSolucion: number;
  numAfianzadora: number;
  numTipSol: number;
  numCorp: number;
  numBenef: number;
  numGrupo: number;
  idNumUsu: number;
  nomBenef: string;
  rfcFiado: string;
  dvFootCom: string;
  numDocument: number = 0;
  display: boolean;
  inTabSel: number = 0;
  isActiveFia:boolean = false;
  isActiveRec:boolean = false;
  showButton: boolean = false;  
  showBtnCom: boolean = false;  
  dataUsuFia: any;  
  solfiaPrueba : solicitudFianzasDTO;
  arrStatusRec: Array<statusRecDTO> = [];
  dateRangeDisp = {'begin': Date, 'end': Date};

  postRegUpdSol: PostRegisterSolAct = new PostRegisterSolAct();
  postRegUpdCon: PostRegisterContrato = new PostRegisterContrato();
  postRegUpdFia: PostRegisterFianza = new PostRegisterFianza();
  postRegUpdRec: PostRegisterRecibo = new PostRegisterRecibo();
  postAnularSol: PostAnularSolicitud = new PostAnularSolicitud();
  
  newFileSol: fileFianza; 

  fianzaForm: FormGroup;
  contracForm: FormGroup;
  reciboForm: FormGroup;
  ComentForm: FormGroup;
  CancelacionForm: FormGroup; // #Cancelaciones -> Ahernandezh 02/02/2021

  date = new FormControl(moment().locale('es'));

  //FGonzalez: COMENTARIOS DE SOLICITUD
  comentariosSol: any; 
  
  arrFileComent: Array<FileComentDTO> = [];
  
  divArchivos :boolean = true;  

  MostrarDocCancelacion: boolean = true;  // #Cancelaciones -> Ahernandezh 02/02/2021
  ComentarioCancelacion: string = '';     // #Cancelaciones -> Ahernandezh 02/02/2021

  constructor(
    private formBuilder: FormBuilder,
    private solicitudService: SrvsolicitudesService,
    public dialogRef: MatDialogRef<SolicitudesDetalleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router,
    private spinner: NgxSpinnerService,
    private securityServices: SecurityService,
    public session: SessionStorageService,
    private _scrollToService: ScrollToService
    ) { 
      // FGonzalez: Lo primero que se hace es validar si la sessión esta activa.
      // En caso de que no la misma función validateSession() nos redireccionara al login
      this.securityServices.validateSession();   
    }

  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);

  ngOnInit() {
    
    //Validar por usuario que y que puede ver IPR
    this.dataUsuFia = JSON.parse(JSON.parse(sessionStorage.getItem('infoUser'))._value);
    this.idNumUsu = JSON.parse(JSON.parse(sessionStorage.getItem('infoUser'))._value).idUsuario;
    this.display = true;
    this.solfiaPrueba = this.data.solDetFianza;
    this.numSolucion = this.data.solDetFianza.NUM_SOL;
    this.idFia = this.data.solDetFianza.ID_FIA;
    this.idContr = this.data.solDetFianza.CONTRAC_SOL.ID_CONTR;
    this.numAfianzadora = this.data.solDetFianza.CVE_AFIAN;
    this.numTipSol   = this.data.solDetFianza.CVE_PAQUETE;
    this.numCorp = this.dataUsuFia.id_grupo;//this.data.solDetFianza.CVE_CORPORATIVO;
    this.numBenef = this.data.solDetFianza.CVE_BENEF;
    this.numGrupo = this.data.solDetFianza.CVE_GRUPO;
    this.nomBenef = this.data.solDetFianza.DESC_GERE;
    this.rfcFiado = this.data.solDetFianza.FIADO_RFC;
    this.arrStatusRec = this.data.solDetFianza.lstStatusRec;
    this.date1.begin = this.data.solDetFianza.FIANZA_SOL.FEC_VIG_INI_FIA;
    this.date1.end = this.data.solDetFianza.FIANZA_SOL.FEC_VIG_FIN_FIA;

    // #Cancelaciones -> Ahernandezh 02/02/2021
    if(this.data.solDetFianza.lstCancelacionFia.length > 0)
    {
      this.MostrarDocCancelacion = true;
    }
    else
    {
      this.MostrarDocCancelacion = false;
    }
    // #FinCancelaciones

    //////////////////////////////// FGONZALEZ ////////////////////////////////
    var cta = document.getElementById("cta");
    var newC = document.getElementById("new");
    
    var sceneFirst = new Scene({
      ".container" : [
        {
          transform: "scale(.7)",
          opacity: 0
        },
        {
          transform: "scale(1)",
          opacity: 1
        },
      ]
    },
    {
      duration: 2,
      easing: "ease-in-out",
      selector: true
    }).playCSS();
  
    var sceneMostrar = new Scene({
      '#cta' : {
        0: {
          transform: {
            scale: 1
          }
        },
        .1: {
          transform: {
            scale: 2   
          }     
        },
        .3: {
          transform: {
            scale: 1   
          }     
        }
      },
      '.info' :
      {
        .5 :{
          transform: {
            scale: 0
          },  
          opacity: 0
          
        },
        .7 :{
          transform: {
            scale: .5
          },  
          opacity: .5
          
        },
        1 : {
          transform: {
            scale: 1
          },
          opacity: 1
        },
      },
      '.newCom' : {
        0: {
          transform: {
            scale: 1
          }
        },
        .1: {
          transform: {
            scale: 2   
          }     
        },
        .3: {
          transform: {
            scale: 1   
          }     
        }
      },
      // '.container' : {
      //   .4 :{
      //     transform: 'scale(1)',          
      //     // height: '1%',           
      //     opacity: 1
          
      //   },
      //   .7 : {         
      //     // height: '60%',
      //   },
      // },
      // ".item" : function (i)
      // {
      //   // console.log(i);
      //   return{
      //     0: {
      //       opacity: '0'
      //     },
      //     .5 :{
      //       opacity: '1'
      //     },
      //     options: {
      //       delay: i * .5
      //     }
      //   }
      // }
    },
    {    
      easing: "ease-in-out",
      selector: true
    });

    var sceneOcultar = new Scene({
      '#cta' : {
        0: {
          transform: {
            scale: 1
          }
        },
        .1: {
          transform: {
            scale: 2   
          }     
        },
        .3: {
          transform: {
            scale: 1   
          }     
        }
      },
      '.info' :
      {
        .5 :{
          transform: {
            scale: 1
          },  
          opacity: 1
          
        },
        .7 :{
          transform: {
            scale: .5
          },  
          opacity: .5
          
        },
        1 : {
          transform: {
            scale: 1
          },
          opacity: 0
        },
      },
      '.newCom' : {
        0: {
          transform: {
            scale: 1
          }
        },
        .1: {
          transform: {
            scale: 2   
          }     
        },
        .3: {
          transform: {
            scale: 1   
          }     
        }
      },
    },
    {    
      easing: "ease-in-out",
      selector: true
    });

    this.ComentForm = new FormGroup({
      descComentario: new FormControl()
    });
  
    newC.addEventListener("click", () => {

      var Prueba = sceneMostrar.getDirection().toString();
     
      if (sceneMostrar.getDirection() === 'normal') {
        sceneMostrar.playCSS();    
        sceneMostrar.setDirection('reverse');
        document.getElementById('formComent').style.display = 'none';  

      }
      else{
        sceneMostrar.playCSS();
        sceneMostrar.setDirection('normal');
        document.getElementById('formComent').style.display = 'block'; 
        
        
      }

   
    });

    cta.addEventListener("click", () => {

      if (sceneMostrar.getDirection() === 'normal') {
        sceneMostrar.playCSS();
        sceneMostrar.setDirection('reverse');

        document.getElementById('infoComents').style.display = 'block'; 
         
        var cta = document.getElementById('cta') as HTMLImageElement;        
        cta.src = "assets/Images/svg/shrink.svg";           
      }
      else{
        sceneMostrar.setDirection('normal');
        sceneMostrar.playCSS();    

        document.getElementById('infoComents').style.display = 'none'; 

        var cta = document.getElementById('cta') as HTMLImageElement;        
        cta.src = "/assets/Images/svg/expand.svg";   
      }
    });

    this.GetComentariosSol(this.numSolucion);


    // let statusRec = this.data.solDetFianza.RECIBO_SOL.STATUS_REC;
    // if(statusRec != 2){      
    //   this.divArchivos = false;
    // }

    this.divArchivos = (this.data.solDetFianza.RECIBO_SOL.STATUS_REC != 2) ? false : true; 


    //Pinta los INPUT de los 3 archivos principales de las fianzas IPR
    if(this.data.solDetFianza.lstFilesFia){
      this.show = this.data.solDetFianza.lstFilesFia.length == 0 ? true : false;

      if(this.dataUsuFia.CvePerfil == 8){
        this.data.solDetFianza.lstFilesFia.forEach( fileFia => {
          if( (fileFia.ID_TIPO == 3 || fileFia.ID_TIPO == 33) &&  !this.divArchivos){
            fileFia.b_DOCUMENTO = false;
          }else if((fileFia.ID_TIPO != 3 || fileFia.ID_TIPO != 33)){
            fileFia.b_DOCUMENTO = true;
          }else{
            fileFia.b_DOCUMENTO = true;
          }   
        });
      }else{
        this.data.solDetFianza.lstFilesFia.forEach( fileFia => {
            fileFia.b_DOCUMENTO = true;
        });

      }
     


      this.data.solDetFianza.lstFilesFia

      

      if(this.data.solDetFianza.lstFilesFia.length > 0 && this.data.solDetFianza.lstFilesFia.length <= 2 ){
        //this.show = false;
        var fileFia = this.data.solDetFianza.lstFilesFia.filter(function (f) { return f.ID_TIPO == 3; });
        var fileFiaXml = this.data.solDetFianza.lstFilesFia.filter(function (f) { return f.ID_TIPO == 33; });
        var fileRec = this.data.solDetFianza.lstFilesFia.filter(function (f) { return f.ID_TIPO == 7; });
        var fileXml = this.data.solDetFianza.lstFilesFia.filter(function (f) { return f.ID_TIPO == 8; });
  
        if(fileFia.length == 0){
          this.newFileSol = new fileFianza();
          this.newFileSol.ID_TIPO = 3;
          this.newFileSol.ID_DOCUMENTO = 0;
          this.newFileSol.DISPLAY_BTN = true;
          this.newFileSol.DES_TIP = "Fianza";
          this.data.solDetFianza.lstFilesFia.push(this.newFileSol);
        }if(fileRec.length == 0){
          this.newFileSol = new fileFianza();
          this.newFileSol.ID_TIPO = 7;
          this.newFileSol.ID_DOCUMENTO = 0;
          this.newFileSol.DISPLAY_BTN = true;
          this.newFileSol.DES_TIP = "Recibo";
          this.data.solDetFianza.lstFilesFia.push(this.newFileSol);
        }if(fileXml.length == 0){
          this.newFileSol = new fileFianza();
          this.newFileSol.ID_TIPO = 8;
          this.newFileSol.ID_DOCUMENTO = 0;
          this.newFileSol.DISPLAY_BTN = true;
          this.newFileSol.DES_TIP = "XMLRec";
          this.data.solDetFianza.lstFilesFia.push(this.newFileSol);
        }if(fileFiaXml.length == 0){
          this.newFileSol = new fileFianza();
          this.newFileSol.ID_TIPO = 33;
          this.newFileSol.ID_DOCUMENTO = 0;
          this.newFileSol.DISPLAY_BTN = true;
          this.newFileSol.DES_TIP = "XMLFia";
          this.data.solDetFianza.lstFilesFia.push(this.newFileSol);
        }

      }else if(this.data.solDetFianza.lstFilesFia.length == 3){ 
        this.newFileSol = new fileFianza();
        this.newFileSol.ID_TIPO = 33;
        this.newFileSol.ID_DOCUMENTO = 0;
        this.newFileSol.DISPLAY_BTN = true;
        this.newFileSol.DES_TIP = "XMLFia";
        this.data.solDetFianza.lstFilesFia.push(this.newFileSol);
      }
    }
    
    if(this.data.solDetFianza.lstDocsFia){
      this.btnDis = this.data.solDetFianza.lstDocsFia.length == 0 ? true : false;
    }else{
      this.btnDis= true;
    }

    /////////////////////////////// Validacion FORM //////////////////////////////////////

    this.fianzaForm = this.formBuilder.group({
      numFia:   [{value: '', disabled: true}, Validators.compose([Validators.required])],
      numInc:   [{value: '0', disabled: true}, Validators.compose([Validators.required])],
      vigFia:   [{value: '', disabled: true}, Validators.compose([Validators.required])],
      monFia:   [{value: '', disabled: true}, Validators.compose([Validators.required])],
      slAfin:   [{value: '', disabled: true}, Validators.compose([Validators.required])],
      descSol:  [{value: '', disabled: true}, Validators.compose([Validators.required])],
      benFia:   [{value: '', disabled: true}, Validators.compose([Validators.required])],
      emiFia:   [{value: '', disabled: true}, Validators.compose([Validators.required])],
      primTot:  [{value: '', disabled: true}, Validators.compose([Validators.required])],
      cumFia:   [{value: '', disabled: true}, Validators.compose([Validators.required])]
    });
  
    this.reciboForm = this.formBuilder.group({
      folRec:     [{value: '', disabled: true}, Validators.compose([Validators.required])],
      dateRec:    [{value: '', disabled: true}, Validators.compose([Validators.required])],
      primNeta:   [{value: '', disabled: true}, Validators.compose([Validators.required])],
      comRec:     [{value: '', disabled: true}, Validators.compose([Validators.required])],
      statusRec:  [{value: '', disabled: true}, ],
      uuidRec:    [{value: '', disabled: true}, Validators.compose([Validators.required])],
      tipMonRec:  [{value: '', disabled: true}, Validators.compose([Validators.required])],
    });

    //Deshabilita los campos para los usuarios Ordas-Howden y PROVEEDORES
    if(this.dataUsuFia.CvePerfil != 7 && this.dataUsuFia.CvePerfil != 8){
    
    
      this.contracForm = this.formBuilder.group({
        contrAfi:     [ {value: '', disabled: true},  Validators.compose([Validators.required])],
        dateContr:    [ {value: '', disabled: true},  Validators.compose([Validators.required])],
        dateCumContr: [ {value: '', disabled: true},  Validators.compose([Validators.required])],
        rfcFia:       [ {value: '', disabled: true},  ],
        gerContr:     [ {value: '', disabled: true} , ],
        relA:         [ {value: '', disabled: true} , Validators.compose([Validators.required])],
        proyectContr: [ {value: '', disabled: true} , ],
      });

      this.showButton = true;
      this.contracForm.controls['contrAfi'].enable();
      this.contracForm.controls['dateContr'].enable();
      this.contracForm.controls['dateCumContr'].enable();
      this.contracForm.controls['relA'].enable();
      
      this.fianzaForm.controls['numFia'].enable();
      this.fianzaForm.controls['numInc'].enable();
      this.fianzaForm.controls['vigFia'].enable();
      this.fianzaForm.controls['monFia'].enable();
      this.fianzaForm.controls['emiFia'].enable();
      this.fianzaForm.controls['primTot'].enable();
      this.fianzaForm.controls['cumFia'].enable();

      this.reciboForm.controls['folRec'].enable();
      this.reciboForm.controls['dateRec'].enable();
      this.reciboForm.controls['primNeta'].enable();
      this.reciboForm.controls['comRec'].enable();
      this.reciboForm.controls['statusRec'].enable();
      this.reciboForm.controls['uuidRec'].enable();
      this.reciboForm.controls['tipMonRec'].enable();

      let numContr =  (this.data.solDetFianza.CONTRAC_SOL.NUM_CONTR == null) ? "" :
      this.data.solDetFianza.CONTRAC_SOL.NUM_CONTR;
      
      let numFia =  (this.data.solDetFianza.FIANZA_SOL.NUM_FIA == null) ? "" :
      this.data.solDetFianza.FIANZA_SOL.NUM_FIA;

      this.isActiveFia = (numContr.length > 0) ? false : true ; 
      this.isActiveRec = (numFia.length > 0) ? false : true ; 
      
    }else{
     
      this.contracForm = this.formBuilder.group({
        contrAfi:     [ {value: '', disabled: true}],
        dateContr:    [ {value: '', disabled: true}],
        dateCumContr: [ {value: '', disabled: true}],
        rfcFia:       [ {value: '', disabled: true}],
        gerContr:     [ {value: '', disabled: true}],
        relA:         [ {value: '', disabled: true}],
        proyectContr: [ {value: '', disabled: true}],
      });

      this.isActiveFia = (this.contracForm.invalid == true) ? true : false ; 
      this.isActiveRec = (this.fianzaForm.invalid == true) ? true : false ; 

    }

    /////////////////////////////////////////////////////////////////////////////////////

    //Valor seteado para el status del Recibo IPR
    this.reciboForm.controls['statusRec'].setValue(this.data.solDetFianza.RECIBO_SOL.STATUS_REC);
    this.reciboForm.controls['tipMonRec'].setValue(this.data.solDetFianza.RECIBO_SOL.CVE_MONEDA);

    //Validacion de los campos del formulario
    this.ComentForm = this.formBuilder.group({
      descComentario:     ['', Validators.compose([Validators.required]) ],
    });
   
    this.onLinkClick(0);

    let cvePerfil = this.dataUsuFia.CvePerfil;
    let cve_afianzadora = this.data.solDetFianza.FIANZA_SOL.CVE_AFIAN;
    //let statusRec = this.data.solDetFianza.RECIBO_SOL.STATUS_REC;
    let id_grupo : Number = this.dataUsuFia.id_grupo;   

    //FGonzalez: validamos si se trata de un proveedor
    if (cvePerfil == 8){

      this.reciboForm.controls['statusRec'].disable();     

      //Si el estatus del recibo es diferenete de PAGADO no se muetsra el panel al provedor
      // if(statusRec != 2){      
      //   this.divArchivos = false;
      // }
    }

    this.solicitudService.getAfianzadoras().subscribe(
      data => {
        //Filtramos por grupo    
        var arrAfianz =  JSON.stringify(data.afianzadorasDTO);  
        this.solNewAfia.lstAfianzadoras = JSON.parse(arrAfianz).filter(x => x.ID_GRUPO == id_grupo);
        this.fianzaForm.controls['slAfin'].setValue(cve_afianzadora);
        
        if (cvePerfil == 6){
          this.fianzaForm.controls['slAfin'].enable(); 
        }        
      },
      // Si ducede algun error se limpia la sessión y redirige al login
      error => {
        this.spinner.hide();
        this.securityServices.clearSession();
        console.log('oops: ', error)
      }
    );

    //Mantiene abierto el cuadro de comengtarios
    document.getElementById("cta").click();
    
    // #Cancelaciones -> Ahernandezh 02/02/2021
    this.CancelacionForm = this.formBuilder.group(
    {
      comentario_cancelacion:     ['', Validators.compose([Validators.required]) ],
    });


  }

  //****************************************************************************************************/
  
  cargarDocComent() 
  {
      this.fileUpload = document.getElementById("docComent") as HTMLInputElement;
      this.fileUpload.click();
  }

  onChangeDocComent(event: Event) {
    let evFiles = event.target['files'];
    let tipoDocu : number = 32

    this.fileUpload = document.getElementById('docComent') as HTMLInputElement;

    let nomArchivo = evFiles[0].name.length
    if(evFiles[0].name.length > 10){
      nomArchivo = evFiles[0].name.substring(0, 5) + '...';
    }
    document.getElementById("nameDoc").innerHTML = nomArchivo;

    if (event.target['files']) {
      this.readFiles(event.target['files'], 0, tipoDocu);
    }
  };
  
  GetComentariosSol(numSol){

    this.solicitudService.getComentariosSol(numSol).subscribe(
      data => 
      {
        if (data) 
        {              
            //deserealizo obj Usu
            var objAllSt = JSON.stringify(data);
            //serealizo obj usu
            this.comentariosSol = JSON.parse(objAllSt).comentariosSol; 
            this.showBtnCom = (this.comentariosSol.length > 0) ? false : true;
            this.comentariosSol.forEach(element => 
            {
              let responseDate = moment(element.fecComentario.toString(), 'DD/MM/YYYY, h:mm').toDate();
              element.fecComentario = moment(responseDate).locale('es-mx').fromNow();      
              
              // #Cancelaciones -> Ahernandezh 02/02/2021
              let docInterno = this.comentariosSol[0].documentosComentarioSol[0];
              for (var clave in docInterno)
              {
                if (clave == 'ID_TIPO' && docInterno.hasOwnProperty)
                {
                  if(docInterno[clave] == 34)
                  {
                    this.ComentarioCancelacion = element.comentario;
                    break;
                  }
                }
              }
              // #FinCancelaciones
              
            });
        }
        // Si no trea la data correctamente o trae otra cosa se limpia la sesión y redirige al login
        else{
          this.securityServices.clearSession();
        } 
      },
      // Si ducede algun error se limpia la sessión y redirige al login
      error => {
        this.spinner.hide();
        this.dialogRef.close();
        this.securityServices.clearSession();
        console.log('oops: ', error)
      }
    );
  }
  ///////////////////////////////////// FGONZALEZ /////////////////////////
  onAltaComent(){

    this.spinner.show();

    this.postRegNewComent.userId = this.session.get("usuIdFia");
    this.postRegNewComent.numSol = this.numSolucion;
    this.postRegNewComent.idDoc = 0;
    this.postRegNewComent.desComent = this.ComentForm.get('descComentario').value;
    this.postRegNewComent.fileComentDTO = this.arrFileComent;

    this.solicitudService.postNuevoComentario(this.postRegNewComent).subscribe(
      data => {

        if(data.codeResult == 0){
          
          this.GetComentariosSol(this.numSolucion);
          this.spinner.hide();  
          
          Swal.fire(
            'Gurdado!',
            '¡Comentario: gurdado con éxito!',
            'success'
          )
          
          this.arrFileComent = [];
          this.ComentForm.reset();
          document.getElementById("new").click();
          document.getElementById("nameDoc").innerHTML = 'Archivo';

        }else{
          this.spinner.hide();  
          Swal.fire(
            'Error!',
            '¡Contactar a Sistemas Ordás-Howden!',
            'error'
          )
        }
      },
      // Si ducede algun error se limpia la sessión y redirige al login
      error => {
        this.spinner.hide();
        this.dialogRef.close();
        this.securityServices.clearSession();
        console.log('oops: ', error)
      }
    );
  }

  closeFormDialog() {
    this.display = false;
    this.closeSolFianzaEvent.emit();
  }

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
    this.formControl.hasError('email') ? 'Not a valid email':'';

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

 
  DownLoadFiles(file){
   
    this.spinner.show();

    this.solicitudService.getFile(file.ID_DOCUMENTO).subscribe(
      data => {

        var ext = this.getTypeFile(file.NOM_DOCUMENTO);
        var blob = new Blob([data], {type: ext} );
        this.spinner.hide();
        
        return FileSaver.saveAs(blob, file.NOM_DOCUMENTO);
        },
        // Si ducede algun error se limpia la sessión y redirige al login
        error => {
          this.spinner.hide();
          this.dialogRef.close();
          this.securityServices.clearSession();
          console.log('oops: ', error)
        }

    );
  }    

  getTypeFile(extFile) {
    
    var fileType = 'application/octet-stream';
    var fileExtension = extFile.split('.').pop();

    if (fileExtension == 'jpg') {
        fileType = 'image/jpg';
    }
    else if (fileExtension == 'png') {
        fileType = 'image/png';
    }else if (fileExtension == 'gif') {
        fileType = 'image/gif';
    }else if (fileExtension == 'tiff') {
        fileType = 'image/tiff';
    }else if (fileExtension == 'jpeg' || fileExtension == 'jpg') {
        fileType = 'image/jpeg';
    }else if (fileExtension == 'pdf') {
      fileType = 'application/pdf';
    }else if (fileExtension == 'txt') {
      fileType = 'text/plain';
    }else if (fileExtension == 'doc' || fileExtension == 'docx' ) {
      fileType = 'application/vnd.ms-word';
    }else if (fileExtension == 'xls ' || fileExtension == 'xlsx') {
      fileType = 'application/vnd.ms-excel';
    }else if (fileExtension == 'csv') {
      fileType = 'text/csv';
    }
    
    return fileType;

  }

  //******************* FILE UPLOAD ******************************************/
  onClickFile(id, numDoc) {
    this.stMatPro = "in"+id;
    this.numDocument = numDoc;
    var tipDoc =  this.arrTipDocs.filter(
                  doc => doc.CVE_TIPO_DOC === id);
      this.fileUpload = document.getElementById(this.stMatPro) as HTMLInputElement;
      this.fileUpload.click();
      this.stMatPro = "void";
  }

  onChange(event: Event, id) 
  {
    
    let evFiles = event.target['files'];
    let spId = "sp"+id;
  
    this.fileUpload = document.getElementById(id) as HTMLInputElement;
    
    switch(id){
      case 3:
        document.getElementById("spFianza").innerHTML = evFiles[0].name;
        break;

      case 7:
        document.getElementById("spRecibo").innerHTML = evFiles[0].name;
        break;

      case 8:
        document.getElementById("spXMLRec").innerHTML = evFiles[0].name;
        break;
      
      case 33:
          document.getElementById("spXMLFia").innerHTML = evFiles[0].name;
          break;
      
      case 34:  // #Cancelaciones -> Ahernandezh 02/02/2021
        document.getElementById("spDocCancelacion").innerHTML = evFiles[0].name;
        break;

      default:
        document.getElementById(spId).innerHTML = evFiles[0].name;
      break;


    }
   
    if (event.target['files']) {
      this.readFiles(event.target['files'], 0, id);
    }
  
  };

  private readFiles(filesLoad: FileDataForm, index: number, idDoc: number) 
  {
    if (idDoc == 32 || idDoc == 34) // #Cancelaciones -> Ahernandezh 02/02/2021
    {
      let file = filesLoad[index];
      let fileComent: FileComentDTO = new FileComentDTO();
    
      fileComent.nameFile = file.name;
      fileComent.numDoc = this.numDocument;
      fileComent.sizeFile = this.formatBytes(file.size, 0).toString();
      fileComent.extFile = file.name.split('.').pop();
      fileComent.idTipFile = idDoc;
      fileComent.typeFile = file.type.toString();

      this.fileReader.onload = () => 
      {
        fileComent.dataFile = this.fileReader.result.toString();
        this.arrFileComent = this.arrFileComent.filter(function (p){ return p.idTipFile != idDoc});
        this.arrFileComent.push(fileComent);
      };
      this.fileReader.readAsDataURL(file);
    }
    else
    {
      let file = filesLoad[index];
      let fileFiaSol: FilesSolNuevaDTO = new FilesSolNuevaDTO();
     
      fileFiaSol.nameFile = file.name;
      fileFiaSol.numDoc = this.numDocument;
      fileFiaSol.sizeFile = this.formatBytes(file.size, 0).toString();
      fileFiaSol.extFile = file.name.split('.').pop();
      fileFiaSol.idTipFile = idDoc;
      fileFiaSol.typeFile = file.type.toString();
  
      this.fileReader.onload = () => 
      {
        fileFiaSol.dataFile = this.fileReader.result.toString();
        //Quitar antes si existe un id
        this.arrFileDocs = this.arrFileDocs.filter(function (p){ return p.idTipFile != idDoc});
        this.arrFileDocs.push(fileFiaSol);     
      };
      this.fileReader.readAsDataURL(file);
    }
  }

  formatBytes(bytes, decimals) {
    if (bytes == 0) return 0;
    var k = 1024;
    var dm = decimals + 1 || 2;
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    var i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

  //******************* SAVE EMISION ******************************************/
 
  onSaveContrato(){


    if(this.dataUsuFia.CvePerfil != 7 && this.dataUsuFia.CvePerfil != 8){
      Swal.fire({
        title: 'Actualizar la Solictud ' + this.numSolucion,
        text: "¿Estas seguro de actualiar esta solictud?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'SI, estoy de acuerdo!'

      }).then((result) => {

        
        if(!result.dismiss){

          this.spinner.show();
          //Creacion del obj Contrato
          this.postRegUpdCon.ID_CONTR =  this.idContr; //Obtner de la respuesta de [data.solDetFianza]
          this.postRegUpdCon.NUM_CONTR =  (this.contracForm.get('contrAfi').value == null) ? "" :   
                                            this.contracForm.get('contrAfi').value;
          this.postRegUpdCon.ID_FIA = this.idFia;
          this.postRegUpdCon.NUM_SOL = this.numSolucion;
          this.postRegUpdCon.ID_USUARIO = this.idNumUsu;
          this.postRegUpdCon.CVE_PAQUETE = this.numTipSol;
          this.postRegUpdCon.CVE_AFIAN = this.numAfianzadora;
          this.postRegUpdCon.CVE_COMPANY =  this.numCorp;
          this.postRegUpdCon.RFC_FIADO =  this.contracForm.get('rfcFia').value;
          this.postRegUpdCon.RELA_A =  (this.contracForm.get('relA').value == null) ? "" :
                                        this.contracForm.get('relA').value;
          this.postRegUpdCon.FEC_CONTR = this.contracForm.get('dateContr').value;
          this.postRegUpdCon.FEC_CUMP_CONTR = this.contracForm.get('dateCumContr').value;
          this.postRegUpdCon.FilesContratoDTO = this.arrFileDocs;
          
          //Llamado del servicio Guardar Contrato
          this.solicitudService.postUpdateContrato(this.postRegUpdCon).subscribe(
            data => {
              var prueba = data;

              if(data.codeResult == 0){

                this.spinner.hide();
                
                Swal.fire({
                  type: 'success',
                  title: 'Actualizado!',
                  html: '¡Solicitud: <strong>' + this.numSolucion + '</strong> actualizada con exito!',
                  timer: 2000,
                  onBeforeOpen: () => {
                   
                  },
                  onClose: () => {
                    this.dialogRef.close();
                    this.router.navigate(["/solicitudes", 0]).then( (e) => {
                    });
                  }
                });

              }else{
                Swal.fire(
                  'Error!',
                  '¡Contactar a Sistemas Ordás-Howden!',
                  'error'
                )
                
                this.dialogRef.close();

                this.router.navigate(["/solicitudes", 0]).then( (e) => {
                });
              
              }
            },
            // Si ducede algun error se limpia la sessión y redirige al login
            error => {
              this.spinner.hide();
              this.dialogRef.close();
              this.securityServices.clearSession();
              console.log('oops: ', error)
            }

          );

        }//Fin if

      });//Fin Swal
    }else{

      if ( this.arrFileDocs.length > 0  ) {
        Swal.fire({
          title: 'Actualizar la Solictud ' + this.numSolucion,
          text: "¿Estas seguro de actualiar esta solictud?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'SI, estoy de acuerdo!'
        }).then((result) => {
          
          if(!result.dismiss){
  
            this.spinner.show();
            //Creacion del obj Contrato
            this.postRegUpdCon.ID_CONTR =  this.idContr; //Obtner de la respuesta de [data.solDetFianza]
            this.postRegUpdCon.NUM_CONTR =  (this.contracForm.get('contrAfi').value == null) ? "" :   
                                              this.contracForm.get('contrAfi').value;
            this.postRegUpdCon.ID_FIA = this.idFia;
            this.postRegUpdCon.NUM_SOL = this.numSolucion;
            this.postRegUpdCon.ID_USUARIO = this.idNumUsu;
            this.postRegUpdCon.CVE_PAQUETE = this.numTipSol;
            this.postRegUpdCon.CVE_AFIAN = this.numAfianzadora;
            this.postRegUpdCon.CVE_COMPANY =  this.numCorp;
            this.postRegUpdCon.RFC_FIADO =  this.contracForm.get('rfcFia').value;
            this.postRegUpdCon.RELA_A =  (this.contracForm.get('relA').value == null) ? "" :
                                          this.contracForm.get('relA').value;
            this.postRegUpdCon.FEC_CONTR = this.contracForm.get('dateContr').value;
            this.postRegUpdCon.FEC_CUMP_CONTR = this.contracForm.get('dateCumContr').value;
            this.postRegUpdCon.FilesContratoDTO = this.arrFileDocs;
            
            //Llamado del servicio Guardar Contrato
            this.solicitudService.postUpdateContrato(this.postRegUpdCon).subscribe(
              data => {
                var prueba = data;
  
                if(data.codeResult == 0){
  
                  this.spinner.hide();
                  
                  Swal.fire({
                    type: 'success',
                    title: 'Actualizado!',
                    html: '¡Solicitud: <strong>' + this.numSolucion + '</strong> actualizada con exito!',
                    timer: 2000,
                    onBeforeOpen: () => {
                      
                    },
                    onClose: () => {
                      this.dialogRef.close();
                      this.router.navigate(["/solicitudes", 0]).then( (e) => {
                      });
                    }
                  });
  
                }else{
                  Swal.fire(
                    'Error!',
                    '¡Contactar a Sistemas Ordás-Howden!',
                    'error'
                  )
                  
                  this.dialogRef.close();
  
                  this.router.navigate(["/solicitudes", 0]).then( (e) => {
                  });
                
                }
              },
              // Si ducede algun error se limpia la sessión y redirige al login
              error => {
                this.spinner.hide();
                this.dialogRef.close();
                this.securityServices.clearSession();
                console.log('oops: ', error)
              }
  
            );
  
          }//Fin if
  
        });//Fin Swal
  
      }else{
            this.spinner.hide();  
            Swal.fire(
              'Error!',
              '¡Debes Actualizar el documento!',
              'error'
            )
      }


    }


   
  
  }
 
  onSaveFia(){

    Swal.fire({
      title: 'Actualizar la Solictud ' + this.numSolucion,
      text: "¿Estas seguro de actualiar esta solictud?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'SI, estoy de acuerdo!'
    }).then((result) => {


      if(!result.dismiss){

        this.spinner.show();
   
        this.postRegUpdFia.ID_FIA = this.idFia; //la traera del objeto de consulta
        this.postRegUpdFia.NUM_SOL = this.numSolucion;
        this.postRegUpdFia.NUM_FIA = this.fianzaForm.get('numFia').value;
        this.postRegUpdFia.NUM_INC = this.fianzaForm.get('numInc').value;
        this.postRegUpdFia.CVE_AFIAN = this.fianzaForm.get('slAfin').value;
        this.postRegUpdFia.CVE_PAQUETE =  this.numTipSol;
        this.postRegUpdFia.FEC_EMI_FIA = this.fianzaForm.get('emiFia').value;
        this.postRegUpdFia.FEC_VIG_INI_FIA = this.fianzaForm.get('vigFia').value.begin;
        this.postRegUpdFia.FEC_VIG_FIN_FIA = this.fianzaForm.get('vigFia').value.end;
        this.postRegUpdFia.FEC_CUM_FIA = this.fianzaForm.get('cumFia').value;
        this.postRegUpdFia.ID_USUARIO = this.idNumUsu;
        this.postRegUpdFia.CVE_PROV = this.numCorp;
        this.postRegUpdFia.CVE_GRUPO = this.numGrupo;
        this.postRegUpdFia.CVE_BENEF = this.numBenef;
        this.postRegUpdFia.MONTO_FIA = this.fianzaForm.get('monFia').value;
        this.postRegUpdFia.PRIMA_TOTAL_FIA = this.fianzaForm.get('primTot').value;


        this.postRegUpdFia.FilesFianzaDTO = this.arrFileDocs;

         //Llamado del servicio Guardar Fianza
         this.solicitudService.postUpdateFianza(this.postRegUpdFia).subscribe(
            data => {
              var prueba = data;

              if(data.codeResult == 0){

                this.spinner.hide();
                
                Swal.fire({
                  type: 'success',
                  title: 'Actualizado!',
                  html: '¡Solicitud: <strong>' + this.numSolucion + '</strong> actualizada con exito!',
                  timer: 2000,
                  onBeforeOpen: () => {
                   
                  },
                  onClose: () => {
                    this.dialogRef.close();
                    this.router.navigate(["/solicitudes", 0]).then( (e) => {
                    });
                  }
                });

              }else{
                Swal.fire(
                  'Error!',
                  '¡Contactar a Sistemas Ordás-Howden!',
                  'error'
                )
                
                this.dialogRef.close();

                this.router.navigate(["/solicitudes", 0]).then( (e) => {
                });
              
              }
            },
            // Si ducede algun error se limpia la sessión y redirige al login
            error => {
              this.spinner.hide();
              this.dialogRef.close();
              this.securityServices.clearSession();
              console.log('oops: ', error)
            }
        );
      

      }//Fin if



  });//Fin fire

  }

  onSaveRec(){

    Swal.fire({
      title: 'Actualizar la Solictud ' + this.numSolucion,
      text: "¿Estas seguro de actualiar esta solictud?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'SI, estoy de acuerdo!'
    }).then((result) => {


      if(!result.dismiss){

        this.spinner.show();
 
        this.postRegUpdRec.ID_REC = this.data.solDetFianza.RECIBO_SOL.ID_REC; //la traera del objeto de consulta
        this.postRegUpdRec.NUM_REC = this.reciboForm.get('folRec').value;
        this.postRegUpdRec.NUM_SOL = this.numSolucion;
        this.postRegUpdRec.ID_USUARIO = this.idNumUsu;
        this.postRegUpdRec.CVE_AFIAN = this.numAfianzadora;
        this.postRegUpdRec.UUID_REC = this.reciboForm.get('uuidRec').value;
        this.postRegUpdRec.FEC_REC = this.reciboForm.get('dateRec').value;
        this.postRegUpdRec.PRIMA_NETA = this.reciboForm.get('primNeta').value;
        this.postRegUpdRec.COM_REC = this.reciboForm.get('comRec').value;
        this.postRegUpdRec.STATUS_RECIBO = this.reciboForm.get('statusRec').value;  
        this.postRegUpdRec.TIPO_MONEDA = this.reciboForm.get('tipMonRec').value;  

         //Llamado del servicio Guardar Recibo
         this.solicitudService.postUpdateRecibo(this.postRegUpdRec).subscribe(
          data => {

            if(data.codeResult == 0){

              this.spinner.hide();
              
              Swal.fire({
                type: 'success',
                title: 'Actualizado!',
                html: '¡Solicitud: <strong>' + this.numSolucion + '</strong> actualizada con exito!',
                timer: 2000,
                onBeforeOpen: () => {
                 
                },
                onClose: () => {
                  this.dialogRef.close();
                  this.router.navigate(["/solicitudes", 0]).then( (e) => {
                  });
                }
              });

            }else{
              Swal.fire(
                'Error!',
                '¡Contactar a Sistemas Ordás-Howden!',
                'error'
              )
              
              this.dialogRef.close();

              this.router.navigate(["/solicitudes", 0]).then( (e) => {
              });
            }
          },
          // Si ducede algun error se limpia la sessión y redirige al login
          error => {
            this.spinner.hide();
            this.dialogRef.close();
            this.securityServices.clearSession();
            console.log('oops: ', error)
          }

        );

      }//Fin if

  });//Fin fire

  }

  // #Cancelaciones -> Ahernandezh 02/02/2021
  cancelacion()
  {
    this.spinner.show();
    this.postRegNewComent.userId = this.session.get("usuIdFia");
    this.postRegNewComent.numSol = this.numSolucion;
    this.postRegNewComent.idDoc = 0;
    this.postRegNewComent.desComent = this.CancelacionForm.get('comentario_cancelacion').value;
    this.postRegNewComent.fileComentDTO = this.arrFileComent;
    if(this.arrFileComent.length == 0)
    {
      Swal.fire('Error!','¡Debes cargar el documento de cancelación!', 'error');  
      this.spinner.hide();
      return
    }
    this.solicitudService.CancelacionFianza(this.postRegNewComent).subscribe(
    data => 
    {

      if(data.codeResult == 0)
      {  
        this.GetComentariosSol(this.numSolucion);
        this.spinner.hide();  
        Swal.fire('Gurdado!','¡Cancelación: gurdado con éxito!','success');
        this.arrFileDocs = [];
        this.CancelacionForm.reset();
        // document.getElementById("new").click();
        document.getElementById("spDocCancelacion").innerHTML = 'Archivo';
        this.dialogRef.close();
      }
      else
      {
        this.spinner.hide();  
        Swal.fire('Error!','¡Contactar a Sistemas Ordás-Howden!','error');
      }
    },
    error => 
    {
      this.spinner.hide();
      this.dialogRef.close();
      this.securityServices.clearSession();
    });
  }
  // #FinCancelaciones

  //************************** DATE-RAGE-PICKER********************************************* */


  // Code behind
  saveDate(event: any) {

    // look at how the date is emitted from save
    console.log(event.target.value.begin);
    console.log(event.target.value.end);

    // change in view
    this.dateRangeDisp = event.target.value;

    // save date range as string value for sending to db
    var prueba = new Date(event.target.value.begin) + "|" + new Date(event.target.value.end);
    // ... save to db
  }

//*************************************************************************************** */

  // Control click Tab Navigation and Styles Acciona/Ordas IPR
  onLinkClick(index) {
    this.inTabSel = index;

    var css = document.createElement("style");
    var cssAccSt = document.createElement("style");
    var cssAccTm = document.createElement("style");
    var cssAccFrmRec = document.createElement("style");
    css.type = "text/css";
    cssAccSt.type = "text/css";
    cssAccTm.type = "text/css";
    cssAccFrmRec.type = "text/css";
    
    switch (index) {
      case 0:
          if(this.dataUsuFia.CvePerfil == 7 || this.dataUsuFia.CvePerfil == 8 ){
            css.innerHTML = ".mat-dialog-content { height : 610px !important; max-height: 110vh !important; }";
          }else{
            css.innerHTML = ".mat-dialog-content { height : 610px !important; max-height: 110vh !important; } ";
          }
          document.body.appendChild(css);
          this.dvFootCom = "dvfootCardCon";
          break;
      case 1:

          if(!this.isActiveFia){
            if(this.dataUsuFia.CvePerfil == 7 || this.dataUsuFia.CvePerfil == 8){
              css.innerHTML = ".mat-dialog-content { height : 610px !important; max-height: 110vh !important; } .info-box { height: 62px !important;}";  
            }else{
              css.innerHTML = ".mat-dialog-content { height : 610px !important; max-height: 110 vh !important; } .info-box { height: 90px !important;} ";
            }
            document.body.appendChild(css);
            this.dvFootCom = "dvfootCardFia";
          }
          break;
      case 2:

          if(!this.isActiveRec){
            css.innerHTML = ".mat-dialog-content { height : 520px !important; max-height: 90vh !important; }";
            if(this.dataUsuFia.CvePerfil == 7 || this.dataUsuFia.CvePerfil == 8){
              cssAccSt.innerHTML = ".stRecAcc {  display: inline-block; position: relative; }";
              cssAccTm.innerHTML = ".tpMonAcc { display: inline-block; position: relative; top: 0px; left: 17px; }";
              cssAccFrmRec.innerHTML = ".frmRec {margin-top: 40px !important;}";
              document.body.appendChild(cssAccSt);
              document.body.appendChild(cssAccTm);
              document.body.appendChild(cssAccFrmRec);
            }

            document.body.appendChild(css);
            this.dvFootCom = "dvfootCardRec";
          }
          break;
    }

  
  }


  //for diferent Style Comentarios    
  getStyleClass() { 
      
    switch (this.inTabSel) {
      case 0:
        return "cssMCardComentsContr";  
      case 1:
          if(this.dataUsuFia.CvePerfil != 7 && this.dataUsuFia.CvePerfil != 8){
            return "cssMCardComentsFiaGO";
          }else{
            return "cssMCardComentsFia";
          }
        
      case 2:
          if(this.dataUsuFia.CvePerfil != 7 && this.dataUsuFia.CvePerfil != 8){
            return "cssMCardComentsRecGO";
          }else{
            return "cssMCardComentsRec";
          }
        
      default:
        return "cssMCardComentsContr";
    }
    
  }


  viewDocComent(){
    this._scrollToService.scrollTo(config);
  }


  bitacoraPDF(num_sol: number) { 
      
    this.spinner.show();

    //Cargar los documentos de la solicitud
    this.solicitudService.ReportBitacoraSol(num_sol).subscribe(
      data => {
        var archivo = data.FileStream._buffer;
        this.solicitudService.downloadPDF(archivo, "Bitacora_Acciona_" + num_sol +"_");
        this.spinner.hide();
      },
      // Si ducede algun error se limpia la sessión y redirige al login
      error => {
        this.spinner.hide();
        this.securityServices.clearSession();
        console.log('oops: ', error)
      }
    );
  }

//****************************************************************************/

//******************* SAVE EMISION ******************************************/

  onAnuSol(){

    Swal.fire({
      title: 'Anular la Solictud ' + this.numSolucion,
      text: "¿Estas seguro de ANULAR esta solictud?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'SI, estoy de acuerdo!'
    }).then((result) => {


      if(!result.dismiss){

        this.spinner.show();
  
        this.postAnularSol.NUM_SOL = this.numSolucion;
        this.postAnularSol.ID_USUARIO = this.idNumUsu;
      
          //Llamado del servicio Guardar Recibo
          this.solicitudService.postAnularSolicitud(this.postAnularSol).subscribe(
          data => {

            if(data.codeResult == 0){

              this.spinner.hide();
              
              Swal.fire({
                type: 'success',
                title: 'Actualizado!',
                html: '¡Solicitud: <strong>' + this.numSolucion + '</strong> ANULADA con exito!',
                timer: 2000,
                onBeforeOpen: () => {
                  
                },
                onClose: () => {
                  this.dialogRef.close();
                  this.router.navigate(["/solicitudes", 0]).then( (e) => {
                  });
                }
              });

            }else{
              Swal.fire(
                'Error!',
                '¡Contactar a Sistemas Ordás-Howden!',
                'error'
              )
              
              this.dialogRef.close();

              this.router.navigate(["/solicitudes", 0]).then( (e) => {
              });
            }
          },
          // Si ducede algun error se limpia la sessión y redirige al login
          error => {
            this.spinner.hide();
            this.dialogRef.close();
            this.securityServices.clearSession();
            console.log('oops: ', error)
          }

        );

      }//Fin if

    });//Fin fire

  }

}
