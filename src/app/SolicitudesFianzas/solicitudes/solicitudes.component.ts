import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
declare var $;

@Component({
  selector: 'app-solicitudes',
  templateUrl: './solicitudes.component.html',
  styleUrls: ['./solicitudes.component.scss']
})
export class SolicitudesComponent implements OnInit {

  statusSol: number = 0;
  show: boolean = false;
  nameTit: string = "";
  showDash: boolean = false;
  showMenu: boolean = true;
  showNewSol: boolean = true;   

  constructor(
    private route: ActivatedRoute,
    private cd:ChangeDetectorRef,
    private router: Router,
  ) { }

  ngOnInit() {

    let JsonUsu =  JSON.parse(JSON.parse(sessionStorage.getItem('infoUser'))._value);

    if(JsonUsu.CvePerfil == 6){
      this.showNewSol = false;
    }

    //Get params the other component
    this.route.paramMap.subscribe(params => { 
      this.statusSol = Number.isNaN(parseInt(this.route.snapshot.paramMap.get('id'))) ? 0 : parseInt(this.route.snapshot.paramMap.get('id'));
        this.cd.markForCheck();
    })

    if(this.statusSol>0){
      this.nameTit = "Dashboard";
      this.showDash = true;
      this.showMenu = false;
    }




    var pBtnMenu = document.getElementById("push-menu");
    document.getElementById("push-menu").click();
    

    pBtnMenu.addEventListener("click", () => {
      //alert("Prueba");
    });

  }

  ngOnDestroy(): void {
    document.body.className = '';
  }


  accSol(url, id) {

    if(id>0){
      this.router.navigate([url, id]).then( (e) => {
        if (e) {
        } else {
        }
      });
    }else{
      this.router.navigate([url]).then( (e) => {
        if (e) {
        } else {
        }
      });
    }
    
  }


}
