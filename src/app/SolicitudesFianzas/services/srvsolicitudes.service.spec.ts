import { TestBed, inject } from '@angular/core/testing';

import { SrvsolicitudesService } from '../services/srvsolicitudes.service';

describe('SrvsolicitudesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SrvsolicitudesService]
    });
  });

  it('should be created', inject([SrvsolicitudesService], (service: SrvsolicitudesService) => {
    expect(service).toBeTruthy();
  }));
});
