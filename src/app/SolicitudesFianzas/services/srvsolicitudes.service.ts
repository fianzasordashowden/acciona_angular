import { Injectable } from '@angular/core';

//Import variables globales
import { rootGlobals } from 'src/app/rootGlobals';

//IPR Peticiones Rest
import { HttpClient, HttpResponse, HttpHeaders, HttpParams, HttpErrorResponse,} from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { Response, RequestMethod } from '@angular/http';

import { map, catchError, tap } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import {JsonConvert, OperationMode, ValueCheckingMode} from "json2typescript"

//Models
import {ResponseSol} from '../../Models/responseSol'
import { PostRegisterSolNueva, PostRegisterSolAct, PostRegisterContrato, PostRegisterFianza, PostRegisterRecibo, PostAnularSolicitud } from 'src/app/Models/PostRegisterSolNueva';
import { solicitudAccionaFianzasDTO, contracNotifiDTO } from 'src/app/Models/solicitudes';

import { SecurityService } from 'src/app/Servicios/security.service';
import { PostRegisterComentNuevo } from 'src/app/Models/PostRegisterComentNuevo';
import * as moment from 'moment';
import { PostInfoUser } from 'src/app/Models/userLog';

@Injectable({
  providedIn: 'root'
})
export class SrvsolicitudesService {

   //FGonzalez: declaramos la variable que usaremos para alamacenar el token
   tokenUsuFia: any;  
   infUser = new PostInfoUser ();  
   // Asiganamos nuestras URL's de nuestros servicios
   private urlGetSolicitudes:  string = "Solicitudes/GetSolicitudesAcc";
   private urlNuevaSol:        string = "Solicitudes/ObtSolicitudNueva";
   private urlUpFilesSolNew:   string = "Solicitudes/upFilesSolNew";
   private urlGetFilesSolAcc:  string = "Solicitudes/GetFilesSolAcc";
   private urlGetFile:         string = "Solicitudes/GetFile";
   private urlUpdateFia:       string = "Solicitudes/UpdateFia";
   private urlUpdateContact:   string = "Solicitudes/UpdContract";
   private urlUpdateFianza:    string = "Solicitudes/UpdFianza";
   private urlUpdateRecibo:    string = "Solicitudes/UpdRecibo";
   private urlComentariosSol:    string = "Solicitudes/GetComentariosSol";
   private urlRegisterComentSol: string = "Solicitudes/RegisterComentSol";
   private urlReportPDFSol:      string = "Reportes/RptSolicitudes";
   private urlAfianzadoras:      string = "Solicitudes/ObtAfianzadoras";
   private urlNotificaciones:    string = "Solicitudes/PushNotiAdfia";
   private urlBitacoraPDFSol:    string = "Reportes/RptBitacoraComents";
   private urlManualSolicitudes: string = 'Solicitudes/GetManualAcciona';
   private urlAnularSolicitud:    string = "Solicitudes/AnularSol";
   private urlCancelacionFianza:    string = "Solicitudes/CancelacionFianza"; // #Cancelaciones -> Ahernandezh 02/02/2021
   
   
   constructor(
     private http: HttpClient,
     private securityServices: SecurityService,
     // Traemos nustras variables globales
     public globals: rootGlobals,
     ) {       
       // FGonzalez: Lo primero que se hace es validar si la sessión esta activa.
       // En caso de que no la misma función validateSession() nos redireccionara al login
       this.securityServices.validateSession();              
     }  
 

     
     getAllSolicitudes(dataForm: PostInfoUser): Observable<any> {
 
       try{
         
        // FGonzalez: Armamos el token con la variable de sesion
        this.tokenUsuFia = JSON.parse(sessionStorage.getItem('TokenUsuFia'))._value;
 
         //Url del servicio
         //let urlGetSolicitudes: string = this.globals.GetUrlServicio() + this.urlGetSolicitudes + "?infoUsu=" + JSON.stringify(this.infUser);
         let urlGetSolicitudes: string = this.globals.GetUrlServicio() + this.urlGetSolicitudes;
 
         let jsonCrt = new JsonConvert();
         jsonCrt.operationMode = OperationMode.LOGGING; // print some debug data
         jsonCrt.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
         jsonCrt.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null
 
         // Armamos los headers
         var headers = new HttpHeaders()
         .set('Content-Type', 'application/json')
         .set('Authorization', this.tokenUsuFia)
         .set('Accept', 'application/json');
 
         // Armamos el body para este servicio ira vacio
         //const body = new HttpParams().toString();
 
        //  return this.http
        //  .post<solicitudAccionaFianzasDTO>(
        //    urlGetSolicitudes, 
        //    body,    
        //    {headers: headers}    
        //  )
        //  .pipe(map(data => data));  
         
         // Armamos el body con los datos del formulario
         const body =  dataForm;
     
         return this.http
         // .post<solicitudComentarioSolDTO>(
         .post<any>(
            urlGetSolicitudes, 
            body,    
           {headers: headers}    
         )
         .pipe(map(data => data));


       }catch(e) { 
         this.securityServices.clearSession();
       }
   
     
     }
   
     getNuevaSolicitud(): Observable<any> {
       
       try {
         
         // FGonzalez: Armamos el token con la variable de sesion
         this.tokenUsuFia = JSON.parse(sessionStorage.getItem('TokenUsuFia'))._value;
     
         //Url del servicio
         let urlNuevaSol: string = this.globals.GetUrlServicio() + this.urlNuevaSol;
     
         let jsonCrt = new JsonConvert();
         jsonCrt.operationMode = OperationMode.LOGGING; // print some debug data
         jsonCrt.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
         jsonCrt.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null
     
         // Armamos los headers
         var headers = new HttpHeaders()
         .set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
         .set('Authorization', this.tokenUsuFia)
         .set('Accept', 'application/json');
     
         // Armamos el body para este servicio ira vacio
         const body = new HttpParams().toString();
     
         return this.http
         .post<solicitudAccionaFianzasDTO>(
           urlNuevaSol, 
           body,    
           {headers: headers}    
         )
         .pipe(map(data => data));  
 
         
       }catch(e) { 
         this.securityServices.clearSession();
       }
     }
 
     getAfianzadoras(): Observable<any> {
       
       try {
         
         // FGonzalez: Armamos el token con la variable de sesion
         this.tokenUsuFia = JSON.parse(sessionStorage.getItem('TokenUsuFia'))._value;
     
         //Url del servicio
         let urlAfianzadoras: string = this.globals.GetUrlServicio() + this.urlAfianzadoras;
     
         let jsonCrt = new JsonConvert();
         jsonCrt.operationMode = OperationMode.LOGGING; // print some debug data
         jsonCrt.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
         jsonCrt.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null
     
         // Armamos los headers
         var headers = new HttpHeaders()
         .set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
         .set('Authorization', this.tokenUsuFia)
         .set('Accept', 'application/json');
     
         // Armamos el body para este servicio ira vacio
         const body = new HttpParams().toString();
     
         return this.http
         .post<solicitudAccionaFianzasDTO>(
           urlAfianzadoras, 
           body,    
           {headers: headers}    
         )
         .pipe(map(data => data));  
 
       }catch(e) { 
         this.securityServices.clearSession();
       }
     }
   
     postNuevaSolicitud(dataForm: PostRegisterSolNueva): Observable<any> {   
       
       try {
         // FGonzalez: Armamos el token con la variable de sesion
         this.tokenUsuFia = JSON.parse(sessionStorage.getItem('TokenUsuFia'))._value;
     
         //Url del servicio
         let urlUpFilesSolNew: string = this.globals.GetUrlServicio() + this.urlUpFilesSolNew;
     
         let jsonCrt = new JsonConvert();
         jsonCrt.operationMode = OperationMode.LOGGING; // print some debug data
         jsonCrt.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
         jsonCrt.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null
     
         // Armamos los headers
         var headers = new HttpHeaders()
         .set('Content-Type', 'application/json')
         .set('Authorization', this.tokenUsuFia)
         .set('Accept', 'application/json');
     
         // Armamos el body con los datos del formulario
         const body = dataForm;
     
         return this.http
         .post<solicitudAccionaFianzasDTO>(
           urlUpFilesSolNew, 
           body,    
           {headers: headers}    
         )
         .pipe(map(data => data));
 
       }catch(e) { 
         this.securityServices.clearSession();
       }
       
      
     }
   
     postNuevoComentario(dataForm: PostRegisterComentNuevo): Observable<any> {   
       
       try {
         // FGonzalez: Armamos el token con la variable de sesion
         this.tokenUsuFia = JSON.parse(sessionStorage.getItem('TokenUsuFia'))._value;
     
         //Url del servicio
         let urlRegisterComentSol: string = this.globals.GetUrlServicio() + this.urlRegisterComentSol;
     
         let jsonCrt = new JsonConvert();
         jsonCrt.operationMode = OperationMode.LOGGING; // print some debug data
         jsonCrt.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
         jsonCrt.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null
     
         // Armamos los headers
         var headers = new HttpHeaders()
         .set('Content-Type', 'application/json')
         .set('Authorization', this.tokenUsuFia)
         .set('Accept', 'application/json');
     
         // Armamos el body con los datos del formulario
         const body = dataForm;
     
         return this.http
         // .post<solicitudComentarioSolDTO>(
         .post<PostRegisterComentNuevo>(
           urlRegisterComentSol, 
           body,    
           {headers: headers}    
         )
         .pipe(map(data => data));
 
       }catch(e) { 
         this.securityServices.clearSession();
       }
  
     }
 
     getFilesSolicitud(numSol: number): Observable<any> {
      
       try {
         // FGonzalez: Armamos el token con la variable de sesion
         this.tokenUsuFia = JSON.parse(sessionStorage.getItem('TokenUsuFia'))._value;
         
         //Url del servicio
         let urlGetFilesSolAcc: string = this.globals.GetUrlServicio() + this.urlGetFilesSolAcc + "?numSol=" + numSol;
     
         let jsonCrt = new JsonConvert();
         jsonCrt.operationMode = OperationMode.LOGGING; // print some debug data
         jsonCrt.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
         jsonCrt.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null
     
         // Armamos los headers
         var headers = new HttpHeaders()
         .set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
         .set('Authorization', this.tokenUsuFia)
         .set('Accept', 'application/json');
     
         // Armamos el body para este servicio ira vacio
         const body = new HttpParams().toString();
     
         return this.http.post(
           urlGetFilesSolAcc, 
           body,    
           {headers: headers}    
         )
         .pipe(map(data => data));
 
       }catch(e) { 
         this.securityServices.clearSession();
       }
    
     }
   
     getFile(numFile: number): Observable<any> {
       
       try {
         // FGonzalez: Armamos el token con la variable de sesion
         this.tokenUsuFia = JSON.parse(sessionStorage.getItem('TokenUsuFia'))._value;
     
         //Url del servicio
         let urlGetFile: string = this.globals.GetUrlServicio() + this.urlGetFile + "?numFile=" + numFile;
     
         let jsonCrt = new JsonConvert();
         jsonCrt.operationMode = OperationMode.LOGGING; // print some debug data
         jsonCrt.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
         jsonCrt.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null
     
         // Armamos los headers
         var headers = new HttpHeaders()
         .set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
         .set('Authorization', this.tokenUsuFia)
         .set('Accept', 'application/json');
     
         return this.http.post(urlGetFile, RequestMethod.Post, { headers: headers, responseType:'blob'})
         .map((x) => {
             return x;
         });
 
       }catch(e) { 
         this.securityServices.clearSession();
       }
 
     
     }
   
     postUpdateSolicitud(dataForm: PostRegisterSolAct): Observable<any> {
   
       try {
         // FGonzalez: Armamos el token con la variable de sesion
         this.tokenUsuFia = JSON.parse(sessionStorage.getItem('TokenUsuFia'))._value;
     
         // Url del servicio
         let urlUpdateFia: string = this.globals.GetUrlServicio() + this.urlUpdateFia;
     
         let jsonCrt = new JsonConvert();
         jsonCrt.operationMode = OperationMode.LOGGING; // print some debug data
         jsonCrt.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
         jsonCrt.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null
     
         // Armamos los headers
         var headers = new HttpHeaders()
         .set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
         .set('Authorization', this.tokenUsuFia)
         .set('Accept', 'application/json');
     
         return this.http
           .post<any>(
             urlUpdateFia, 
             dataForm,
             { headers: headers }
         )
         .pipe(map(data => data
           ));
 
       }catch(e) { 
         this.securityServices.clearSession();
       }
  
     }
   
     postUpdateContrato(dataForm: PostRegisterContrato): Observable<any> {
   
       try {
         // FGonzalez: Armamos el token con la variable de sesion
         this.tokenUsuFia = JSON.parse(sessionStorage.getItem('TokenUsuFia'))._value;
     
         // Url del servicio
         let urlUpdateFia: string = this.globals.GetUrlServicio() + this.urlUpdateContact;
     
         let jsonCrt = new JsonConvert();
         jsonCrt.operationMode = OperationMode.LOGGING; // print some debug data
         jsonCrt.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
         jsonCrt.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null
     
         // Armamos los headers
         var headers = new HttpHeaders()
         .set('Content-Type', 'application/json')
         .set('Authorization', this.tokenUsuFia)
         .set('Accept', 'application/json');
     
         return this.http
           .post<any>(
             urlUpdateFia, 
             dataForm,
             { headers: headers }
         )
         .pipe(map(data => data
           ));
 
       }catch(e) { 
         this.securityServices.clearSession();
       }
    
     }
   
     postUpdateFianza(dataForm: PostRegisterFianza): Observable<any> {
   
       try {
         // FGonzalez: Armamos el token con la variable de sesion
         this.tokenUsuFia = JSON.parse(sessionStorage.getItem('TokenUsuFia'))._value;
     
         // Url del servicio
         let urlUpdateFia: string = this.globals.GetUrlServicio() + this.urlUpdateFianza;
     
         let jsonCrt = new JsonConvert();
         jsonCrt.operationMode = OperationMode.LOGGING; // print some debug data
         jsonCrt.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
         jsonCrt.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null
     
         // Armamos los headers
         var headers = new HttpHeaders()
         .set('Content-Type', 'application/json')
         .set('Authorization', this.tokenUsuFia)
         .set('Accept', 'application/json');
     
         return this.http
           .post<any>(
             urlUpdateFia, 
             dataForm,
             { headers: headers }
         )
         .pipe(map(data => data
           ));
 
       }catch(e) { 
         this.securityServices.clearSession();
       }
    
     }
     
     postUpdateRecibo(dataForm: PostRegisterRecibo): Observable<any> {
   
       try {
         // FGonzalez: Armamos el token con la variable de sesion
         this.tokenUsuFia = JSON.parse(sessionStorage.getItem('TokenUsuFia'))._value;
     
         // Url del servicio
         let urlUpdateFia: string = this.globals.GetUrlServicio() + this.urlUpdateRecibo;
     
         let jsonCrt = new JsonConvert();
         jsonCrt.operationMode = OperationMode.LOGGING; // print some debug data
         jsonCrt.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
         jsonCrt.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null
     
         // Armamos los headers
         var headers = new HttpHeaders()
         .set('Content-Type', 'application/json')
         .set('Authorization', this.tokenUsuFia)
         .set('Accept', 'application/json');
     
         return this.http
           .post<any>(
             urlUpdateFia, 
             dataForm,
             { headers: headers }
         )
         .pipe(map(data => data
           ));
 
       }catch(e) { 
         this.securityServices.clearSession();
       }
 
     }
 
     getComentariosSol(numSol: number): Observable<any> {
     
       try {
         // FGonzalez: Armamos el token con la variable de sesion
         this.tokenUsuFia = JSON.parse(sessionStorage.getItem('TokenUsuFia'))._value;
         
         //Url del servicio
         let urlComentariosSol: string = this.globals.GetUrlServicio() + this.urlComentariosSol + "?numSol=" + numSol;
 
         let jsonCrt = new JsonConvert();
         jsonCrt.operationMode = OperationMode.LOGGING; // print some debug data
         jsonCrt.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
         jsonCrt.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null
 
         // Armamos los headers
         var headers = new HttpHeaders()
         .set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
         .set('Authorization', this.tokenUsuFia)
         .set('Accept', 'application/json');
 
         // Armamos el body para este servicio ira vacio
         const body = new HttpParams().toString();
 
         return this.http.post(
           urlComentariosSol, 
           body,    
           {headers: headers}    
         )
         .pipe(map(data => data));
 
       }catch(e) { 
         this.securityServices.clearSession();
       }
  
     }
     
     ReportPDFSol(): Observable<any> {
     
       try {
         // FGonzalez: Armamos el token con la variable de sesion
         this.tokenUsuFia = JSON.parse(sessionStorage.getItem('TokenUsuFia'))._value;
         
         //cve perfil
         let cve_perfil = JSON.parse(JSON.parse(sessionStorage.getItem('infoUser'))._value).CvePerfil;
 
         //Url del servicio
         let urlReportPDFSol: string = this.globals.GetUrlServicio() + this.urlReportPDFSol + "?cve_perfil=" + cve_perfil;
 
         let jsonCrt = new JsonConvert();
         jsonCrt.operationMode = OperationMode.LOGGING; // print some debug data
         jsonCrt.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
         jsonCrt.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null
 
         // Armamos los headers
         var headers = new HttpHeaders()
         .set('Content-Type', 'application/json')
         .set('Authorization', this.tokenUsuFia)
         .set('Accept', 'application/json');
 
         // Armamos el body para este servicio ira vacio
         const body = new HttpParams().toString();
 
         return this.http
         .post<any>(
           urlReportPDFSol, 
           body,    
           {headers: headers}    
         )
         .pipe(map(data => data));
 
       }catch(e) { 
         this.securityServices.clearSession();
       } 
     }  
 
     downloadPDF(pdf, nomArchivo) {
       const linkSource = `data:application/pdf;base64,${pdf}`;
       const downloadLink = document.createElement("a");
       const fileName = nomArchivo + moment().format("DD/MM/YYYY") +".pdf";
       downloadLink.href = linkSource;
       downloadLink.download = fileName;
       downloadLink.click();
     }

     getAllNotifications(): Observable<any> {
 
      try{

        // FGonzalez: Armamos el token con la variable de sesion
        this.tokenUsuFia = JSON.parse(sessionStorage.getItem('TokenUsuFia'))._value;

        //Url del servicio
        let urlGetNotifications: string = this.globals.GetUrlServicio() + this.urlNotificaciones;

        let jsonCrt = new JsonConvert();
        jsonCrt.operationMode = OperationMode.LOGGING; // print some debug data
        jsonCrt.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
        jsonCrt.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null

        // Armamos los headers
        var headers = new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
        .set('Authorization', this.tokenUsuFia)
        .set('Accept', 'application/json');

        // Armamos el body para este servicio ira vacio
        const body = new HttpParams().toString();

        return this.http
        .post<contracNotifiDTO>(
          urlGetNotifications, 
          body,    
          {headers: headers}    
        )
        .pipe(map(data => data));  
        
      }catch(e) { 
        this.securityServices.clearSession();
      }
  
    
    }
 

    ReportBitacoraSol(num_sol: number): Observable<any> {
     
      try {
        // FGonzalez: Armamos el token con la variable de sesion
        this.tokenUsuFia = JSON.parse(sessionStorage.getItem('TokenUsuFia'))._value;
        
        //cve perfil
        let cve_perfil = JSON.parse(JSON.parse(sessionStorage.getItem('infoUser'))._value).CvePerfil;

        //Url del servicio
        let urlReportPDFSol: string = this.globals.GetUrlServicio() + this.urlBitacoraPDFSol + "?_num_sol=" + num_sol;

        let jsonCrt = new JsonConvert();
        jsonCrt.operationMode = OperationMode.LOGGING; // print some debug data
        jsonCrt.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
        jsonCrt.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null

        // Armamos los headers
        var headers = new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', this.tokenUsuFia)
        .set('Accept', 'application/json');

        // Armamos el body para este servicio ira vacio
        const body = new HttpParams().toString();

        return this.http
        .post<any>(
          urlReportPDFSol, 
          body,    
          {headers: headers}    
        )
        .pipe(map(data => data));

      }catch(e) { 
        this.securityServices.clearSession();
      } 
    }  

    GetManual(): Observable<any> {
     
      try {
        // FGonzalez: Armamos el token con la variable de sesion
        this.tokenUsuFia = JSON.parse(sessionStorage.getItem('TokenUsuFia'))._value;
        
        //cve perfil
        let cve_perfil = JSON.parse(JSON.parse(sessionStorage.getItem('infoUser'))._value).CvePerfil;

        //Url del servicio
        let urlReportPDFSol: string = this.globals.GetUrlServicio() + this.urlManualSolicitudes;
 
        // Armamos los headers
        var headers = new HttpHeaders()
        .set('Authorization', this.tokenUsuFia)
        .set('Access-Control-Allow-Origin', '*')

        // Armamos el body para este servicio ira vacio
        const body = new HttpParams().toString();

        return this.http
        .post<any>(
          urlReportPDFSol, 
          body,    
          {headers: headers}    
        )
        .pipe(map(data => data));

      }catch(e) { 
        this.securityServices.clearSession();
      } 
    }


    postAnularSolicitud(dataForm: PostAnularSolicitud): Observable<any> {
   
      try {
        // FGonzalez: Armamos el token con la variable de sesion
        this.tokenUsuFia = JSON.parse(sessionStorage.getItem('TokenUsuFia'))._value;
    
        // Url del servicio
        let urlUpdateFia: string = this.globals.GetUrlServicio() + this.urlAnularSolicitud;
    
        let jsonCrt = new JsonConvert();
        jsonCrt.operationMode = OperationMode.LOGGING; // print some debug data
        jsonCrt.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
        jsonCrt.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null
    
        // Armamos los headers
        var headers = new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', this.tokenUsuFia)
        .set('Accept', 'application/json');
    
        return this.http
          .post<any>(
            urlUpdateFia, 
            dataForm,
            { headers: headers }
        )
        .pipe(map(data => data
          ));

      }catch(e) { 
        this.securityServices.clearSession();
      }

    }

    // #Cancelaciones -> Ahernandezh 02/02/2021
    CancelacionFianza(dataForm: PostRegisterComentNuevo): Observable<any> 
    {      
      try 
      {
        // FGonzalez: Armamos el token con la variable de sesion
        this.tokenUsuFia = JSON.parse(sessionStorage.getItem('TokenUsuFia'))._value;
        //Url del servicio
        let urlCancelacionFianza: string = this.globals.GetUrlServicio() + this.urlCancelacionFianza;
        // Armamos los headers
        var headers = new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', this.tokenUsuFia)
        .set('Accept', 'application/json');
        // Armamos el body con los datos del formulario
        const body = dataForm;
        return this.http.post<PostRegisterComentNuevo>
        (urlCancelacionFianza,body,{headers: headers})
        .pipe(map(data => data));
      }catch(e) 
      { 
        this.securityServices.clearSession();
      }
    }
    // #FinCancelaciones

}
