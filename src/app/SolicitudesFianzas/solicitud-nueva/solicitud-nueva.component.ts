import { Component, OnInit, Renderer2, ElementRef, ViewChild, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { trigger, state, style, animate, transition} from '@angular/animations';
import { HttpClient} from '@angular/common/http';
import { FormGroup, FormBuilder, Validators, AbstractControl, ValidatorFn } from '@angular/forms';

import { NgxSpinnerService } from 'ngx-spinner';

import { Router } from '@angular/router';
import { SessionStorageService } from 'angular-web-storage';

import * as moment from 'moment';
import {JsonConvert, JsonConverter, OperationMode, ValueCheckingMode} from "json2typescript"
import Swal from 'sweetalert2'

//MODELS
import { solicitudNuevaDTONew } from 'src/app/Models/solicitudes';
import { TipoSolicitudNew } from 'src/app/Models/tipoSolicitud';
import { TipoDocumento } from 'src/app/Models/tipoDocu';
import { FilesSolNuevaDTO } from 'src/app/Models/filesSolNuevaDTO';
import { PostRegisterSolNueva } from 'src/app/Models/PostRegisterSolNueva';


//SERVICES
import { SrvsolicitudesService } from '../services/srvsolicitudes.service';
import { TipoSubRamo } from 'src/app/Models/fiaCore';
import { SecurityService } from 'src/app/Servicios/security.service';
import { validateStyleParams } from '@angular/animations/browser/src/util';
import { distinctUntilChanged } from 'rxjs/operators';


declare var $;

@Component({
  selector: 'app-solicitud-nueva',
  templateUrl: './solicitud-nueva.component.html',
  styleUrls: ['./solicitud-nueva.component.scss'],
  animations: [
    trigger('changeDivSize', [
      state('initial', style({
        backgroundColor: 'green',
        width: '100px',
        height: '100px'
      })),
      state('final', style({
        backgroundColor: 'red',
        width: '200px',
        height: '200px'
      })),
      transition('initial=>final', animate('1500ms')),
      transition('final=>initial', animate('1000ms'))
    ]),

    trigger('balloonEffect', [
      state('initial', style({
        backgroundColor: 'green',
        transform: 'scale(1)'
      })),
      state('final', style({
        backgroundColor: 'red',
        transform: 'scale(1.5)'
      })),
      transition('final=>initial', animate('1000ms')),
      transition('initial=>final', animate('1500ms'))
    ]),

    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition('void <=> *', animate(1000)),
    ]),

    trigger('EnterLeave', [
      state('flyIn', style({ transform: 'translateX(0)' })),
      transition(':enter', [
        style({ transform: 'translateX(-100%)' }),
        animate('0.5s 300ms ease-in')
      ]),
      transition(':leave', [
        animate('0.3s ease-out', style({ transform: 'translateX(100%)' }))
      ])
    ]),
    trigger('fadeInOut', [
      state('in', style({ opacity: 100 })),
      transition('* => void', [
            animate(300, style({ opacity: 0 }))
      ])
    ])
  ]
})
export class SolicitudNuevaComponent implements OnInit {

  @ViewChild("hDocs") hDocs: ElementRef;

  nameTit: string = "Nueva";
  altSolForm: FormGroup;
  solNew: solicitudNuevaDTONew = new solicitudNuevaDTONew();
  usuIdSol: any;
  dateNow: any;
  divDocs; any;
  arrTipSols: TipoSolicitudNew []; 
  arrSubRamos: TipoSubRamo []; 
  arrTipFia: TipoSolicitudNew[];
  idAfi: number;
  idRamo: number;
  idSubRamo: number;
  bolOk: Boolean = false;
  arrTipDocs: Array<TipoDocumento> = [];
  arrFileDocs: Array<FilesSolNuevaDTO> = [];
  postRegNewSol: PostRegisterSolNueva = new PostRegisterSolNueva();
  fileUpload: HTMLInputElement;
  value: number = 50;
  divDatFiado: Boolean = false;
  fiadoOther: Boolean = false;

//FileUpload
/*
//***************************************************************************************************************** /**         Link text */
      @Input() text = 'Upload';
      /** Name used in form which will be sent in HTTP request. */
      @Input() param = 'file';
      /** Target URL for file uploading. */
      @Input() target = 'https://file.io';
      /** File extension that accepted, same as 'accept' of <input type="file" />. 
          By the default, it's set to 'image/*'. */
      @Input() accept = 'image/*, .pdf, .doc, .xls, .xlsx, .docx, ppt, pptx, .xml';
      /** Allow you to add handler after its completion. Bubble up response text from remote.**/
      @Output() complete = new EventEmitter<string>();

     // private filesUpMod: Array<FileUploadModel> = [];

      formData = new FormData(); 

      stMatPro: string = "void";
   
      private filesLoad: any[] = [];
      private fileReader = new FileReader();


//******************************************************************************************************************/
  constructor(
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private solicitudService: SrvsolicitudesService,
    private securityServices: SecurityService,
    public renderer: Renderer2,
    private _http: HttpClient,
    private router: Router,
    public session: SessionStorageService
  ) { 
    
    this.usuIdSol = JSON.parse(JSON.parse(sessionStorage.getItem('infoUser'))._value);

    moment.locale('es');


    setInterval(() => {
      this.dateNow = moment().format('LLLL');
    }, 1);
    

  }

  ngOnInit() {
    //Validacion de los campos del formulario
    this.altSolForm = this.formBuilder.group({
      slRamo:     ['', Validators.compose([Validators.required]) ],
      slSubRamo:  ['', Validators.compose([Validators.required]) ],
      slTipFia:   ['', Validators.compose([Validators.required]) ],
      slBenf:     ['', Validators.compose([Validators.required])],
      slTipMov:   ['', Validators.compose([Validators.required])],
      slAfin:     ['', Validators.compose([Validators.required])],
      slProyecSol: ['', Validators.compose([Validators.required])],

      slFiad:     ['', Validators.compose([Validators.required])],
      observaciones:     ['', Validators.compose([Validators.required])],
      inRfcFiado:   ['', Validators.compose([])],
      inRfcFiadoConf:   ['', Validators.compose([])],
      nombreFiado:   ['', Validators.compose([])],
      
    });

  
    /***********************************************************************************************************/

    this.setUserFiadoValidators();
    //this.getSolicitudNueva('');

    /***********************************************************************************************************/

    document.querySelector("html").classList.add('js');

  }

  ngOnDestroy(): void {
    document.body.className = '';
  }

  ngAfterViewInit() {
    
    this.spinner.show();  

    this.getSolicitudNueva();
              
  }

  //------------------------------ Metodos ----------------------------------------------
  getSolicitudNueva() {

    this.spinner.show();    

    if(
      this.solNew.lstTipRamos.length == 0 ||
      this.solNew.lstTipSubRamos.length == 0 ||
      this.solNew.lstTipMovimiento.length == 0 ||
      this.solNew.lstTipSol.length == 0 ||
      this.solNew.lstAfianzadoras.length == 0 ||
      this.solNew.lstBeneficiarios.length == 0 ||
      this.solNew.lstFiados.length == 0)
    {
      this.solicitudService.getNuevaSolicitud().subscribe(
        data => {
          
          this.spinner.show(); 

          //Filtramos por grupo
          let id_grupo : Number = this.usuIdSol.id_grupo;

          var arrRamos = JSON.stringify(data.solicitudNuevaDTO.lstTipRamos);
          var arrSubRamos = JSON.stringify(data.solicitudNuevaDTO.lstTipSubRamos);
          var arrTipSOl =  JSON.stringify(data.solicitudNuevaDTO.lstTipSolicitudes);
          var arrTipMov =  JSON.stringify(data.solicitudNuevaDTO.lstTipMovimientos);
          var arrAfianz =  JSON.stringify(data.solicitudNuevaDTO.lstAfianzadoras);
          var arrBenef =  JSON.stringify(data.solicitudNuevaDTO.lstBeneficiarios);
          var arrFiad =  JSON.stringify(data.solicitudNuevaDTO.lstFiados);
          var arrProy =  JSON.stringify(data.solicitudNuevaDTO.lstProyectos);
  
          this.solNew.lstTipRamos = JSON.parse(arrRamos);
          this.solNew.lstTipSubRamos = JSON.parse(arrSubRamos);
          this.solNew.lstTipMovimiento = JSON.parse(arrTipMov);
          this.solNew.lstTipSol = JSON.parse(arrTipSOl);
          this.solNew.lstProyectos = JSON.parse(arrProy).sort();
          this.solNew.lstAfianzadoras = JSON.parse(arrAfianz).filter(x => x.ID_GRUPO == id_grupo);
          this.solNew.lstBeneficiarios = JSON.parse(arrBenef);

          //Usuario proveedor tiene un fiado fijo
          if( this.usuIdSol.CvePerfil != 8 ){
            this.fiadoOther = true;
            this.solNew.lstFiados = JSON.parse(arrFiad).filter(x => x.ID_GRUPO == id_grupo);
          }else{
            this.fiadoOther = false;
            this.solNew.lstFiados = JSON.parse(arrFiad).filter(x => x.ID_GRUPO == id_grupo && x.ID_FIADO == this.usuIdSol.idFiado);
          }

          this.spinner.hide();

        },
        // Si ducede algun error se limpia la sessión y redirige al login
        error => {
          this.spinner.hide();
          this.securityServices.clearSession();
          console.log('oops: ', error)
        },
        () => {        

          // if(id != ''){
          //   var evt = document.createEvent("MouseEvents");
          //   evt.initMouseEvent("click", true, true, window,
          //     0, 0, 0, 0, 0, false, false, false, false, 0, null);
          //   var a = document.getElementById(id.toString()); 
          //   a.dispatchEvent(evt); 
          // }
          
          this.spinner.hide();
        }    
      );
    }
    else{
      this.spinner.hide();
    }
  }

 
//Ok Form Alta Solicitud
//*************************************************************************************************************/

  onAltaSol(){

    if (this.altSolForm.valid ) {

      Swal.fire({
        title: 'Generar una Solictud',
        text: "¿Estas seguro de generar una solictud?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'SI, estoy de acuerdo!'
      }).then((result) => {
    
        this.bolOk =  (result.value === 'true') ? true : false; 
        
        this.spinner.show();  

        if(result.value){

          if ( this.arrFileDocs.length > 0  ) {
    
            this.postRegNewSol.userName = this.session.get("nameUsuFia");
            this.postRegNewSol.userId =   this.usuIdSol.idUsuario;
            this.postRegNewSol.userDate = moment().format();
            this.postRegNewSol.ramoId =     this.altSolForm.get('slRamo').value;
            this.postRegNewSol.subRamoId = this.altSolForm.get('slSubRamo').value;
            this.postRegNewSol.paqId =    this.altSolForm.get('slTipFia').value;
            this.postRegNewSol.tipMoviId = this.altSolForm.get('slTipMov').value;
            this.postRegNewSol.clieId = 1;  //[dato que mandara Coria en la respuesta del login] debe tomar el cliente del Core de fianzas
            this.postRegNewSol.gereId = 1; 
            this.postRegNewSol.afianId =  this.altSolForm.get('slAfin').value;
            this.postRegNewSol.benfId =   this.altSolForm.get('slBenf').value;
            this.postRegNewSol.nameBenef = this.solNew.lstBeneficiarios.filter(x => x.CVE_BENEF == this.altSolForm.get('slBenf').value)[0].RAZON_SOCIAL_BENEF.toString();
            this.postRegNewSol.fiadoId =   this.altSolForm.get('slFiad').value;

            if (this.postRegNewSol.fiadoId == 0){               
              this.postRegNewSol.fiadoRFC = this.altSolForm.get('inRfcFiado').value;
              this.postRegNewSol.fiadoRFCConf = this.altSolForm.get('inRfcFiadoConf').value;
              this.postRegNewSol.fiadoNombre = this.altSolForm.get('nombreFiado').value;
              this.postRegNewSol.fiadoGrupo = this.usuIdSol.id_grupo;
            }
            else{              
              this.postRegNewSol.fiadoRFC = '';
              this.postRegNewSol.fiadoNombre = '';
              this.postRegNewSol.fiadoGrupo = 0;
            }

            this.postRegNewSol.tipProy =  this.altSolForm.get('slProyecSol').value;
            this.postRegNewSol.detSol =   this.altSolForm.get('observaciones').value;

            // this.postRegNewSol.observaciones = this.altSolForm.get('observaciones').value;

            this.postRegNewSol.filesSolNuevaDTO = this.arrFileDocs;
            this.solicitudService.postNuevaSolicitud(this.postRegNewSol).subscribe(
                  data => {

                    if(data.codeResult == 0){
  
                      this.spinner.hide();  
                      
                      Swal.fire(
                        'Gurdado!',
                        '¡Solicitud: ' + data.solicitudInsDTO.NUM_SOL + ' gurdada con exito!',
                        'success'
                      )
                      
                      this.router.navigate(["/solicitudes", 3]).then( (e) => {
                      });
  
                    }else{
                      this.spinner.hide();  
                      Swal.fire(
                        'Error!',
                        '¡Contactar a Sistemas Ordás-Howden!',
                        'error'
                      )
                    }
                  },
                  // Si ducede algun error se limpia la sessión y redirige al login
                  error => {
                    this.spinner.hide();
                    this.securityServices.clearSession();
                    console.log('oops: ', error)
                  }
                );
  
          }else{
            this.spinner.hide();  
            Swal.fire(
              'Error!',
              '¡Debes cargar al menos 1 documento!',
              'error'
            )            
          }        
        }else{
          this.spinner.hide();  
            Swal.fire(
              'Error!',
              '¡Decidiste cancelar la operación!',
              'error'
            )
        }
      });   
    }
  }

//FileUploas Methods
//*************************************************************************************************************/
  onClick(id) {
      this.stMatPro = "in";
      var tipDoc =  this.arrTipDocs.filter(
                    doc => doc.CVE_TIPO_DOC === id);
        this.fileUpload = document.getElementById(id) as HTMLInputElement;
        this.fileUpload.click();
        this.stMatPro = "void";
  }

  onChange(event: Event, id) {
    let evFiles = event.target['files'];

    var tipDoc =  this.arrTipDocs.filter(doc => doc.CVE_TIPO_DOC === id);
    this.fileUpload = document.getElementById(id) as HTMLInputElement;
    document.getElementById(tipDoc[0].idTipDoc.toString()).innerHTML = evFiles[0].name;

    if (event.target['files']) {
      this.readFiles(event.target['files'], 0, id);
    }
  };

  private readFiles(filesLoad: FileDataForm, index: number, idDoc: number) {
    
    let file = filesLoad[index];
    let fileNewSol: FilesSolNuevaDTO = new FilesSolNuevaDTO();
   
    fileNewSol.nameFile = file.name.split('.')[0] + "." + file.name.split('.').pop();
    fileNewSol.sizeFile = this.formatBytes(file.size, 0).toString();
    fileNewSol.extFile = file.name.split('.').pop();
    fileNewSol.idTipFile = idDoc;
    fileNewSol.typeFile = file.type.toString();

    this.fileReader.onload = () => {
      fileNewSol.dataFile = this.fileReader.result.toString();
      this.arrFileDocs = this.arrFileDocs.filter(function (p){ return p.idTipFile != idDoc});
      this.arrFileDocs.push(fileNewSol);
      this.formData.append(fileNewSol.nameFile.toString(), file);
    };
    this.fileReader.readAsDataURL(file);
  }

  formatBytes(bytes, decimals) {
    if (bytes == 0) return 0;
    var k = 1024;
    var dm = decimals + 1 || 2;
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    var i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

  changeFiado (ev) {
    if(ev.value == 0){
      this.divDatFiado = true;
      this.altSolForm.controls['inRfcFiado'].setValidators([Validators.compose([Validators.required])]);
      this.altSolForm.controls['inRfcFiadoConf'].setValidators([Validators.compose([Validators.required])]);
      this.altSolForm.controls['nombreFiado'].setValidators([Validators.compose([Validators.required])]);
    }
    else{
      this.divDatFiado = false;
      this.altSolForm.controls['inRfcFiado'].clearValidators();
      this.altSolForm.controls['inRfcFiadoConf'].clearValidators();
      this.altSolForm.controls['nombreFiado'].clearValidators();
    }
  }

  //Coments
  /************************************************************************************************************/
  getAfianzadora (ev) {
    this.idAfi = ev.source.value;
  }

  getSubRamo(ev) {
      
    this.arrSubRamos = this.solNew.lstTipSubRamos;
    this.idRamo = ev.source.value;
    this.arrSubRamos =  this.arrSubRamos.filter( tipSubRamo => tipSubRamo.CVE_RAMO === this.idRamo);
      
  }

  getTipFianza(ev) {
    
    this.arrTipFia = this.solNew.lstTipSol;
    this.idSubRamo = ev.source.value;
    //Añadir CVE de la afianzadora
    this.arrTipFia =  this.arrTipFia.filter( tipFia => tipFia.CVE_RAMO === this.idRamo &&  tipFia.CVE_SUBRAMO === this.idSubRamo && tipFia.CVE_AFIANZADORA == this.idAfi);
    this.arrTipFia.sort((a,b) => a.DES_PAQUETE.localeCompare(b.DES_PAQUETE.toString()));

  }

  showFilesFia(ev){
    
      this.arrTipSols = this.solNew.lstTipSol;
      let idTipPaq = ev.source.value;
      var selectedIds =  this.arrTipSols.filter(tipSol => tipSol.CVE_PAQUETE === idTipPaq && tipSol.CVE_RAMO === this.idRamo && tipSol.CVE_SUBRAMO === this.idSubRamo);
      this.arrTipDocs = selectedIds[0].lstTipDocumentos;
      
      var prueba = this.arrTipDocs[0].DES_DOCUMENTO.toString().replace(/\s/g, ""); 

      this.arrTipDocs.forEach( obj => {
        obj.idTipDoc = obj.DES_DOCUMENTO.toString().replace(/\s/g, ""); 
      
      })    
  }

  changeRfc(){

    if(this.altSolForm.get('inRfcFiado').value != ''){
      
      this.altSolForm.controls['inRfcFiadoConf'].enable();  

    }

    this.solNew.lstFiados.forEach( (fia) => {
      if(this.altSolForm.get('inRfcFiado').value == fia.RFC){
        Swal.fire(
          'Error!',
          '¡El RFC del fiado ya existe en su grupo, favor de seleccionarlo en la lista!',
          'error'
        );
        this.altSolForm.controls['inRfcFiado'].setValue('');
        this.altSolForm.controls['inRfcFiadoConf'].setValue('');
      }  
    });
  }

  changeRfcConf(){

    if(this.altSolForm.get('inRfcFiado').value != this.altSolForm.get('inRfcFiadoConf').value){
      
      Swal.fire(
        'Error!',
        '¡Los RFC no coinciden, intente de nuevo!',
        'error'
      );
      this.altSolForm.controls['inRfcFiadoConf'].setValue('');
    }
  }


  setUserFiadoValidators() {
    
    const getFiado = this.altSolForm.get('slFiad');
    const rfcFiadoControl = this.altSolForm.get('inRfcFiado');
    const rfcFiadoConfControl = this.altSolForm.get('inRfcFiadoConf');
    const nomFiadoControl = this.altSolForm.get('nombreFiado');

    this.altSolForm.get('slFiad')
    .valueChanges.pipe(distinctUntilChanged())
    .subscribe(userFiado => {

      if (userFiado === 0) {
        this.divDatFiado = true;
        rfcFiadoControl.setValidators([Validators.required]);
        rfcFiadoConfControl.setValidators([Validators.required]);
        nomFiadoControl.setValidators(Validators.required);
        getFiado.setValidators(null);

      }else{
        this.divDatFiado = false;
        rfcFiadoControl.setValidators(null);
        rfcFiadoConfControl.setValidators(null);
        nomFiadoControl.setValidators(null);
        getFiado.setValidators(Validators.required);
      }

      getFiado.updateValueAndValidity();
      rfcFiadoControl.updateValueAndValidity();
      rfcFiadoConfControl.updateValueAndValidity();
      nomFiadoControl.updateValueAndValidity();
    });
  }
}