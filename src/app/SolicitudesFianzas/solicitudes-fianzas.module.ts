import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//Forms
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DataViewModule } from 'primeng/dataview';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';

//Material Desing
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

//DATAPICKER RANGE
import { MyDateRangePickerModule } from 'mydaterangepicker';
//PrimeNg
import { PanelModule, DataTableModule, SharedModule, ButtonModule, DialogModule, InputTextModule,
        CalendarModule, DropdownModule, ConfirmDialogModule, ConfirmationService, OrderListModule,
        StepsModule
} from 'primeng/primeng';

import {TableModule } from 'primeng/table';

import { MatTableModule, MatFormFieldModule, MatInputModule, MatPaginatorModule,
         MatButtonModule, MatIconModule, MatDialogModule, MatTabsModule,
         MatCardModule, MatSlideToggleModule, MatSelectModule, MatProgressBarModule, MatDatepickerModule,
         MatNativeDateModule } from '@angular/material';

import { NgxSpinnerModule} from 'ngx-spinner'

import { NgxCurrencyModule } from "ngx-currency";

import { SatDatepickerModule } from 'saturn-datepicker'

import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';




//Componentes
  //Carga de Layout 
import { LayoutModule } from 'src/app/layout/layout.module';
import { SolicitudesComponent } from './solicitudes/solicitudes.component';
import { AppRoutingModule } from '../app-routing.module';

import { SolicitudesListComponent } from './solicitudesComp/solicitudes-list/solicitudes-list.component';
import { SolicitudesDetalleComponent } from './solicitudesComp/solicitudes-detalle/solicitudes-detalle.component';
import { SolicitudNuevaComponent } from './solicitud-nueva/solicitud-nueva.component';

//IPR Root Variables
import { rootGlobals } from '../rootGlobals';


//Servicios Solicitudes
import { SrvsolicitudesService } from './services/srvsolicitudes.service';


import { RouterModule } from '@angular/router';

import {ToastModule, ToastOptions} from 'ng6-toastr/ng2-toastr';
import { CustomOption } from '../SolicitudesFianzas/solicitudesComp/custom-option';



@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    //Shared View
    LayoutModule,
    //DateRange OK
    MyDateRangePickerModule,
    SatDatepickerModule,
    //DataView PrimeNg
    PanelModule,
    DataViewModule,
    //DataTable PrimeNg
    DataTableModule,
    SharedModule,
    ButtonModule,
    DialogModule,
    InputTextModule,
    CalendarModule,
    DropdownModule,
    ConfirmDialogModule,
    //Table PrimeNg
    TableModule,
    OrderListModule,
    StepsModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatTabsModule,
    MatCardModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatProgressBarModule,
    MatDatepickerModule,
    MatNativeDateModule,
    BrowserAnimationsModule,
    NgxSpinnerModule,
    NgxCurrencyModule,
    //Scroll
    ScrollToModule.forRoot(),
    ToastModule.forRoot()
  ],
  declarations: [
    SolicitudesComponent,
    SolicitudesListComponent,
    SolicitudesDetalleComponent,
    SolicitudNuevaComponent,
   
  ],
  entryComponents: [
    SolicitudesDetalleComponent
  ],
   //IPR Services
   providers: [
    {provide: ToastOptions, useClass: CustomOption},
    SrvsolicitudesService,
    ConfirmationService,
    rootGlobals],
    exports: [],
    
  
})
export class SolicitudesFianzasModule { }
