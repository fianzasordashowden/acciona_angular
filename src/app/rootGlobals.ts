import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SessionStorageService } from 'angular-web-storage';

@Injectable()
export class rootGlobals {
    
    public aplicacionID: string = "1";

    public aplicacion: string = "accionaApp";
    public senderID: string = "AAAAWucWkAs:APA91bF5bjBOaBoVusNb97oUBuxrWihJXLK8pjyuKSgBIax_2BAk3Hk99D4QizbTPC0dx2welD-Xm4ga_CTzcXm63_9XMwwHz0wNsmUYlGhuxm6des2oKxy7gm6PEbJIbIigd2D4DkDc";

    private UrlServiceLocal = 'http://localhost:13096/api/';
    //private UrlServiceLocal = 'https://enginia.grupordas.com.mx/FianzasWebApi_Desarrollo/api/';
    //private UrlServiceLocal = 'https://enginia.grupordas.com.mx/FianzasWebApi/api/';
    
    private UrlServicePublish = 'https://enginia.grupordas.com.mx/FianzasWebApi/api/';
    //private UrlServicePublish = 'https://enginia.grupordas.com.mx/FianzasWebApi_Desarrollo/api/';

    private session = new SessionStorageService();

  // Agrego Ahernandezh 25/10/2019 -> Obtiene Url para LocalHost/ServidorWeb
  GetUrlServicio()
  {
      if (location.hostname === 'localhost' 
          || location.hostname === '127.0.0.1' 
          || location.hostname === '')
      {
          return this.UrlServiceLocal;
      }
      else
      {
          return this.UrlServicePublish;
      }
  }

  GetIdUsuario()
  {
      let idUsuarioApp: number = 0;
      if(this.session.get('usuIdFia') != undefined)
      {
          idUsuarioApp = parseInt(this.session.get('usuIdFia'));
      }
      return idUsuarioApp
  }

  // Agrego Ahernandez 14/10/2019 -> Heeader con JWT para evitar HttpInterceptor
  GetHttpHeaders(): HttpHeaders
  {
      const headers: HttpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', this.session.get('TokenUsuFia'))
      .set('Accept', 'application/json');
      return headers;
  }

}   