import { TestBed, inject } from '@angular/core/testing';

import { BlockUIServiceService } from './block-uiservice.service';

describe('BlockUIServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BlockUIServiceService]
    });
  });

  it('should be created', inject([BlockUIServiceService], (service: BlockUIServiceService) => {
    expect(service).toBeTruthy();
  }));
});
