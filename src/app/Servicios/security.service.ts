


import { Injectable } from '@angular/core';

//IPR 
import { HttpClient, HttpResponse, HttpHeaders, HttpParams, HttpErrorResponse,} from '@angular/common/http';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/internal/Observable';
import { map, catchError, tap } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { BlockUIService } from './block-uiservice.service';

import { SessionStorageService, SessionStorage, StorageService } from 'angular-web-storage';

import {JsonConvert, OperationMode, ValueCheckingMode} from "json2typescript"

import { InfoUsuario } from '../Models/infoUser'
import { Usuario } from '../Models/usuFront';
import { SchemaUsuario } from '../Models/shemaUsu';
import { FuncUsuario } from '../Models/funcUsu';
import { rootGlobals } from 'src/app/rootGlobals';

import { DeviceDetectorService } from 'ngx-device-detector';
import { UserLog } from '../Models/userLog';
import { Router, ActivatedRoute  } from '@angular/router';
import { NgxSpinnerService} from 'ngx-spinner'
import { PostChangePass } from '../Models/PostChangePass';
import { PostTokenNotificacion } from '../Models/PostTokenNotificacion';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})


export class SecurityService  {
  
  private urlLogin: string = "Security/Token";
  private urlValToken: string = "Security/Autenticado";
  private urlSendMailToResPassword: string = "Security/SendMailToResPassword";
  private urlChangePassword: string = "Security/ChangePassword";
  private urlAddTokenNotificacion: string = "Security/AddTokenNotificacion";
 
  //IPR 16/03/2021
  idUsuario: number;
  sUrl: string;
  private infoUser : any;

  constructor(
    private http: HttpClient,
    private blockUIService: BlockUIService,
    public session: SessionStorageService,
    private spinner: NgxSpinnerService,
    // Info device
    public deviceService: DeviceDetectorService,
    // Traemos nustras variables globales
    public globals: rootGlobals,
    public router: Router,
  ) { }

  //*********************************************************************************************************************/

  // FGonzalez: Fución de inicio de sesión
  loginuser(username: string, password: string): Observable<any> {   

    let userLogString: string = ''
    let jsonCrt     = new JsonConvert();
    let userLogObj  = new UserLog();

    // Url del servicio
    let urlModLogin: string  = this.globals.GetUrlServicio() + this.urlLogin;

    // Traemos la información del dispositivo
    let deviceInfo = this.deviceService;

    // FGonzalez: Se añaden los valores al objeto userLogObj
    userLogObj.username  = username
    userLogObj.fecha     = new Date().toDateString();
    userLogObj.aplicacion= this.globals.aplicacion;
    userLogObj.browser   = deviceInfo.browser;
    userLogObj.device    = deviceInfo.device;
    userLogObj.isDesktop = deviceInfo.isDesktop();
    userLogObj.isMobile  = deviceInfo.isMobile();
    userLogObj.isTablet  = deviceInfo.isTablet();
    userLogObj.os        = deviceInfo.os;

    //Convertimos el objeto JSON a string
    userLogString = JSON.stringify(userLogObj).toString();

    jsonCrt.operationMode = OperationMode.LOGGING; // print some debug data
    jsonCrt.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
    jsonCrt.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null

    // Armamos los headers
    const headers: HttpHeaders = new HttpHeaders()
    .set('Content-Type', 'application/x-www-form-urlencoded');

    // Armamos el body enviando los parametros vía Http
    const body = new HttpParams() 
    .set('grant_type', 'password') 
    .set('username', username)
    .set('password', encodeURIComponent(password))
    .set('client_id', this.globals.aplicacion)
    .set('userLog', userLogString)
    .set('aplicacionID', this.globals.aplicacionID);
    
    //Mandamos a llamar al servicio
    return this.http
      .post<Usuario>(
        urlModLogin, 
        body.toString(), 
        { headers: headers }
      )
      .pipe(map(data => data));  
  }

  // FGonzalez: Función para validar el token
  validateToken(): Observable<any> {   

    // Traemos el token de la variable de sesión
    let tokenUsuFia: string = JSON.parse(sessionStorage.getItem('TokenUsuFia'))._value;
    
    //Url del servicio
    let urlValToken: string  = this.globals.GetUrlServicio() + this.urlValToken;

    // const headers: HttpHeaders = this.globals.GetHttpHeaders();
    // const AutorizacionRequest = 
    // {
    //   idUsuario: this.globals.GetIdUsuario(),
    //   sUrl: this.router.url
    // };
    // console.log('AutorizacionRequest ', AutorizacionRequest);
    
    // //Mandamos a llamar al servicio
    // return this.http
    //   .post<String>(
    //     urlValToken, 
    //     AutorizacionRequest, 
    //     { headers: headers}
    //   )
    //   .pipe(map(data => data));  

     //IPR 16/03/2021
     const headers: HttpHeaders = this.globals.GetHttpHeaders();
     let idUsuario: number = this.globals.GetIdUsuario();    
 
     //this.sUrl = this.router.url;
     if( (this.router.url.split('/').length -1) > 1){
       this.sUrl = this.router.url.substring(0, this.router.url.lastIndexOf("/") + 0);
     }else{
       this.sUrl = this.router.url;
     }
     
     const AutorizacionRequest =
       {
       idUsuario: idUsuario,
       sUrl: this.sUrl,
       id_aplicacion: Number(this.globals.aplicacionID)
       };
 
     return this.http
       .post<any>(
                 urlValToken,
                 AutorizacionRequest,               
                 { headers: headers }
                 ).pipe(map(
                   data => data
                   ));


  }
  
  // FGonzalez: Funcion que valida la session llamando a otras funciones como validateToken() y clearSession()
  validateSession(){
    // Preguntamos si la sesion existe o treae parametros
    var session = window.sessionStorage.length;
    if(session > 0){
      // En caso de que si vamos a validar el token que ya tendría que existir en la sesión
      return this.validateToken().subscribe(
        data => {
          //IPR 16/03/2021
          if (data.ExisteError) {

            Swal.fire(
              'Aviso!',
              '¡Sin derechos de acceso, se terminara la sessión.!',
              'error'
            );

            this.clearSession();
          }
          // Cualquiera de las otras vertientes limpiaran la session y redireccionaran al index
          else{
            return data;
          } 
        },
        error => {
          this.clearSession();
        }
      );
    }
    else{
      this.clearSession();
    }    
  }

  // FGonzalez: Función que limpia la session y redirecciona al login
  clearSession() {    
    // Mostramos spinner de carga
    this.spinner.show();
    // Limpia de sesión
    this.session.clear();
    // Redireccion al login
    this.router.navigate(['login']);
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 500);
  }
  //*********************************************************************************************************************/

    private handleError(error: any, blockUIService: BlockUIService, blocking: Boolean) {

    let body = error.json();

    if (blocking) {
        blockUIService.blockUIEvent.emit({
            value: false
        });
    }
    return Observable.throw(body);

    }

    private parseResponse(response: Response, blockUIService: BlockUIService, blocking: Boolean) {

        let authorizationToken = response.headers.get("Authorization");

        if (authorizationToken != null) {
            if (typeof (Storage) !== "undefined") {
                localStorage.setItem("VIDA.Token", authorizationToken);
            }
        }

        if (blocking) {
            blockUIService.blockUIEvent.emit({
                value: false
            });
        }

        let body = response.json();

        return body;
    }

  // FGonzalez: Funcion que valida la session llamando a otras funciones como validateToken() y clearSession()
  sendMailToResPassword(userMail: string){
    
      //Url del servicio
      let urlSendMailToResPassword: string  = this.globals.GetUrlServicio() + this.urlSendMailToResPassword + "?userMail=" + userMail;

      let jsonCrt = new JsonConvert();
      jsonCrt.operationMode = OperationMode.LOGGING; // print some debug data
      jsonCrt.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
      jsonCrt.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null

      // Armamos los headers
      var headers = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
      .set('Accept', 'application/json');

      // Armamos el body para este servicio ira vacio
      const body = new HttpParams().toString();
      
      //Mandamos a llamar al servicio
      return this.http
        .post<String>(
          urlSendMailToResPassword, 
          body, 
          { headers: headers}
        )
        .pipe(map(data => data));   
  }

  // FGonzalez: Funcion que valida la session llamando a otras funciones como validateToken() y clearSession()
  changePass(postChangePass: PostChangePass){
    
    //Url del servicio
    let urlChangePassword: string  = this.globals.GetUrlServicio() + this.urlChangePassword;

    let jsonCrt = new JsonConvert();
    jsonCrt.operationMode = OperationMode.LOGGING; // print some debug data
    jsonCrt.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
    jsonCrt.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null

    // Armamos los headers
    var headers = new HttpHeaders()
    .set('Content-Type', 'application/json')
    .set('Accept', 'application/json');

    // Armamos el body para este servicio ira vacio
    const body = postChangePass;
    
    //Mandamos a llamar al servicio
    return this.http
      .post<String>(
        urlChangePassword, 
        body, 
        { headers: headers}
      )
      .pipe(map(data => data));   
  }

  // FGonzalez: Notificaciones
  addTokenNoti(PostTokenNotificacion: PostTokenNotificacion){
    
    //Url del servicio
    let urlAddTokenNotificacion: string  = this.globals.GetUrlServicio() + this.urlAddTokenNotificacion;

    let jsonCrt = new JsonConvert();
    jsonCrt.operationMode = OperationMode.LOGGING; // print some debug data
    jsonCrt.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
    jsonCrt.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null

    // Armamos los headers
    var headers = new HttpHeaders()
    .set('Authorization', 'key=' + this.globals.senderID)
    .set('Accept', 'application/json')
    .set('Content-Type', 'application/json');

    // Armamos el body para este servicio ira vacio
    const body = PostTokenNotificacion;
    
    //Mandamos a llamar al servicio
    return this.http
      .post<String>(
        urlAddTokenNotificacion, 
        body, 
        { headers: headers}
      )
      .pipe(map(data => data));   
  }
  
}

  
	
	